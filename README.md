Coco-CC is a collection of sources for the cc collection distributed for the Tandy Color Computer.  

Included in this package is:

cc     - The frontend for the package
c,comp - The single-pass compiler
c.opt  - The optimizer
rma    - The assembler
rlink  - The linker

At the moment, the original prep (c.prep) is not included, but prep19, a custom prep app is included
