/*
** driver for the C compiler
*/
/*
**  01-23-85  Added -b and -t options. Enabled /nl.
**  ??-??-??  Added -O option to stop compilation after optimization.
**            Prevent optimization of ".a" files.
**  02-15-85  Fix naming conventions if -O is used.
**  ??-??-??  ll option for lex lib
**  07-30-86  P option for special debug and z for debug
**  03-24-87  don't optimize ".o" files, but accept as ".a"
**  03-12-88  Added two pass (CoCo) compiler support.  Bill Dickhaus
**  09-23-90  Increased command-line buffer (parmbuf) to 4k, Eddie Kuns
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include "cc.h"
/*#include <module.h>*/

char findsuff();
void logo();
void splcat();
void trmcat();
void say_error();
void runit();
void chgsuff();

struct hlpmsg
{
    char *mnem,
         *help;
};

struct hlpmsg hlp[] = {
    { "-a",
        "Suppress assembly.  Leave output in \".a\" file."},
    {"-b=<path>",
        "Use <path> as the mainline module.  /dd/lib/cstart.r is the \n                  default if <path> is left off or this option isn't used."},
    {"-c",
        "Include C source & comments as comments in assembly code."},
    {"-d<NAME>",
        "Equivalent to #define <NAME> 1 in the preprocessor."},
    {"-d<NAME>=<STRING>",    "is equivalent to #define <NAME> <STRING>"},
    {"-e=n",     "Set edition number to n."},
    {"-f=<path>",
        "Use <path> as the output filename.  Filename part of <path>\n                  is used as the module name unless overridden with -n."},
    {"-l=<path>",    "Use <path> as a library during link phase."},
    {"-ll",          "Same as \"-l=/dd/lib/lexlib.l\""},
    {"-m=<n>",  "Instruct linker to add n pages to program's data size."},
    {"-m=<n>k",
            "Instruct linker to add n kilobytes to program's data size."},
    {"-M",        "Request Linker to generate a linkage map."},
    {"-n=<name>", "Use <name> as the module name."},
    {"-o",        "Skip optimization pass."},
    {"-O",        "Stop after optimization."},
    {"-P",        "Same as \" -p -l=/dd/lib/dbg.l \"."},
    {"-p",        "Invoke the execution profiler."},
    {"-q",        "Quiet mode (put all output in the file c.errors)."},
    {"-r",        "Suppress linking.  Stop at .r file."},
    {"-r=<dir>",
            "Suppress linking.  Stop at .r file, putting .r's in <dir>"},
    {"-s",        "Don't include stack checking code in the program."},
    {"-S",        "Ask linker to generate a symbol table."},
    {"-t",  "Use transcendental library clibt.l INSTEAD of normal clib.l."},
    {"-T",        "Put temporary files in the current directory."},
    {"-T=<dir>",
      "Put temporary files in <dir>.  Without this option, cc puts\n                  temporary files in the first of /tmp, /dd/tmp, or . which\n                  exists. (They are looked for in that order.)"},
    {"-w",
      "Waste this compile as an error check.\n                  Don't generate any object."},
    {"-x",   "Use current data directory for main library."},
    {"-z",   "Print, but do not execute, the compilation commands."},
    {0,      0}
};

static void help (int retval)
{
    struct hlpmsg *opt;

    opt = hlp;

    fprintf (stderr, "co_opt [opts] srcfile1 [srcfile2 ...] [opts]\n");
    fprintf (stderr, "  \nOPTIONS (case matters!)\n\n");

    while (opt->mnem)
    {
        fprintf (stderr, "%16s  %s\n", opt->mnem, opt->help);
        ++opt;
    }
    
    exit (retval);
}

void
cleanup ()
{
    /* We may need to insert a Windows call for here */
#ifndef WIN32
    if (childid)
    {
        kill (childid, 2);
    }
#endif

    if (newopath)
    {
        close (1);
        dup (newopath);
    }
    
    if (thisfilp)
        unlink (thisfilp);
    
    if (lasfilp)
        unlink (lasfilp);
}


int
trap (code)
     int code;
{
    cleanup ();
    exit (code);
}

int
main (argc, argv)
     int argc;
     char **argv;
{
    char **emp,
           suffix;
    int j,
        m,
        deltmpflg;
    register char *p;

    j = 0;
    
    while ((--argc > 0) && (++j < 100))
    {
        if (*(p = *++argv) == '-')
        {
            while (*++p)
            {
                switch (*p)
                {
                    case 'a':  /* stop at asm (no .r) (D) */
                        aflag = TRUE;
                        break;

                    case 'b':  /* use different "cstart" */
                        bflag = TRUE;
                        
                        if (*(p + 1) != '=')
                            break;
                        
                        strcpy (mainline, (p + 2));
                        goto saver;

                    case 'c':  /* include comments (C) */
                        cflag = TRUE;
                        break;

                    case 'd':  /* make identifier (define) (C) */
                        if (*(p + 1) == '\0')
                            goto saver;
                        
                        *--p = '-';
                        *(p + 1) = 'D';
                        macarray[maccnt++] = p;
                        goto saver;

                    case 'e':  /* set edition number */
                        emp = &edition;
                        goto emcommon;

                    case 'f':  /* set outfile path (L) */
                        if (*++p != '=')
                            goto saver;
                        
                        strcpy (objname, (p + 1));
                        
                        if (objname[0] == '\0')
                            goto saver;
                        
                        ++fflag;
                        suffix = findsuff (objname);
                        
                        if (suffix == 'c' || suffix == 'r')
                        {
                            say_error ("Suffix '.%c' not allowed for output",
                                   suffix);
                        }

                        goto saver;
                        /*page */
                    case 'l':  /* specify a library (L) */
                        if (*(p + 1) == 'l')
                        {
                            llflg++;
                            goto saver;
                        }
                        else if (*(p + 1) != '=')
                            goto saver;
                        
                        if (libcnt == 4)
                            say_error ("Too many libraries", "");
                        
                        *--p = '-';
                        libarray[libcnt++] = p;
                        goto saver;

                    case 'm':  /* set memory size */
                        emp = &xtramem;
                        *p &= 0x5f;
                      emcommon:
                        if ((*(p + 1)))
                        {
                            *--p = '-';
                            *emp = p;
                        }
                        
                        goto saver;

                    case 'M':  /* ask linker for link map (L) */
                        mflag = TRUE;
                        break;

                    case 'n':  /* give module a name (L) */
                        *--p = '-';
                        modname = p;
                        goto saver;

                    case 'o':  /* no optimizer (O) */
                        oflag = FALSE;
                        break;

                    case 'O':  /* stop after optimization */
                        o2flg = TRUE;
                        break;

                    case 'P':
                        p2flg = TRUE;   /* fall to set pflag too */

                    case 'p':  /* add profiler (C) */
                        pflag = TRUE;
                        break;

                    case 'q':  /* use quiet mode */
                        qflag = TRUE;
                        freopen ("c.errors", "w", stderr);
                        break;

                    case 'r':  /* stop at .r (no link) */
                        rflag = TRUE;
                        if (*++p != '=')
                            goto saver;
                        
                        strcpy (rlib, (p + 1));
                        
                        if (rlib[0] == '\0')
                            goto saver;
                        
                        strcat (rlib, "/");
                        goto saver;
                        /*page */
                    case 's':  /* no stack checking (C) */
                        sflag = TRUE;
                        break;

                    case 'S':  /* ask linker for symbol table (L) */
                        s2flg = TRUE;
                        break;

                    case 't':  /* use transendental library */
                        tflag = TRUE;
                        break;

                    case 'T':  /* use alternate (or NO) tmpdir */
                        if (*(p + 1) != '=')
                        {
                            tmpdir = FALSE;
                            break;
                        }
                        else
                        {
                            strcpy (tmpname, (p + 2));
                            strcat (tmpname, "/");
                        }
                        
                        goto saver;

                    case 'w':  /* waste the compile for error check */
                        nullflag = TRUE;
                        break;

                    case 'x':  /* use the work dir for main library */
                        xflag = TRUE;
                        break;

                    case 'z':
                        zflag = TRUE;
                        break;

                    case '?':
                    case 'h':
                        help (0);
                    default:
                        help (1);
                        say_error ("unknown flag : -%c\n", *p);
                }               /* end of switch */
            }                   /* end of inner while */
          saver:
            continue;
        }                       /* end of if */
        else
        {
            switch (suffix = findsuff (*argv))
            {
                case 'r':
                    rsufflg = 1;
                case 'a':
                case 'c':
                case 'o':
                    suffarray[filcnt] = suffix;
                    namarray[filcnt] = *argv;
                    ++filcnt;
                    break;

                default:
                    say_error ("%s : no recognised suffix", *argv);
            }                   /* end of switch */
        }                       /* end of else */
    }                           /* end of outer while */
    
    /*   command line now parsed, start real work   */

    logo ();                    /* identify us */
    
    if (filcnt == 0)
    {
        fprintf (stderr, "no files!\n");
        help (1);
    }

    if ((aflag + rflag) > 1)
        say_error ("incompatible flags", "");

    if (fflag)
    {
        if (filcnt > 1)
        {
            if (aflag || rflag)
            {
                say_error ("%s : output name not applicable", objname);
            }
        }
    }

    if (fflag == 0)
    {
        strcpy (objname, ((filcnt == 1) ? namarray[0] : "output"));
    }

    if ((tmpdir) && (*tmpname == '\0'))
    {
        for (loopcnt = 0; loopcnt < sizeof (tmproot) / sizeof (char *);
             loopcnt++)
        {
            if (access (tmproot[loopcnt], 0x83) == 0)
            {
                strcpy (tmpname, tmproot[loopcnt]);
                strcat (tmpname, "/");
                break;
            }
        }
    }

    strcat (tmpname, tmptail);

    mktemp (tmpname);
    strcat (tmpname, ".m");     /* add a suffix for chgsuff */

#ifdef OSK
    intercept (trap);
    dummy ();
#endif

    for (j = 0; j < filcnt; ++j)        /* for each file on cmd line */
    {
        fprintf (stderr, "'%s'\n", namarray[j]);
        
        if (suffarray[j] == 'c')
        {
            deltmpflg = 1;      /* is C prog so prep and compile it */
            strcpy (destfile, tmpname);
            chgsuff (destfile, 'm');
            frkprmp = parmbuf;
            
            if (cflag)
                splcat ("-l");  /* wants comments */
            
            if (edition)
                splcat (edition);       /* explicit edition number */
            
            for (m = 0; m < maccnt;)
                splcat (macarray[m++]); /* tack on "defines" */
            
            splcat (namarray[j]);       /* and now the file name */
            newopath = dup (1);
            close (1);
            
            if ((creat (destfile, 0666)) != 1)
                say_error ("can't create temporary file for '%s'", namarray[j]);
            
            trmcat ();
            thisfilp = 0;
            lasfilp = destfile;
            runit (PREPPER, 1);
            close (1);
            dup (newopath);
            close (newopath);
            newopath = 0;
            /*page */

            /* now compile it */

            if (TWOPASS)
            {
                strcpy (srcfile, destfile);
                frkprmp = parmbuf;
                thisfilp = srcfile;
                splcat (srcfile);
                
                if (sflag)
                    splcat ("-s");      /* no stack checking */
                
                strcpy (destfile, tmpname);
                chgsuff (destfile, 'a');
                strcpy (ofn, "-o=");
                strcat (ofn, destfile);
                splcat (ofn);
                
                if (pflag)
                    splcat ("-p");      /* profiler code */
                
                trmcat ();
                lasfilp = destfile;
                runit ("c.pass1", 0);
                unlink (srcfile);
            }

            strcpy (srcfile, destfile);
            frkprmp = parmbuf;
            thisfilp = srcfile;
            splcat (srcfile);
            
            if (sflag)
                splcat ("-s");  /* no stack checking */
            
            if (nullflag)
            {
                if ( ! TWOPASS)
                {
                    splcat ("-n");      /* waste the output */
                }

                strcpy (destfile, "/dev/null");
                deltmpflg = 0;
            }
            else
            {
                if (aflag)
                {
                    if (fflag)
                    {
                        strcpy (destfile, objname);     /* explicit obj name */
                    }
                    else
                    {
                        strcpy (destfile, namarray[j]);
                        chgsuff (destfile, 'a');
                    }
                }
                else
                {
                    chgsuff (destfile, 'i');
                }
            }

            strcpy (ofn, "-o=");
            strcat (ofn, destfile);
            splcat (ofn);
            
            if (pflag)
            {
                splcat ("-p");  /* wants profiler code */
            }
            
            trmcat ();
            lasfilp = destfile;

            if (TWOPASS)
            {
                runit ("c.pass2", 0);
            }
            else
            {
                runit (COMPILER, 0);
            }

            unlink (srcfile);
        }

        else
        {
            deltmpflg = 0;
        }

        /* now assemble and perhaps optimize it */
        
        if (aflag || nullflag || (suffarray[j] == 'r'))
        {
            lasfilp = 0;        /* is .r so no work to do */
        }
        else
        {
            /* don't optimize ".a" or ".o" files */
            
            if ((suffarray[j] == 'a') || (suffarray[j] == 'o'))
            {
                strcpy (srcfile, namarray[j]);
                thisfilp = 0;
            }
            else
            {
                strcpy (srcfile, destfile);
                thisfilp = srcfile;
                
                if (oflag)
                {
                    frkprmp = parmbuf;  /* yes,  optimize it */
                    splcat (srcfile);
                    
                    if ((filcnt == 1) && (o2flg == 0))
                    {
                        strcpy (destfile, tmpname);
                        chgsuff (destfile, 'm');
                    }
                    else
                    {
                        if (fflag && o2flg)
                        {
                            strcpy (destfile, objname);
                        }
                        else
                        {
                            chgsuff (namarray[j], 'o');
                            strcpy (destfile, namarray[j]);
                        }
                    }
                    splcat (destfile);
                    trmcat ();
                    lasfilp = destfile;
                    runit ("co_opt", 0);
                    
                    if (deltmpflg)
                    {
                        unlink (srcfile);
                    }

                    strcpy (srcfile, destfile);
                    thisfilp = srcfile;
                }
            }
            if (o2flg == 0)
            {
                frkprmp = parmbuf;
                splcat (srcfile);
                
                if ((filcnt == 1) && (rflag == 0))
                {
                    strcpy (destfile, tmpname);
                    chgsuff (destfile, 'r');
                }
                else
                {
                    if (fflag && rflag)
                    {
                        strcpy (destfile, rlib);
                        strcat (destfile, objname);
                    }
                    else
                    {
                        chgsuff (namarray[j], 'r');
                        strcpy (destfile, rlib);
                        strcat (destfile, namarray[j]);
                    }
                }
                
                strcpy (ofn, "-o=");
                strcat (ofn, destfile);
                splcat (ofn);
                trmcat ();
                lasfilp = destfile;
                runit (ASSEMBLER, 0);
                
                if (deltmpflg)
                {
                    unlink (srcfile);
                }
            }
        }
    }                           /* end of for each file */
    if (nullflag || aflag || rflag || o2flg)
    {
        exit (0);
    }

    /* now link all together */
    frkprmp = parmbuf;
    
/*    if ((p = chkccdev ()) == 0)
        say_error ("Cannot find default system drive");*/
    p = "/dd";    /* Temporary fix */
    
    if (bflag)
        strcpy (ofn, mainline); /* use cstart.r or whatever */
    else
        strcat (strcat (strcpy (ofn, p), "/lib/"), mainline);   /* global */
    
    splcat (ofn);
    
    if ((filcnt == 1) && (suffarray[0] != 'r'))
        splcat (thisfilp = destfile);
    else {
        for (thisfilp = 0, j = 0; j < filcnt; ++j)
        {
            splcat (namarray[j]);
        }
    }

    strcpy (ofn, "-o=");
    
    if (fflag == 0)
        chgsuff (objname, 0);
    
    strcat (ofn, objname);
    splcat (ofn);
    
    for (j = 0; j < libcnt; j++)
        splcat (libarray[j]);
    
    if (llflg)
    {
        strcat (strcat (strcpy (ofn, "-l="), p), "/lib/lexlib.l");
        splcat (ofn);
    }

    if (p2flg)
    {
        strcat (strcat (strcpy (ofn, "-l="), p), "/lib/dbg.l");
        splcat (ofn);
    }
    
    strcpy (ofn, "-l=");
    
    if (xflag == 0)
        strcat (strcat (ofn, p), "/lib/");
    
    strcat (ofn, (tflag) ? "clibt.l" : "clib.l");
    splcat (ofn);
    
    if (modname)
        splcat (modname);
    
    if (xtramem)
        splcat (xtramem);
    
    if (edition)
        splcat (edition);
    
    if (mflag)
        splcat ("-m");
    
    if (s2flg)
        splcat ("-s");
    
    trmcat ();
    lasfilp = 0;
    runit (LINKER, 0);
    cleanup ();
    return 0;
}

void
runit (cmd, code)
     char *cmd;
     int code;
{
    char shellstr[500];

    /*   fprintf(stderr, "   %-6s:\n", cmd); */
    fprintf (stderr, "   %6s:  %s", cmd, parmbuf);
    
    if (zflag)
        return;

#ifdef OSK
    if ((childid = os9fork (cmd, frkprmsiz, parmbuf, 1, 1, 0)) < 0)
        say_error ("cannot execute %s", cmd);
    
    wait (&childstat);
    childid = 0;
    
    if (childstat > code)
        trap (childstat);
#else
    strcpy (shellstr, cmd);
    strcat (shellstr, parmbuf);

    if ((childid = system (shellstr)) == -1)
    {
        char msg[50] = "Could not fork ";

        strcat (msg, cmd);
        perror (msg);
        exit (EXIT_FAILURE);
    }
    else if (childid) {
        char msg[50];

        strcpy (msg, cmd);
        strcat (msg, " failed");
        perror (msg);
        exit (EXIT_FAILURE);
    }
#endif
}


/*chkccdev ()
{
    char *s,
      c;
    register char *p;
    struct mod_exec *q;
    struct mod_config *r;

    if ((q = modlink ("ccdevice", 4, 0)) != -1)
    {
        strcpy (devnam1, (char *) q + q->m_data);
        munlink (q);
        return (devnam1);
    }
    else
    {
        if ((r = modlink ("Init", 0x0c, 0)) != -1)
        {
            s = (char *) r + r->m_sysdrive;
            p = devnam1;
            
            while ((c = *s++) > 0)
                *p++ = c;
            
            *p++ = (c & 0x7f);
            *p = 0;
            munlink (r);
            return (devnam1);
        }
    }
    return (0);
}*/

void
say_error (format, arg)
     char *format;
     void *arg;
{
    logo ();                    /* print logo if not done yet */
    fprintf (stderr, format, arg);
    putc ('\n', stderr);
    trap (0);
}

void
chgsuff (s, c)
     char *s,
       c;
{
    register char *p = s;

    while (*(p++))
        ;
    
    if (*(p - 3) != '.')
        return;
    
    if (c == '\0')
    {
        *(p - 3) = 0;
    }
    else
    {
        *(p - 2) = c;
    }
}

char
findsuff (p)
     register char *p;
{
    int j;
    char c;

    j = 0;
    
    while ((c = *(p++)))
        if (c == '/')
            j = 0;
        else
            j++;
    
    if (j <= 29 && j > 2 && *(p - 3) == '.')
    {
        return (*(p - 2) | 0x40);
    }
    else
    {
        return (0);
    }
}

void
splcat (s)
     char *s;
{
    register char *p = s;

    *(frkprmp++) = 0x20;

    while ((*(frkprmp++) = *(p++)))
        ;
    
    --frkprmp;
}

void
trmcat ()
{
    *frkprmp++ = '\n';
    *frkprmp = '\0';
    frkprmsiz = frkprmp - parmbuf;
}

void
dummy ()
{
}


void
logo ()
{
    if (hello == 0)
        fprintf (stderr, "\n cc version %d.%d.%d\n", VERSION, MAJREV, MINREV);
}
