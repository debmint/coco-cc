/* ************************************************************************ *
 * longs.c - part 6 for c.comp                                              $
 *                                                                          $
 * Part 6 of modules to build c.comp                                        $
 *                                                                          $
 * $Id:: longs.c 94 2014-05-04 22:34:25Z David                              $
 * ************************************************************************ */

#include "cj.h"

#define swap(x,y)      {expnode *t;t=x;x=y;y=t;}
/*#ifndef COCO
int *
#ifdef __STDC__
split_long(int *arr, int val)
#else
split_long(arr, val)
    int *arr; val;
#endif
{
    short x;
    arr[0] = (x = (val >> 16));
    arr[1] = (x = val & 0xffff);
    return arr;
}
#endif*/

void
#ifndef __STDC__
lload(ptr)
expnode *ptr;
#else
lload(expnode *ptr)
#endif
{
    tranlexp(ptr);
    getadd(ptr);
}

void
#ifndef __STDC__
tranlexp(node)
register expnode *node;
#else
tranlexp(expnode *node)
#endif
{
    expnode *p;
    int op;
    int s;

    switch (op = node->op) {
        case FREG:             /* L3894 */
        case XIND:
        case YIND:
        case UIND:
        case NAME:
            return;             /* Do nothing for index regs or USRLBL */
        case STAR:             /* L34ee */
            dostar(node);
            getinx(node);

            switch (node->op) {
                case XIND:     /* L3500 */
                case YIND:
                case UIND:
                    if (node->val.num) {
                        gen(LEAX, NODE, (int) node NUL1);
                        node->op = XIND;
                        node->val.num = 0;
                        return;
                    }
                default:       /* L3894 */
                    return;
            }

            /*return; */

        case UTOL:
            /* L3530 */
            lddexp(node->left);
            gen(LONGOP,UTOL NUL2);
            goto vty2flacc;
        case DTOL:
            /* L353f */
            dload(node->left);
            gen(DBLOP,DTOL,(int)(node->left) NUL1);
            goto vty2flacc;
        case ITOL:             /* L355e */
            lddexp(node->left);
            gen(LONGOP,ITOL NUL2);
vty2flacc:
            node->op = FREG;
            return;
        case LCONST:
            /* L3582 */
            gen(LONGOP,LCONST, node->val.num NUL1);
            goto L3771;
        case QUERY:
            /* L3597 */
            doquery(node,lload);
            goto L3771;
        case INCBEF:           /* L35a4 */
        case DECBEF:
        case COMPL:
        case NEG:
            lload(node->left);
            gen(LONGOP, op NUL2);
            goto L3771;
        case INCAFT:           /* L35c0 */
        case DECAFT:
            gen(LOADIM,XREG,FREG,0);
            gen(PUSH, XREG NUL2);
            lload(node->left);
            gen(LONGOP, op NUL2);
            gen(LONGOP, MOVE NUL2);
            gen(LONGOP, ((op == INCAFT) ? DECAFT : INCAFT) NUL2);
            goto vty2flacc;

        case CALL:             /* L3621 */
            docall(node);
            goto vty2flacc;
        case TIMES:
            /* L362b */
            if (node->left->op == LCONST) swap(node->left,node->right);
            if (node->right->op == LCONST) {
                p = (expnode *)((node->right)->val.lp);
                /* L3696 */
                if ((p->type == 0) && (s = isashift(p->size))) {
                    p = node->right;
                    /*  should free constant storage here  */
                    p->val.num = s;
                    p->op = CONST;
                    p->type = INT;
                    op = (op == TIMES) ? SHL : SHR;
                    goto shifts;
                }
            }

        case DIV:              /* L3696 */
        case EQ:
        case NEQ:
        case GEQ:
        case LEQ:
        case GT:
        case LT:
        case PLUS:
        case MINUS:
        case MOD:
        case AND:
        case OR:
        case XOR:
            p=node->left;

            if (p->op == LCONST)
                pushslong(p);
            else {
                lload(p);
                gen(LONGOP,STACK NUL2);
            }

            lload(node->right);
            gen(LONGOP,op NUL2);
            goto vty2flacc;
        case SHL:              /* L36d2 */
        case SHR:
shifts:
            lload(node->left);
            gen(PUSH,XREG NUL2);
            lddexp(node->right);
            gen(LONGOP,op NUL2);
            goto vty2flacc;
        case ASSIGN:
            lload(node->left);
            gen(PUSH,XREG NUL2);
            lload(node->right);
            goto L375e;
L372b:
            lload(p = node->left);
            gen(PUSH, XREG NUL2);
            node->op = op - 80;
            p->op = XIND;
            tranlexp(node);
L375e:
            gen(LONGOP, MOVE NUL2);
L3771:
            node->op = XIND;
            node->val.num = 0;
            return;
        default:
            if (op >= ASSPLUS) {
                goto L372b;
            }

            comperr(node,"longs");
            return;
    }
}

void
#ifndef __STDC__
getadd(ptr)
expnode *ptr;
#else
getadd(expnode * ptr)
#endif
{
    switch (ptr->op) {
        case NAME:             /* L38a6 */
            gen(LOADIM, XREG, NODE, ptr);
            break;
        case YIND:             /* L38c0 */
        case UIND:
            gen(LEAX, NODE, (int) ptr NUL1);
            break;
    }
}

/* **************************************************************** *
 * pushslong () - Push the 4-byte value of a long onto the stack    *
 * **************************************************************** */

void pushslong(p)
register expnode *p;
{
    register LONGTYPE *p1 = p->val.lp;

    /*_lng.l = (long *)(p->cmdval);  */

    if (*p1) {                  /* else L3947 */
        gen(LOAD, DREG, CONST, ((expnode **)p1)[1]);
        gen(PUSH, DREG NUL2);
        gen(LOAD, DREG, CONST, *(expnode **)p1);    /* go to L396e */
    } else {
        gen(LOAD, DREG, CONST, 0);
        gen(PUSH, DREG NUL2);
    }

    gen(PUSH, DREG NUL2);
}
