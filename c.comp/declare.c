/* **************************************************************** *
 * comp_14.c - part 14 for c.comp                                   *
 * comes from p1_05.c                                               *
 *                                                                  *
 * This file contains the "head" routines.  That is, the mainline   *
 * routines for parsing a file.  These routines call initialize     *
 * the expnode for a variable or routine, and do the primary setups *
 *                                                                  *
 * $Id:: comp_14.c 73 2008-10-03 20:20:29Z dlb                $     *
 * **************************************************************** */

#include "cj.h"

extern symnode *freesym;
static direct int reguse;
/*static*/ direct struct initstruct *initlist, *initlast, *initfree;

/* Following are function prototypes for static vars */

static int setclass();
static void sizundef();
static void argdef();
static void blkdef();

static int declrcmp(
#ifdef __STDC__
                       symnode *, int, elem *
#endif
    );

static void newfunc(
#ifdef __STDC__
                          symnode *, int
#endif
    );

static void declist(
#ifdef __STDC__
    symnode **
#endif
    );

static int shiftin(
#ifdef __STDC__
                        int ftype, int depthflgs
#endif
    );


static int set_regclass(
#ifdef __STDC__
                           int sclass, int genty
#endif
    );


/* ******************************************************************** *
 * funcmain () - The mainline function for processing a command or      *
 *          function.  This function is called from main() to process   *
 *          each command.                                               *
 *          When a function is encountered, control branches out into   *
 *          functions that handle parameters, block vars, and commands  *
 *          within the function.  Control returns to funcmain () after  *
 *          the end of the function                                     *
 *                                                                      *
 * On entry, we have already run getsym().  Any "^#" directives have  *
 * already been taken care of, so now we are at the begin of a variable *
 * definition, function prototype, or function entry.                   *
 *                                                                      *
 * If it's a variable def, extern def, or function prototype,           *
 * funcmain () process that command line (until a semicolon is          *
 * encountered), and then returns to main ().                           *
 * ******************************************************************** */

void
#ifdef __STDC__
funcmain(void)
#else
funcmain()
#endif
{
    int size;
    int _varsz;
    int real_ftyp;
    dimnode *tdp;
    int _vt_ftyp;
    int type;
    int temp;
    dimnode *dimptr;
    symnode *ptemp;
    elem *eptr;

    register symnode *ptr;      /* I'm sure this is correct */

    /* This is my addition.  It seems that c.comp (even on the COCO)
     * erroneously reports an error when multiple ";"'s are encountered
     * outside any blocks.  This doesn't seem to happen when inside a
     * block.  All the other compilers I use (gcc, c68) permit this,
     * which is definitely not illegal.
     */

    /* FIXME
     * Second thought...  it _could_ be useful to _not_ allow this,
     * although it _is_ legal, and although this is a bug, it could be
     * used as a "feature".  Where I noticed this was in my header
     * file and for an #ifdef COCO... I'd placed a semicolon outside
     * a second-level #ifdef block when it belonged inside the first level...
     * Oh, well... if anyone thinks it's better to eliminate this, it can
     * be done.
     */

    /* eliminate patch for code check
       while (*symptr == ';')
       {
       getsym ();
       } *//* end of patch */

    /* We never see either Brace here.  Blocks are handled further down */

    while (sym == RBRACE) {
        error("too many brackets");
        getsym();
    }

    /* If it's a left brace, it's illegal at this point.
     * Process the block and return.
     */

    if (sym == LBRACE) {
        error("function header missing");
        /* I believe the parameter 0 for block() is invalid.  We'll
         * leave it like this till we verify that we have correct code
         */
#ifdef COCO
        block(0);
#else
        block();
#endif
        clear(&labelist);
        getsym();
        return;
    }

    switch (_vt_ftyp = setclass()) {
        case REG:
        case AUTO:
            /* At this point, we're still outside the function
             * we cannot have register or auto types */

            error("storage error");
            /* Fall through to next case */
        case 0:
            /* We didn't have a type definition, default to EXTDEF */
            _vt_ftyp = EXTDEF;
    }

    if (!(type = settype(&size, &dimptr, &eptr))) {  /* else L8228 */
        type = INT;
    }

    /* L8228 */
    /* We should now be at the variable (or function) name,
     * with its storage class (if any) defined.
     * The following loop will process the variable (or extern) def,
     * and if it is a comma-separated list of names, all these.
     * If it's a function, call newfunc () to process the whole function
     */

    for (;;) {
        tdp = dimptr;           /* Non-zero if it's a typedef ? */
        temp = declarator(&ptemp, &tdp, type);

        if (!(ptr = ptemp)) {   /* else L825c */
            if ((temp != STRUCT) && (temp != UNION)) {
                identerr();
            }

            goto L841c;
        }

        if (isftn(temp)) {      /* else L8281 */
            if ((sym == COMMA) || (sym == SEMICOL)) {
                real_ftyp = EXTERN;     /* function prototype */
            } else {
                real_ftyp = EXTDEF;     /* parameter assignment */
            }
        } else {
            real_ftyp = _vt_ftyp;       /* Any variable *//* L8283 */
        }

        if (ptr->type) {        /* else L82cf */
            /* 0 = match */
            if (!(declrcmp(ptr, temp, eptr)) && (real_ftyp != EXTERN)) {        /* else L83a3 */
                if ((ptr->storage != EXTERN) && (ptr->storage != EXTERND)) {    /*else L82be */
                    multidef();
                } else {
                    ptr->storage = real_ftyp;   /* L82be */
                    ptr->dimptr = tdp;
                    ptr->x.elems = eptr;
                    goto L82e1;
                }
            }
        } else {                /* ptr->gentyp = 0 --- new definition? */
            /* L82cf */
            ptr->type = temp;
            ptr->blocklevel = 0;
            ptr->storage = real_ftyp;
            ptr->x.elems = eptr;
          L82e1:
            _varsz = sizeup(ptr, tdp, size);

            if (!(isftn(temp))) {       /* if not function */
                /* else L83a3 */
                if (sym == ASSIGN) {    /* assignment */
                    do_initvar(ptr, real_ftyp, temp);
                } else {
                    if (!_varsz && (real_ftyp != EXTERN)) {     /* L831f */
                        sizundef();
                    } else {    /* It's a legal var. print to output stream */
                        switch (real_ftyp) {
                            case STATICD:      /* L8336 */
                            case STATIC:
                                extstat(ptr, _varsz,
                                          real_ftyp == STATICD);
                                break;
                            case DIRECT:       /* L8353 */
                            case EXTDEF:
                                defglob(ptr, _varsz,
                                          real_ftyp == DIRECT);
                                break;
                        }
                    }
                }

                /* L8389 */

                if (real_ftyp == STATIC) {
                    ptr->storage = EXTDEF;
                } else {
                    if (real_ftyp == STATICD) {
                        ptr->storage = DIRECT;
                    }
                }
            }
        }

        /* L83a3 */

        if (isftn(temp)) {      /* else L841f */
            temp = decref(temp);

            /* Illegal return type for function */

            if ((isary(temp)) || (isftn(temp)) ||
                (temp == STRUCT) || (temp == UNION)) {
                error("function type error");
                ptr->type = 0x30 + INT; /* isftn + INT */
                temp = INT;
            }

            if (real_ftyp == EXTERN) {  /* prototype */
                /* L83f4 */
                clear(&arglist);
            } else {            /* it's a real function, process */
                FuncGentyp = temp;      /* L8409 */
                newfunc(ptr, _vt_ftyp);
                return;
            }
        }
      L841c:
        if (sym == COMMA) {
            getsym();
        } else {
            break;
        }
    }                           /* end of "for (;;)" at L8228 */

    if (need(SEMICOL)) {        /* _21 ( L842d ) */
        junk();
    }
}


static void
#ifdef __STDC__
argdef(void)
#else
argdef()
#endif
{
    int sclass;
    int type;
    dimnode *tdp, *dimptr;
    int size, ffcod, temp;
    symnode *ptemp;
    elem *eptr;
    register symnode *ptr;

    switch ((sclass = setclass())) {
        default:               /* L8456 */
            error("argument storage");
            /* fall through to next case to reset to a valid storage class */
        case 0:                /* L8461 */
            sclass = AUTO;
        case REG:              /* Only valid storage class for func args */
            break;
    }

    if (!(type = settype(&size, &dimptr, &eptr))) {  /* L8474 */
        type = INT;             /* default */
    }

    /* We have the type, and are now poised on the variable name(s) */

    for (;;) {                  /* Loop through for all comma-separated varnames */
        /* L848e */
        tdp = dimptr;
        temp = declarator(&ptemp, &tdp, type);
        ptr = ptemp;            /* decl ptr to register */

        if (isftn(temp) || (temp == STRUCT) || (temp == USTRUCT)) {
            error("argument error");
            goto namedone;
        } else {
            if (isary(temp))
                temp = incref(decref(temp));    /* go to L84f5 */
            else {
                if (temp == FLOAT) {    /* Promote */
                    temp = DOUBLE;
                }
            }

            if (ptr == 0) {
                identerr();
                goto namedone;
            }
        }

        ffcod = set_regclass(sclass, temp);     /* L8506 */

        switch (ptr->storage) {
            case ARG:          /* L851a */
                ptr->type = temp;
                ptr->storage = ffcod;
                ptr->x.elems = eptr;

                if (!(sizeup(ptr, tdp, size))) {
                    sizundef();
                }

                break;

            case AUTO:
            case REG:
                /* L853e */
                multidef();
                break;
            default:
                error("not an argument");
        }

      namedone:
        if (sym == ASSIGN)
            initerr();

        if (sym != COMMA)
            break;
        getsym();
    }

    /* L857f *//* else L87e1 */
    if (need(SEMICOL))
        junk();
}

/* **************************************************************** *
 * blkdef () - Processes a label definitions within a block    *
 * **************************************************************** */

static void
#ifdef __STDC__
blkdef(void)
#else
blkdef()
#endif
{
    int sclass;
    int type;
    int temp;
    dimnode *dimptr;
    int _varsiz;
    expnode *ep;
    symnode *ptemp;
    elem *eptr;
    initnode *i;

    switch (sclass = setclass()) {
        case DIRECT:           /* Cannot have "direct" local vars */
            error("storage error");
        case 0:                /* Default type is "auto" */
            sclass = AUTO;
    }

    if (!(type = settype(&_varsiz, &dimptr, &eptr))) {
        type = INT;
    }

    for (;;) {                  /* Process var name (or comma-separated list) */
        /* L85de */
        int stemp;
        int _loc_siz;
        dimnode *tdp;

        register symnode *ptr;

        tdp = dimptr;
        temp = declarator(&ptemp, &tdp, type);

        if (!(ptr = ptemp)) {   /* Not a USRLBL */
            /* else L861a */
            if ((temp != STRUCT) && (temp != UNION)) {  /* else L8685 */
                identerr();     /* go to L8666 */
            }

            goto L87c0;
        }

        if ((isftn(temp)) || (sclass == EXTERN)) {      /* L861a *//*else L8666 */
            if (!ptr->type) {   /* L862f */
                /* else L8654 */
                ptr->type = temp;
                ptr->storage = EXTERN;  /* 14 */
                ptr->blocklevel = 0;
                ptr->x.elems = eptr;
                sizeup(ptr, tdp, _varsiz);
            } else {
                declrcmp(ptr, temp, eptr);      /* L8654 */
            }

            goto L87c0;
        }

        stemp = set_regclass(sclass, temp);     /* L8666 */

        /* If it's a previously-defined name */

        if (ptr->type) {        /* else L8692 */
            if (ptr->blocklevel == blklev) {    /* Already a var in this block? */
                /* else L868b */
                multidef();
                goto L87c0;
            }

            pushdown(ptr);      /* reuse this label's storage */
        }

        ptr->type = temp;       /* L8692 */
        ptr->storage = stemp;   /* was EXTERN (switched from L862f */
        ptr->x.elems = eptr;
        ptr->blocklevel = blklev;       /* ??? Parentheses depth ??? */
        ptr->snext = vlist;
        vlist = ptr;

        if (!(_loc_siz = sizeup(ptr, tdp, _varsiz))) {
            if (sym != ASSIGN) {
                sizundef();
            }
        }

        switch (stemp) {
            case AUTO:         /* L86cc */
                stlev -= _loc_siz;
                ptr->offset = stlev;
                break;
            case STATICD:      /* L86d6 */
            case STATIC:
                ptr->offset = (++curlabel);
                break;
        }

        if (sym == ASSIGN) {    /* else L878f */
            /* L86f2 */
            if ((stemp == STATIC) || (stemp == STATICD)) {
                do_initvar(ptr, stemp, temp);   /* go to L87c3 */
            } else {
                getsym();       /* L871f */

                if ((!(isary(temp))) && (temp != STRUCT) && (ep = parsexp(CHAR))) {     /* else L878a */
                    if (initfree) {     /* else L875e */
                        initfree = (i = initfree)->initnext;
                        i->initnext = 0;
                    } else {
                        /*i = grab (6); */
                        i = (initnode *)grab(sizeof(initnode));
                    }

                    i->initp = ep;
                    i->initname = ptr;

                    /* Chain structs or initialize pointer */

                    if (initlist) {
                        initlast->initnext = i;
                    } else {
                        initlist = i;
                    }

                    initlast = i;
                } else {
                    initerr();  /* L878a */
                }
            }
        } else {
            switch (stemp) {
                case STATIC:   /* L8793 *//*15 */
                case STATICD:
                    locstat(ptr->offset, _loc_siz, (stemp == STATICD));
                    break;
                default:       /* L87c3 */
                    break;
            }
        }

      L87c0:
        if (sym != COMMA) {     /* L87c3 */
            break;
        }

        getsym();
    }                           /* End of the for (;;) loop holding 6 bytes of data */

    need(SEMICOL);
}

static int
#ifdef __STDC__
declrcmp(register symnode * ptr, int type, elem * eptr)
#else
declrcmp(ptr, type, eptr)
register symnode *ptr;
int type;
elem *eptr;
#endif
{
    if ((ptr->type != type)
        || ((type == STRUCT) && (ptr->x.elems != eptr))) {
        error("declaration mismatch");
        return 1;
    }

    return 0;
}

/* ******************************************************************** *
 * set_regclass () - Sets REG storage-class variables if        *
 *          applicable.                                                 *
 * Passed:  (1) sclass - original storage class for variable            *
 *          (2) genty - The gentyp for the variable                     *
 * Returns: func-code. Unchanged if not register variable               *
 *          UREG if reguse was 0 (no prev reg var declared)      *
 *              and var is INT, LONG, or anything not a pointer         *
 *          YREG if reguse was < 0 ???                           *
 *          AUTO on anything else                                    *
 *          Increments reguse by one                                *
 * ******************************************************************** */

static int
#ifdef __STDC__
set_regclass(int sclass, int genty)
#else
set_regclass(sclass, genty)
int sclass;
int genty;
#endif
{
    if (sclass == REG) {        /* else L886b */
        if (reguse < 1) {   /* else L8866 */
            switch (genty) {
                default:       /* L8836 */
                    if (!isptr(genty)) {
                        return AUTO;
                    }

                case INT:      /* L8841 */
                case UNSIGN:
                    /* YREG if reguse was < 0 ...
                     * When would this happen??
                     */

                    return ((++reguse == 1) ? UREG : YREG);
            }
        }

        return AUTO;
    }

    return sclass;
}

static void
#ifdef __STDC__
newfunc(symnode * fptr, int sclass)
#else
newfunc(fptr, sclass)
symnode *fptr;
int sclass;
#endif
{
    int offset;
    int rg_fncode;
    register symnode *p1;
    symnode *p2;
    char *cp;
    int prlab;

    blklev = 1;
    callflag = reguse = stlev = maxpush = sp = 0;

    while (sym != LBRACE)
    {     /* L888f */
		/* Pick up K&R style function arguments here*/
        argdef();
    }

    cp = fptr->sname;

    if (pflag)
    {
        profname(cp, (prlab = ++curlabel));
    }

    startfunc(cp, (sclass != STATIC), prlab);

    /* Now process local variables */

    p1 = arglist;
    offset = 4;             /* We already have return and u on stack */

    while (p1) {
        int __varsiz;           /* L88df */

        p1->offset = offset;

        switch (p1->type)
        {
            case LONG:         /* L88e9 */
            case FLOAT:
                __varsiz = FLOATSIZE;
                break;
            case DOUBLE:       /* L88ee */
                __varsiz = DOUBLESIZE;
                break;
            case CHAR:         /* L88f3 */
                ++(p1->offset);
            default:
                __varsiz = INTSIZE;
                break;
        }

        switch ((rg_fncode = p1->storage)) {
            case UREG:
                /* L8921 */
            case YREG:
                gen(rg_fncode, offset NUL2);
                break;
            case ARG:
                /* L8930 */
                p1->storage = AUTO;
                break;
        }

        offset += __varsiz;
        p1 = p1->snext;
    }

    p2 = arglist;
    arglist = NULL;

    block();                    /* go process commands within block */

    clear(&p2);
    clear(&labelist);

    if (lastst != RETURN)
    {
        gen(RETURN, 0, 0 NUL1);
    }

    lastst = 0;
	/* generate stack reservation */
    endfunc();
    blklev = 0;

    if (sym == EOF)
    {
        error("function unfinished");
    }

    getsym();
}


void
#ifdef __STDC__
block(void)
#else
block()
#endif
{
    register expnode *p;
    struct initstruct *i;
    symnode *varlist;
    int savlev;

    varlist = vlist;
    vlist = NULL;
    getsym();
    ++blklev;
    savlev = stlev;


    while (issclass() || istype())
    {    /* L89dc */
        blkdef();
    }

    if (maxpush > stlev)
    {
        maxpush = stlev;
    }

    sp = modstk(stlev);

    while (initlist)
    {          /* L8a75 */
        i = initlist;
        /* L8a05 */
        p = i->initp;
        p = newnode(NAME,0,0,(int)(i->initname),p->lno,p->pnt);
        p = newnode(ASSIGN,p,i->initp,0,p->lno,p->pnt);
        reltree(tranexp(optim(p)));
        i = i->initnext;
        initlist->initnext = initfree;
        initfree = initlist;
        initlist = i;
    }


    while ((sym != RBRACE) && (sym != EOF))
    {
        statement();
    }

    clear(&vlist);
    vlist = varlist;
    --blklev;
    stlev = savlev;

    sp = (lastst != RETURN) ? modstk(savlev) : savlev;
}

static void
#ifdef __STDC__
declist(symnode **list)
#else
declist(list)
symnode **list;
#endif
{
    register symnode *ptr,*last;

    *list = NULL;


    for (;;)
    {
        getsym();

        if (sym == RPAREN)
        {    /* else L8b45 */
            break;
        }

        if (sym == NAME)
        {      /* else L8b38 */
            ptr = (symnode *)symval;

            if (ptr->storage == ARG)
            {
                error("named twice");
            }
            else
                if (ptr->type != UNDECL)
                {
                    pushdown(ptr);
                }

            ptr->type = INT;
            /* L8b09 */
            ptr->storage = ARG;
            ptr->blocklevel = 1;
            ptr->size = 2;

            if (*list)
            {
                last->snext = ptr;
            }
            else
            {
                *list = ptr;
                /* L8b29 */
            }

            ptr->snext = NULL;
            last = ptr;
            getsym();
        }
        else
        {
            identerr();
        }

		if (sym != COMMA)
		{
			break;
		}
    }

    need(RPAREN);               /* L8b45 */
}


static int
#ifdef __STDC__
setclass(void)
#else
setclass()
#endif
{
    int class;

    if (issclass())
    {           /* else L8b9a */
        class = symval;
        getsym();

        if ((sym == KEYWORD) && (symval == DIRECT)) {   /* else L8b96 */
            switch(class)
            {
                case STATIC:
                    /* L8b7d */
                    class = STATICD;
                    break;
                case EXTERN:
                    class = EXTERND;
                    break;
            }

            getsym();
        }

        return class;
    } else {
        return 0;
    }
}


int
#ifdef __STDC__
settype(int *size, dimnode **dimptr, elem **ellist)
#else
settype(size, dimptr, ellist)
int *size;
dimnode **dimptr;
elem **ellist;
#endif
{
    register symnode *ptr, *tagptr;
    int offset, msize, mtype, savflg, dtype;
    dimnode *dptr;
    int s;                /* size of one of a comma-separated var */
    elem *eptr, *elast, *elocal;
    int type = 0, tsize = 2;


    *ellist = 0;

    if (sym==KEYWORD)
    {       /* else L8ebc */
        switch ((type = symval))
        {      /* L8e7a */
            case SHORT:
                type = INT;

            case UNSIGN:       /* L8bd1 */
                getsym();


                if ((sym == KEYWORD) && (symval == INT)) {      /* else L8ee4 */
                    getsym();
                }

                break;

            case CHAR:
                /* L8bea */
                tsize = CHARSIZ;
            case INT:
                /* L8c22 */
                getsym();
                break;

            case LONG:         /* L8bef */
                getsym();
                tsize = LONGSIZE;

                if (sym != KEYWORD)
                {
                    break;
                }
                if (symval == INT)
                {
                    getsym();
                    break;
                }
				if (symval != FLOAT)
				{
					break;
				}
            case DOUBLE:
                /* L8c13 */
                type = DOUBLE;
                tsize = DOUBLESIZE;
                getsym();
                break;

            case FLOAT:
                /* L8c1d */
                tsize = FLOATSIZE;
                getsym();
                break;

default:
                /* L8c28 */
                type = 0;
                break;

            case UNION:
                /* L8c2f */
            case STRUCT:
                tsize = offset = 0;
                ++mosflg;
                tagptr = 0;
                getsym();
                --mosflg;

                if (sym == NAME)
                {
                    /* else L8cc8 */
                    tagptr= (symnode *)symval;

                    if (tagptr->type == UNDECL)
                    {
                        tagptr->type = USTRUCT;
                        tagptr->storage = STRTAG;
                    }
                    else
                        if (tagptr->storage != STRTAG)
                        {
                            error("name clash");
                        }
                    getsym();
                    /* L8c86 */
                    if (sym != LBRACE)
                    {
                        if (tagptr->type == STRUCT)
                        {
                            *size = tagptr->size;
                            *ellist = tagptr->x.elems;
                            return STRUCT;
                        }

                        *size = (int)tagptr;
                        /* L8cb0 */
                        return USTRUCT;
                    }

                    if (tagptr->type == STRUCT)
                    {
                        multidef();
                    }
                }

                if (sym != LBRACE)
                {    /* L8cc8 */
                    error("struct syntax");
                    /*break; */
                    *size = tsize;
                    *dimptr = 0;
                    return type;
                }

                ++mosflg;
                /* L8cde */
                do
                {            /* Loop for each struct member */
                    savflg = mosflg;
                    /* L8ce5 */
                    mosflg = 0;
                    getsym();
                    mosflg = savflg;

                    if (sym == RBRACE)
                    {        /* else L8e4d */
                        break;
                    }

                    mtype = settype(&msize, &dptr, &elocal);

                    while (1)
                    {
                        dimnode *tdptr;
                        symnode *ptemp;

                        tdptr = dptr;

                        if (sym == SEMICOL)
                        {
                            /* else L8e37 */
                            break;
                        }

                        ++blklev;
                        dtype = declarator(&ptemp, &tdptr, mtype);
                        ptr = ptemp;	 /* decl ptr to register */
                        --blklev;

                        if (ptr == NULL)
                        { /* else L8d5c */
                            identerr();
                            goto comma_or_brk;
                        }

                    if (ptr->type != UNDECL)
                        {        /* L8d5c */
                            /* else L8d91 */
                            if (ptr->blocklevel == blklev)
                            {    /*else L8d8a */
                                if ((ptr->type != dtype) ||
                                    (ptr->storage != MOS) ||
                                    (ptr->offset != offset)) {
                                    error("struct member mismatch");
                                }
                            }
                            else
                            {
                                pushdown(ptr);
                                /* L8d8a */
                            }
                        }

                        if (dtype == USTRUCT)
                        {  /* L8d91 */
                            error("undefined structure");
                        }

                        ptr->type = dtype;
                        ptr->storage = MOS;
                        ptr->offset = offset;
                        ptr->x.elems = elocal;

                        if ((s = sizeup(ptr, tdptr, msize))) {
                               /* else L8def */
                            if (type == STRUCT) {
                                tsize = (offset += s);
                            }
                            else
                            {
                                tsize = s > tsize ? s : tsize;
                            }
                        } else {
                            sizundef();
                        }

                        ptr->blocklevel = blklev;       /* L8df2 */
                        ptr->snext = vlist;
                        vlist = ptr;

                        if (type == STRUCT) {
                          /* else L8e2f */
                            eptr = (elem *) grab(sizeof(elem));
                            eptr->element = ptr;

                            if (*ellist) {
                                elast->strnext = eptr;
                            } else {
                                *ellist = eptr;     /* L8e21 */
                            }

                            elast = eptr;
                        }
comma_or_brk:
                        if (sym != COMMA) {
                            break;
                        }

                        getsym();
                        continue;

                    }           /* End of while (1) loop */
                }
                while (sym == SEMICOL);
                /* L8e43 */

                --mosflg;
                /* L8e4d */

                if (tagptr)
                {
                    tagptr->size = tsize;
                    tagptr->type = STRUCT;
                    tagptr->x.elems = *ellist;
                }

                need(RBRACE);
                break;
        }
    }
    else if ((sym == NAME) && ((ptr = (symnode *)symval)->storage == TYPEDEF)) {
    /* L8ebc */
        *size = ptr->size;
        *dimptr = ptr->dimptr;
        *ellist = ptr->x.elems;

        getsym();
        return ptr->type;
    }

    *size = tsize;
    *dimptr = NULL;

    return type;
}

int
#ifdef __STDC__
declarator(symnode ** ptr, dimnode ** dptr, int bastype)
#else
declarator(ptr, dptr, bastype)
symnode **ptr;
dimnode **dptr;
int bastype;
#endif
{
    int dtype, savmos;
    dimnode *p1;
    int count;
    dimnode *tempdim, *dummy;

#ifdef DEVEL
    dtype = *ptr = 0;
#else
    dtype = 0;
    *ptr = NULL;
#endif


    while (sym == STAR) {
          /* Increase pointer count */
        dtype = incref(dtype);
        getsym();
    }

    if (sym == NAME) {
        *ptr = (symnode *)symval;
        getsym();
    } else if (sym == LPAREN) {

        getsym();
        ++blklev;
        bastype = declarator(ptr,dptr,bastype);
        --blklev;
        need(RPAREN);
    }

    if (sym == LPAREN) {
        /* else L8fa1 */
        dtype = (dtype << 2) + FUNCTION;

        if (!blklev)
        {
            declist(&arglist);
        }
        else
        {
            declist((symnode **)(&dummy));
            clear((symnode **)&dummy);
            /* go to L0924 */
        }
    } else {
        register dimnode *p;

#ifdef COCO
        tempdim = count = p1 = 0;   /* L8fa1 */
#else
        tempdim = p1 = NULL;
        count = 0;
#endif

        savmos = mosflg;
        mosflg = 0;

        while (sym == LBRACK) {
            /* L9008 */
            dtype = (dtype << 2) + ARRAY;
            /* L8fb4 */
            getsym();
            p = (dimnode *) grab(sizeof(dimnode));

            if ((count == 0) && (sym == RBRACK)) {
                p->dim = 0;
            } else {            /* L8fde */
                p->dim = getconst(0);
            }

            if (p1) {
                p1->dptr = p;
            } else {
                tempdim = p;
                /* L8ff3 */
            }
            p1 = p;
            need(RBRACK);
            ++count;
        }

        mosflg = savmos;

        if (tempdim) {
            p->dptr = *dptr;
            *dptr = tempdim;
        }
    }

    return shiftin(bastype,dtype);
}


static int
#ifdef __STDC__
shiftin(int a, int b)
#else
shiftin (a,b)
int a, b;
#endif
{
    int temp = a;

    while (temp & XTYPE) {
        temp >>= 2;
        b <<= 2;
    }

    return (a + b);
}


int
#ifdef __STDC__
sizeup(register symnode * ptr, dimnode * dimptr, int size)
#else
sizeup(ptr,dimptr,size)
register symnode *ptr;
dimnode *dimptr;
int size;
#endif
{
    int temp;
    int n;

    switch ((temp = ptr->type) & 0x0f) {
        case CHAR:
            /* L9082 */
            n = CHARSIZ;
            break;
        case INT:              /* L9087 */
        case UNSIGN:
            n = INTSIZE;
            break;
        case LONG:             /* L908c */
        case FLOAT:
            n = LONGSIZE;
            break;
        case DOUBLE:           /* L9091 */
            n = DOUBLESIZE;
            break;
        case UNION:
        case STRUCT:
            n = size;
            break;
        case USTRUCT:
            n = 0;
            break;
    }

    ptr->size = n ? n : size;

    return getsize(temp, n,(ptr->dimptr = dimptr));
}


int
#ifdef __STDC__
getsize(int t, int size, dimnode * dptr)
#else
getsize(t,size,dptr)
int t, size;
register dimnode *dptr;
#endif
{
    int n;

    if ((isptr(t)) || (isftn(t))) {
        return INTSIZE;         /* L9117 */
    }

    if (isary(t)) {
        n = 1;

        do {
            n *= dptr->dim;
            /* L912d */
            dptr = dptr->dptr;
        } while (isary(t = decref(t)));

        return n * (isptr(t) ? POINTSIZE : size);
    }

    return size;
}

void
#ifdef __STDC__
clear(symnode ** list)
#else
clear(list)
symnode **list;
#endif
{
#ifndef COCO
    symnode **pp;
#endif
    symnode *p, *next;
    char err[60];

    register symnode *this = *list;

    while (this) {
        /* L922c */
        next = this->snext;

        if ((this->type == LABEL)) {
            if (!(this->x.labflg & 1)) {
                error(strncat(strcpy(err, "label undefined : "),
                              (this->sname), NAMESIZE));
            }
        }

        switch (this->storage) {
            case UREG:         /* L91c4 */
            case YREG:
                --reguse;
                break;
        }

#ifdef COCO
        /* L91d9 */
        p = this->downptr;

        if ((p > lobrk) && (p < hibrk)) {       /* else L91f4 */
            pullup(this);       /* go to L9229 */
        } else {
            if (this == p->type) {      /* L91f4 */
                p->type = this->hlink;
            } else {
                p = p->type;    /* L9202 */

                /* WHOLE LOOP STILL NOT RIGHT !!!!! */

                while (this != p->hlink) {
                    /* Just to get it to compile */
                    p = p->hlink;
                }

                p->hlink = this->hlink;
            }

            this->hlink = freesym;      /* L9222 */
            freesym = this;
        }
#else
        pp = (symnode **) this->downptr;

        if (((pp > hashtab) && (pp < hashtab + 128)) ||
            ((pp > mostab) && (pp < mostab + 128))) {
            if (*pp == this) {
                *pp = this->hlink;
            } else {
                for (p = *pp; p->hlink != this; p = p->hlink);
                p->hlink = this->hlink;
            }

            this->hlink = freesym;
            freesym = this;
        } else {
            pullup(this);
        }
#endif
        this = next;
    }                           /* end "while" */

    *list = 0;
}

static void
#ifdef __STDC__
sizundef(void)
#else
sizundef()
#endif
{
    error("cannot evaluate size");
}

void
#ifdef __STDC__
identerr(void)
#else
identerr()
#endif
{
    error("identifier missing");
}
