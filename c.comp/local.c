/* ************************************************************ *
 * ccomp_11.c                                                   *
 * Deals with outputting data to the output stream              *
 *                                                              *
 * comes from p1_10.c                                           *
 *                                                              *
 * $Id:: comp_11.c 73 2008-10-03 20:20:29Z dlb                $ *
 * ************************************************************ */

#include "cj.h"
#include <errno.h>

#define GBLB ':'    /* global asm declarator */
#define LCLB ' '    /* local asm declarator */
#ifndef COCO
#include <unistd.h>
#endif

static void defvar(
#ifdef __STDC__
                       symnode *, int, char
#endif
    );

static void dumpstrings();
void vsect(), endsect();

extern void os(), ot(), ol(), olbl(), nlabel();

void
#ifndef __STDC__
epilogue()
#else
epilogue(void)
#endif
{
    dumpstrings();
    endsect();

    if(errcount)
        ol("fail source errors");
}

void
#ifndef __STDC__
locstat(l,size,area)
int l, size, area;
#else
locstat(int l, int size, int area)
#endif
{
    vsect(area);
    olbl(l);
    fprintf(code," rmb %d\n",size);
    endsect();
}

void
#ifndef __STDC__
defglob(ptr, size, area)
symnode *ptr;
int size, area;
#else
defglob(symnode * ptr, int size, int area)
#endif
{
    vsect(area);
    defvar(ptr,size,GBLB);
    endsect();
}

void
#ifndef __STDC__
extstat(ptr,size,area)
symnode *ptr;
int size;
int area;
#else
extstat(symnode * ptr, int size, int area)
#endif
{
    vsect(area);
    defvar(ptr,size,LCLB);
    endsect();
}

static void
#ifndef __STDC__
defvar(ptr,size,scope)
symnode *ptr;
int size;
char scope;
#else
defvar(symnode * ptr, int size, char scope)
#endif
{
#ifdef COCO
    fprintf(code,"%.8s%c rmb %d\n",ptr->sname,scope,size);
#else
    fprintf(code, "%.12s%c rmb %d\n", ptr->sname, scope, size);
#endif
}

void
#ifndef __STDC__
profname(name,lab)
char *name;
int lab;
#else
profname(char *name, int lab)
#endif
{
    olbl(D005e = lab);
#ifdef COCO
    fprintf(code," fcc \"%.8s\"\n fcb 0\n",name);
#else
    fprintf(code, " fcc \"%.12s\"\n fcb 0\n", name);
#endif
}

void
#ifndef __STDC__
startfunc(name,flag,lab)
register char *name;
int flag, lab;
#else
startfunc(char *name, int flag, int lab)   /* label # (for profiler usage if applicable) */
#endif
{
#ifdef COCO
    fprintf(code," ttl %.8s\n",name);
#else
    fprintf(code, " ttl %.12s\n", name);
#endif
    nlabel(name,flag);
    ol("pshs u");

    if (!sflag)
        fprintf(code, " ldd #_%d\n lbsr _stkcheck\n",(stklab = ++curlabel));

    if(pflag) {
        /* else L5dff */
        ot("leax ");
        olbl(lab);
        os(",pcr\n pshs x\n leax ");
        on(name);
        os(",pcr\n pshs x\n lbsr _prof\n leas 4,s\n");
    }
}

void
#ifndef __STDC__
prt_profend()
#else
prt_profend(void)
#endif
{
    if (pflag) {
        fprintf(code,
                " pshs d\n leax _%d,pcr\n pshs x\n lbsr _eprof\n leas 2,s\n puls d\n",
                D005e);
    }
}

void
#ifndef __STDC__
endfunc()
#else
endfunc(void)
#endif
{
    /* generate stack reservation value */
    if(!sflag)
        fprintf(code,"_%d equ %d\n\n",stklab,maxpush-callflag-64);
}

void
#ifndef __STDC__
defbyte()
#else
defbyte(void)
#endif
{
    ot("fcb ");
}

void
#ifndef __STDC__
defword()
#else
defword(void)
#endif
{
    ot("fdb ");
}

void
#ifndef __STDC__
comment()
#else
comment(void)
#endif
{
    os("* ");
}

void
#ifndef __STDC__
vsect(area)
int area;
#else
vsect(int area)
#endif
{
    ol(area ? "vsect dp" : "vsect");
}

void
#ifndef __STDC__
endsect()
#else
endsect(void)
#endif
{
    ol("endsect");
}


static void
#ifndef __STDC__
dumpstrings()
#else
dumpstrings(void)
#endif
{
    register int c;

    if(strfile) {
        rewind(strfile);

        /* Should "-1" be "0" for COCO ??? */
        while ((c = getc(strfile)) != EOF) {
            putc(c,code);
        }

        if (ferror(strfile)) {
            fatal("dumpstrings");
        }

        fclose(strfile);
        unlink(strname);
#ifndef COCO
        strfile = 0;
#endif
    }
}

void
#ifndef __STDC__
tidy()
#else
tidy(void)
#endif
{
    int err = errno ? errno : 1;

    if(strfile) {
        fclose(strfile);
        unlink(strname);
    }

    _exit(err);
}

