/* ************************************************************************ *
 * comp_15.c part 15 for c.comp                                             *
 *                                                                          *
 * comes from first part of p2_03.c                                         *
 *                                                                          *
 * $Id:: comp_15.c 73 2008-10-03 20:20:29Z dlb                            $ *
 * ************************************************************************ */

#include "cj.h"

#ifndef __STDC__
static void tranrel();
static void checkop();
static int invrel();
static int revrel();
static int zeroconst();
#else
static void tranrel(int, expnode *, labstruc *, labstruc *, int nj);
static void checkop(expnode *);
static int invrel(int);
static int revrel(int);
static int zeroconst(expnode *);
#endif

void
#ifndef __STDC__
tranbool(node, tlab, flab, nj)
register expnode *node;
int tlab, flab, nj;
#else
tranbool(expnode *node, labstruc *tlab, labstruc *flab, int nj)
#endif
{
    int op;
    int second;

    switch(op=node->op){
        /* break = L9573 */
        case DBLAND:
            tranbool(node->left,(labstruc *)(second = ++curlabel), flab, 1);       /* L93b2 */
            goto l1;
        case DBLOR:
            tranbool(node->left, tlab, (labstruc *)(second = ++curlabel), 0);       /* L93d2 */
l1:
            label(second);
            tranbool(node->right, tlab, flab, nj);
            break;
        case NOT:
            /* L93fc */
            tranbool(node->left, flab, tlab, (1 - nj));
            break;
        case EQ:
        case NEQ:
        case LEQ:
        case LT:
        case GEQ:              /* L9411 */
        case GT:
        case ULEQ:
        case ULT:
        case UGEQ:
        case UGT:
            tranrel(op,node,tlab,flab,nj);
            break;
        case CONST:
        case FCONST:           /* L942b */
        case LCONST:
            if (node->val.num && (!nj)) {
                gen(JMP, (int)tlab, 0 NUL1);
            } else {
                if (nj && (!node->val.num)) {
                    gen(JMP, (int)flab, 0 NUL1);
                }
            }
            break;
        case COMMA:
            /* L9455 */
            tranexp(node->left);
            tranbool(node->right, tlab, flab, nj);
            break;              /* return */
        default:
            /* L9476  */
            if(islong(node)) {
                lload(node);

                /* output "lda 0,x\n ora 1,x\n ora 2,x\n ora 3,x"
                 * That is, test if == 0 */
                gen(LONGOP, TEST NUL2);
                /* go to L94d7 */
            } else {
                if ((node->type == FLOAT) || (node->type == DOUBLE)) {  /* else L94cc */
                    if (node->op == FTOD) {
                        node = node->left;
                    }

                    /* L94b0 */
                    dload(node);

                    /* output " lda %MSByte,x\n" */
                    gen(DBLOP, TEST, node->type NUL1);
                } else {
                    checkop(node);
                }
            }
#ifdef COCO
          L94d7:
#endif
            if (nj) {
                gen(CNDJMP, EQ, (int)flab NUL1);
            } else {
                gen(CNDJMP, NEQ, (int)tlab NUL1);
            }

            break;
    }
}

static void
#ifndef __STDC__
tranrel(op,node,tlab,flab,nj)
int op, nj;
expnode *node;
labstruc *tlab,*flab;
#else
tranrel(int op, expnode * node, labstruc * tlab, labstruc * flab, int nj)
#endif
{
    int temp;
    labstruc *destin;
    expnode *lhs = node->left;

    register expnode *rhs = node->right;

    destin = nj ? flab : tlab;

    op = nj ? invrel(op) : op;

    if (islong(lhs)) {
        tranlexp(node);
        goto usual;
    }
    else if (isfloat(lhs)) {
        /* L95be */
        trandexp(node);
        goto usual;

    }

    if ((zeroconst(lhs)) ||
        ((isaleaf(lhs)) && (!isaleaf(rhs))) ||
        ((isreg(rhs->op)) && (!isreg(lhs->op)))
        ) {                     /* else L9626 */
        /* Swap lhs with rhs */
        temp = (int)lhs;         /* L9613 */
        lhs = rhs;
        rhs = (expnode *)temp;
        op = revrel(op);
#ifdef COCO
#undef __tmpref
#endif
    }

    if (isreg(temp=lhs->op)) {
        /* else L96a9 */
        tranexp(rhs);

        switch (rhs->op) {
            case AMPER:        /* L9642 */
                loadexp(rhs);
                goto do_pshreg;
            case CTOI:
                gen(LOAD,DREG,NODE,rhs);
                rhs->op = DREG;
                /* fall through to next case */
            case XREG:
            case YREG:         /* L966c */
            case UREG:
            case DREG:
do_pshreg:
#ifdef COCO
                gen(PUSH,rhs->op);
#else
                gen(PUSH, rhs->op, 0, 0);       /* pshs REG */
#endif
                rhs->op = STACK;        /* go to L9725 */
            default:           /* L9725 */
                break;
        }
    }                           /* end if (isreg...) */
    else {
        if ((zeroconst(rhs)) && (op < ULEQ)) {  /* L96a9 */
            checkop(lhs);         /* go to L973e */
            /* this should probably have been goto usual; */
            gen(130, op, (int)destin NUL1);
            return;
        } else {
            loadexp(lhs);         /* L96c6 */
            temp = lhs->op;

            if ((L76bf(rhs)) || ((temp == DREG) && isaleaf(rhs))
                ) {
                tranexp(rhs);
                /* go to L9725 */
            } else {
                gen(PUSH,temp NUL2);
                lhs->op=STACK;
                loadexp(rhs);
                temp = rhs->op;
                rhs = lhs;
                op=revrel(op);
            }
        }
    }                           /* end else => if ! isreg... */

    /* L9725... */
    gen(COMPARE,temp,NODE,rhs);
usual:
    /* gen code 'b_CC destin' */
    gen(130, op, (int)destin NUL1);
}

int
#ifndef __STDC__
isaleaf(node)
expnode *node;
#else
isaleaf(expnode * node)
#endif
{
    switch (node->op) {
        case NAME:             /* L9762 */
        case CONST:
            return 1;
        case STAR:             /* L9767 */
            return crf_isint(node);
            /*default:
               return 0; */
    }

    return 0;
}

#ifdef COCO
/* This function is unused - may be safely deleted if no further
 * COCO testing is needed */

p2_03_notused(p, parm2)
expnode *p;
int parm2;
{
    return p->op == NAME && p->val.sp->storage == AUTO;
}
#endif

static void
#ifndef __STDC__
checkop(node)
register expnode *node;
#else
checkop(expnode * node)
#endif
{
    expnode *lhs;
    int flag = 0;

    switch(node->op){
        /* break = L9889 */
        case AND:
            /* L97c2 */
            if (((lhs = node->right)->op != CONST)
                    || ((unsigned)(lhs->val.num) > (unsigned)255)) {
                flag = 1;
            }

            break;

        case ASSIGN:
            /* L97de */
            if ((isreg((node->left)->op))
                && !(isaleaf(node->right))) {
                flag = 1;
            }

            break;

        case INCAFT:
        case DECAFT:
        case DECBEF:
        case INCBEF:
        case ASSPLUS:
        case ASSMIN:           /* L97fc */
        case ASSAND:
        case ASSOR:
        case ASSXOR:
            if (!isreg((node->left)->op))
                break;
        case YREG:
        case UREG:
        case OR:
        case XOR:              /* L980b */
        case COMPL:
        case NEG:
        case CALL:
            flag = 1;
            break;
    }

    shiftflag = 0;
    tranexp(node);

    switch (node->op) {
        case XREG:
        case DREG:             /* L9898 */
        case YREG:
        case UREG:
            if (flag || shiftflag)
                gen(COMPARE,node->op,CONST,0);

            break;

        case CTOI:
            /* L98b2 */
            gen(LOAD, DREG, NODE, (node = node->left));
            break;
        default:               /* L98b4 */
            gen(LOAD, DREG, NODE, node);
            break;
    }
}

static int
#ifndef __STDC__
invrel(op)
int op;
#else
invrel(int op)
#endif
{
    switch(op){
        case EQ:               /* L98fd */
            return NEQ;
        case NEQ:              /* L9902 */
            return EQ;
        default:               /* L9907 */
            return ((op > GT ? ULEQ+UGT : LEQ+GT) - op);
    }
}

static int
#ifndef __STDC__
revrel(op)
int op;
#else
revrel(int op)
#endif
{
    switch (op) {
        case EQ:               /* L9935 */
        case NEQ:
            return op;
        case LEQ:
        case LT:               /* L9939 */
        case ULEQ:
        case ULT:
            return op+2;
        default:               /* L9940 */
            return op-2;
    }
}

static int
#ifndef __STDC__
zeroconst(node)
register expnode *node;
#else
zeroconst(expnode * node)
#endif
{
    return node->op == CONST && !node->val.num;
}
