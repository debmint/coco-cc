/* ************************************************************ *
 * ccomp_03.c                                                   $
 *                                                              $
 *                                                              $
 * $Id:: comp_03.c 94 2014-05-04 22:34:25Z David                $
 * ************************************************************ */

/* Sun, 01 Jun 2008 14:33:24 -0500
 * This version matches the original except for a couple of jump
 * variations
 */

#include "cj.h"

static expnode *primary();
static int isop();
static expnode *explist();
static expnode *getcast();
static int elsize(
#ifdef __STDC__
                        expnode ** p1
#endif
    );

expnode *
#ifdef __STDC__
parsexp(int priority)
#else
parsexp(priority)
int priority;
#endif
{
    expnode *lhs;
    int op, priop;
    expnode *temp;
    int lno;
    char *errpnt;
    int rprec;

    register expnode *rhs;

    if (!(lhs = primary())) {        /* If no primary, return 0 */
        return 0;
    }

    /* Loop as long as there is any assignment
     * This takes care of such things as a = b = c = .... */

    while ((isop()) && (priority <= symval)) {  /* L087e */
        op = sym;
        /* L07a0 */
        rprec = priop = symval;
        lno = symline;
        errpnt = symptr;
        getsym();

        switch(op) {
            case QUERY:        /* _10 (L07de) */
            case ASSIGN:
                break;
            default:
                if ((op < ASSPLUS) || (op > ASSXOR)) {
                    rprec = priop + 1;
                }

                break;
        }

        if ((rhs = parsexp(rprec))) {
            /* L07de else L0873 */
            if (op == QUERY) {
                if (need(COLON)) {
                    /* else L0847 */
                    reltree(rhs);
                    /* L0847 */
                    goto retrn_ref;     /* Wouldn't "break" work? */
                } else {
                    errpnt = symptr;
                    lno = symline;

                    if ((temp = parsexp(UNION))) {
                        rhs = newnode(COLON,rhs,temp,3,lno,errpnt);
                    } else {
                        error("third expression missing");
                        reltree(rhs);
                        goto retrn_ref;
                    }
                }
            }

            /* L0852 */
            lhs = newnode(op,lhs,rhs,priop,lno,errpnt);
        } else {
            error("operand expected");  /* L0873 */
        }
    }                           /* end while (isop ()) */

  retrn_ref:
    return lhs;
}

static expnode *
#ifdef __STDC__
primary(void)
#else
primary()
#endif
{
    expnode *prim;
    int op,lno;
    char *errpnt;
#ifdef COCO
    int v2;                     /* This var is not used, I think */
#endif
    expnode *temp;

    register expnode *nodep = 0;

    switch (sym) {              /* L0a01 */
        case STRING:
        case NAME:
        case LCONST:
        case FCONST:
        case CONST:            /* L08ab */
            nodep = newnode(sym,0,0,symval,symline,symptr);
            getsym();
            break;

        case LPAREN:           /* L08d0 *//* '-' */
            getsym();

            if (istype()) {
                /* If it's a basic var type, or typedef */
                nodep = getcast();
                need(RPAREN);

                if (!(nodep->left = primary())) {    /* else break */
                    release(nodep);
                    nodep = 0;
                }

                break;
            }

            if (!(nodep = parsexp(0))) {        /* L08fe */
                /* else L0934 */
              need_expression:
                experr();
                nodep = newnode(CONST, 0, 0, 0, symline, symptr);
            }

            need(RPAREN);

            break;
        case NOT:              /* L093f */
        case NEG:
        case COMPL:
        case STAR:
        case DECBEF:
        case INCBEF:
        case AMPER:
            op=sym;
            lno=symline;
            errpnt=symptr;
            getsym();

            if ((prim = primary())) {        /* self */
                /* else L097a */
                nodep = newnode(op, prim, 0, EXTERN, lno, errpnt);
            } else {
                error("primary expected");
            }

            break;

        case SIZEOF:           /* L0986 */
            lno = symline;
            errpnt = symptr;
            getsym();

            if(sym==LPAREN){
                /* else L09c8 */
                getsym();

                if (istype()) { /* else L09aa */
                    prim = getcast();        /* go to L09bc */
                } else {
                    if (!(prim = parsexp(0))) {
                        goto need_expression;
                    }
                }

                need(RPAREN);   /* L09bc */
            } else {
                prim = primary();    /* L09c8 */
            }

            temp = prim;
            /* L09cd */
            nodep = newnode(CONST,0,0,elsize(&temp),lno,errpnt);
            reltree(temp);

            break;
    }

    if (nodep == 0) {           /* _37 */
        return 0;
    }

    for ( ; ; ) {
        switch(sym) {
            case LPAREN:       /* L0a73 *//* '-' */
                errpnt=symptr;
                lno=symline;
                getsym();
                nodep = newnode(CALL,nodep,explist(),STATIC,lno,errpnt);
                need(RPAREN);

                continue;

            case LBRACK:
                /* L0aa4 */
                getsym();

                if (!(prim = parsexp(CHAR))) {  /* else L0ad8 */
                    experr();
                    prim = newnode(CONST, 0, 0, 0, symline, symptr);
                }

                nodep = newnode(PLUS, nodep, prim, 12, symline, symptr);
                nodep = newnode(STAR, nodep, 0, 15, symline, symptr);
                need(RBRACK);

                continue;

            case DOT:          /* L0b24 */
            case ARROW:
                op=sym;
                lno=symline;
                errpnt=symptr;

                ++mosflg;
                getsym();
                --mosflg;

                if (sym!=NAME) {
                    identerr();
                    break;
                }
                prim = newnode(sym,0,0,symval,symline,symptr);
                nodep = newnode(op,nodep,prim,15,lno,errpnt);
                getsym();
                continue;
        }

        break;
    }

    /* L0bb0 */
    switch (sym){
        case INCBEF:
            /* L0bb4 *//* 0x3c */
            sym=INCAFT;
            goto dblop;
        case DECBEF:
            sym=DECAFT;
dblop:      nodep = newnode(sym,nodep,0,14,symline,symptr);
            getsym();

            break;
    }

    return nodep;
}

static expnode *
#ifdef __STDC__
explist(void)
#else
explist()
#endif
{
    expnode *ptr;
    register expnode *list = 0;

    while (1) {
        if (sym == RPAREN) {    /* else L0c50 */
            break;
        }

        if ((ptr = parsexp(CHAR))) {    /* if char or int */
            /* else L0c43     */
            ptr = newnode(ARG,ptr,list,0,ptr->lno,ptr->pnt);
            list = ptr;
        }

        if (sym != COMMA) {
            break;
        }

        getsym();
    }

    return list;
}

int
#ifdef __STDC__
getconst(int p)
#else
getconst(p)
int p;
#endif
{
    int v;
    register expnode *ptr;

    ptr = optim(parsexp(p));

    if ((ptr) && (ptr->op == CONST)) {
        v = ptr->val.num;
    } else {
        v = 0;
        error("constant required");
    }

    if (ptr) {
        reltree(ptr);
    }

    return v;
}

static int
#ifdef __STDC__
isop(void)
#else
isop()
#endif
{
    switch (sym) {
        case AMPER:            /* L0cac */
            sym=AND;
            symval=8;
            return 1;
        case STAR:             /* L0cb6 */
            sym=TIMES;
            symval=13;
            return 1;
        case NEG:              /* L0cc0 */
            sym=MINUS;
            symval=12;
            return 1;
        case COMMA:            /* L0cca */
        case ASSIGN:
        case QUERY:
            return 1;
        case COLON:
            return 0;
        default:
            /* L0ccf */
            return (((sym >= DBLAND) && (sym <= GT)) ||
                    ((sym >= ASSPLUS) && (sym <= ASSXOR)));
    }
}

int
#ifdef __STDC__
constop(int op, int r, int l)
#else
constop(op, r, l)
int op, r, l;
#endif
{
    switch(op) {
        case PLUS:  return r + l;
        case MINUS: return r - l;
        case TIMES: return r * l;
        case DIV:   if (l) return r / l;

                    diverr();
                    return 0;
        case MOD:   if(l) return r % l;
                    diverr();
                    return 0;
        case AND:   return r & l;
        case OR:    return r | l;
        case XOR:   return r ^ l;
        case SHL:   return r << l;
        case SHR:   return r >> l;
        case EQ:    return r == l;
        case NEQ:   return r != l;
        case GT:    return r > l;
        case LT:    return r < l;
        case GEQ:   return r >= l;
        case LEQ:   return r <= l;
        case NEG:   return -r;
        case NOT:   return !r;
        case COMPL: return ~r;
        case DBLAND: return r && l;
        case DBLOR: return r || l;
        case ULEQ:
        case ULT:
        case UGEQ:
        case UGT:
            {
                char *lp,*rp;
                lp = (char *) l; rp = (char *) r;

                switch (op) {
                    case ULEQ: return rp <= lp;
                        /* L0e40 */
                    case ULT:  return rp < lp;
                    case UGEQ: return rp >= lp;
                    default:   return rp > lp;
                        /* L0e64 */
                }
            }

        default:    error("constant operator");
                    return 0;
    }
}

static expnode *
#ifdef __STDC__
getcast(void)
#else
getcast()
#endif
{
    int size;
    expnode *dptr;
    int type;
    expnode *ptr;
    int lno;
    char *errpnt;
    elem *dummy;                /* Not used here, just a dummy for settype () */

    lno = symline;
    errpnt = symptr;
    type = settype(&size,(dimnode **)(&dptr),&dummy);
    type = declarator((symnode **)(&ptr),(dimnode **)(&dptr),type);
    clear(&arglist);

    if (ptr) error("name in a cast");

    ptr = newnode(CAST,0,0,0,lno,errpnt);
    ptr->type = type;
    sizeup((symnode *)ptr,(dimnode *)dptr,size);

    return ptr;
}

expnode *
#ifdef __STDC__
newnode(int op,
        expnode * left, expnode * right, int value, int lno, char *errpnt)
#else
newnode(op, left, right, value, lno, errpnt)
int op;
expnode *left;
expnode *right;
int value;
int lno;
char *errpnt;
#endif
{
    register expnode *node;

    if (freenode) {
        node = freenode;
        freenode = node->left;
    } else {
        node = (expnode *) grab(sizeof(expnode));
    }

    node->op = op;
    node->left = left;
    node->right = right;
    node->val.num = value;
    node->lno = lno;
    node->pnt = errpnt;
    node->modifier = 0;

    return node;
}

void
#ifdef __STDC__
experr(void)
#else
experr()
#endif
{
    error("expression missing");
}

static int
#ifdef __STDC__
elsize(expnode ** prim)
#else
elsize(prim)
expnode **prim;
#endif
{
    register expnode *p = *prim;

    switch (p->op) {
        default:               /* L103d */
            p = *prim = optim(p);
            goto usual;
        case NAME:             /* L104d */
            p->type = (p->val.sp)->type;
            chkdecl(p);
            p = (expnode *) p->val.sp;
        case CAST:             /* L105f */
usual:      return getsize(p->type,p->size,p->dimptr);
            break;
        case DOT:
        case ARROW:            /* L1072 */
            return elsize(&(p->right));
            break;
    }
}
