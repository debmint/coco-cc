/* ******************************************************** *
 * comp_01.c - first module for c.comp                      *
 *                                                          *
 * from pass1_11.c - contains main()                        *
 *                                                          *
 * $Id:: comp_01.c 61 2008-09-19 03:09:17Z dlb            $ *
 *                                                          *
 * ******************************************************** */

/* Cause "pass1.h" to not define variables "extern"*/
#define MAIN

#ifdef UNIX
#include <signal.h>
#endif

#include "cj.h"
#include <stdlib.h>

symnode sdummy = {
    INT, INTSIZE, 0, 0, EXTDEF, {0}, 0, 0, 0, 0
};

char *DummyNm = sdummy.sname;
char strname[]
#ifdef COCO
    = "cstr.XXXXX"
#else
    = "cstr.XXXXXX"
#endif
    ;

int
#ifdef __STDC__
main(int argc, char **argv)
#else
main(argc, argv)
int argc;
char **argv;
#endif
{
    /* Initialization routines */

#ifdef UNIX
    int strfd;

    signal(SIGINT, (__sighandler_t) tidy);

    if ((strfd = mkstemp(strname)) == -1) {
        fatal("Cannot open strings file");
    }

    if (!(strfile = fdopen(strfd, "w+"))) {
        fatal("Could not create stream for strings file");
    }
#else
    intercept(tidy);
    mktemp(strname);
#endif
    strcpy(DummyNm, "_dummy_");
    lexinit();
    in = stdin;
    code = stdout;

    /* Process arguments */

    while (--argc > 0) {        /*  L0295 */
        register char *argstr;

        if (*(argstr = *(++argv)) == '-') {     /* L01b7 */
            /* else L0265 */
            while (*(++argstr) != '\0') {       /*  L0257 */
                switch (*argstr) {
                    case 's':  /* L01d2 */
                        sflag = 1;
                        break;
                    case 'n':  /* L01da */
                        ++ModName;
                        break;
                    case 'o':  /* L01e4 */
                        if (*(++argstr) == '=') {
                            if (!(code = fopen(++argstr, "w"))) {
                                fprintf(stderr, "can't open %s\n", argstr);
                                errexit();
                            }
                        }

                        goto L0261;
                    case 'p':  /* L0219 */
                        pflag = 1;
                        break;
                    default:   /* L0220 */
                        fprintf(stderr, "unknown flag : -%c\n", *argstr);
                        errexit();
                        break;
                }
            }                   /* end while *argv != 0 */

          L0261:
            continue;
        } else {
            if (!D0052) {       /* L0295 */
                ++D0052;
                if (!(in = freopen(*argv, "r", stdin))) {
                    fatal("can't open input file");
                }
            }
        }
    }                           /* end while argc != 0 */

    initbuf0();
    getsym();

    /* Here is where all file reading and output occurs */

    while (sym != -1) {
        funcmain();
    }

    epilogue();                  /* dump strings and write endsect for prog */

    if (ferror(code)) {
        fatal("error writing assembly code file");
    }

    fflush(stdout);

    if (errcount) {
        fprintf(stderr, "errors in compilation : %d\n", errcount);
        errexit();
    }
#ifndef COCO
    return 0;
#endif
}

void
#ifdef __STDC__
errexit(void)
#else
errexit()
#endif
{
    exit(1);
}
