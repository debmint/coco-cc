/* ************************************************************************ *
 * floats.c - part 7 for c.com                                              $
 *                                                                          $
 * Part 7 for c.comp assembly                                               $
 * $Id:: floats.c 94 2014-05-04 22:34:25Z David                             $
 * ************************************************************************ */

#include "cj.h"

void
#ifndef __STDC__
dload(cref)
expnode *cref;
#else
dload(expnode * cref)
#endif
{
    trandexp(cref);
    getadd(cref);
}

void
#ifndef __STDC__
trandexp(node)
register expnode *node;
#else
trandexp(expnode * node)
#endif
{
    int op;
    expnode *p;
    int type;
#ifdef DEVEL
    int var0;                   /* Not used. keep in COCO for matching code */
#endif

    type = node->type;

    switch (op = node->op) {
        case FREG:
        case XIND:
        case YIND:             /* L3cac */
        case UIND:
        case NAME:
            break;
        case STAR:
            /* L39bb */
            tranlexp(node);
            break;
        case UTOD:
            /* L39c5 */
        case ITOD:
            lddexp(node->left);
L39d0:
            gen(DBLOP,op NUL2);
vty2flacc:
            node->op = FREG;
            break;
        case LTOD:
            /* L39ec */
            lload(node->left);
            goto L39d0;
        case DTOF:
            /* L39f5 */
        case FTOD:
            dload(node->left);
            goto L39d0;
        case FCONST:
            /* L3a02 */
            gen(DBLOP,FCONST,node->val.num NUL1);
            node->op = XIND;
            /*  should free constant storage here  */
            node->val.dp = NULL;
            break;
        case QUERY:
            doquery(node,dload);
            goto L3bac;
        case INCBEF:           /* L3a27 */
        case DECBEF:
        case NEG:
            dload(node->left);
            gen(DBLOP,op,type NUL1);
            goto L3bac;
        case INCAFT:
        case DECAFT:
            /* L3a47 */
            gen(LOADIM,XREG,FREG NUL1);
            gen(PUSH,XREG NUL2);
            dload(node->left);
            gen(DBLOP,op,type NUL1);
            gen(DBLOP,MOVE,type NUL1);
            gen(DBLOP,op == INCAFT ? DECAFT : INCAFT,type NUL1);
            goto vty2flacc;
        case CALL:
            /* L3aba */
            docall(node);
            goto vty2flacc;
        case EQ:
        case NEQ:
        case GEQ:
        case LEQ:
        case GT:               /* L3ac3 */
        case LT:
        case PLUS:
        case MINUS:
        case TIMES:
        case DIV:
            dload(node->left);
            gen(DBLOP,STACK NUL2);
            dload(node->right);
            gen(DBLOP,op NUL2);
            goto vty2flacc;
        case ASSIGN:
            /* L3af7 */
            dload(node->left);
            gen(PUSH,XREG NUL2);
            dload(node->right);
            goto L3b95;

L3b1d:
            p = node->left;

            if (type == FLOAT) {
                /* else l3b4f */
                dload(p->left);
                gen(PUSH,XREG NUL2);
                gen(DBLOP,FTOD NUL2);
            } else {
                dload(p);
                gen(PUSH,XREG NUL2);
            }

            node->op = op - 80;
            p->op = XIND;
            trandexp(node);

            if (type == FLOAT) {
                gen(DBLOP, DTOF NUL2);
            }
L3b95:
            gen(DBLOP,MOVE,type NUL1);
L3bac:
            node->op = XIND;
            node->val.num = 0;
            break;

default:
            /* L3bba */
            if (op >= ASSPLUS) {
                goto L3b1d;
            }

            comperr(node,"floats");
    }
}
