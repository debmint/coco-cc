/* ************************************************************************ *
 * comp_10.c - part 10 for c.comp                                           *
 *                                                                          *
 * This file does much of the output to the destination file                *
 *                                                                          *
 * Comes from p2_06.c                                                       *
 * $Id:: comp_10.c 73 2008-10-03 20:20:29Z dlb                            $ *
 * ************************************************************************ */

#include "cj.h"

static char *yind = ",y";
static char *spind = ",s";
static char *lbsr = "lbsr ";
static char *lbra = "lbra ";
static char *clra = "clra";
static char *unkopr = "unknown operator : ";

static int regname(
#ifdef __STDC__
                      int
#endif
    );

static void transfer(
#ifdef __STDC__
                          int, int
#endif
    );

static void dolongs(
#ifdef __STDC__
                       int, int *
#endif
    );

static void dofloats(
#ifdef __STDC__
                        int, int
#endif
    );

static void getcon(
#ifdef __STDC__
                              int *arra, int count
#endif
    );

static void call_intfunc(
#ifdef __STDC__
                            char *
#endif
    );

static void call_longfunc(
#ifdef __STDC__
                             char *
#endif
    );

static void call_realfunc(
#ifdef __STDC__
                             char *
#endif
    );

static void trouble(
#ifdef __STDC__
                         int, int, expnode *
#endif
    );

static void doref(
#ifdef __STDC__
                        int, int, expnode *, int
#endif
    );

static void addoff(
#ifdef __STDC__
                          int
#endif
    );

static char *getrel(
#ifdef __STDC__
                       int
#endif
    );

static void ob(
#ifdef __STDC__
                  int
#endif
    );

static void outlea(
#ifdef __STDC__
                       int
#endif
    );

static void outstr(
#ifdef __STDC__
                           int, int
#endif
    );

void
#ifndef __STDC__
gen(op, rtype, arg, val)
int op;
int rtype;
int arg;
register expnode *val;
#else
gen(int op, int rtype, int arg, expnode * val)
#endif
{
    int reg;
    symnode *sptr;
    int temp, value;

    if (op == LONGOP) {
        dolongs(rtype, (int *)arg);
        return;
    }

    if (op == DBLOP) {
        dofloats(rtype, arg);
        return;
    }

    switch(op) {
        case PUSH:
            /* L465e */
            fprintf(code, " pshs %c\n", regname(rtype));

            if ((sp -= 2) < maxpush)
                maxpush = sp;
            return;
        case JMPEQ:
            /* L468b */
            gen(COMPARE, XREG, CONST, (expnode *)arg);
            arg = rtype;
            rtype = EQ;
        case CNDJMP:
            ot("lb");
            os(getrel(rtype));
            label(arg);
            return;
        case RETURN:
            /* L46c5 */
            if (pflag) {
                prt_profend();
            }

            ol("puls u,pc\n");
            return;
        case AND:
        case OR:
        case XOR:
            trouble(op, arg, val);
            return;
        case TIMES:            /* L46e7 */
            call_intfunc("ccmult");
            return;
        case UDIV:             /* L46ed */
            call_intfunc("ccudiv");
            return;
        case DIV:              /* L46f3 */
            call_intfunc("ccdiv");
            return;
        case SHL:              /* L46f9 */
            call_intfunc("ccasl");
            return;
        case SHR:              /* L46ff */
            call_intfunc("ccasr");
            return;
        case USHR:             /* L4705 */
            call_intfunc("cclsr");
            return;
        case UMOD:             /* L470b */
            call_intfunc("ccumod");
            return;
        case MOD:              /* L4711 */
            call_intfunc("ccmod");
            return;
        case NEG:              /* L471d */
            ol("nega\n negb\n sbca #0");
            return;
        case COMPL:            /* L4724 */
            ol("coma\n comb");
            return;
        case GOTO:             /* L472b */
            ot("leax ");
            od(-sp);
            os(spind);
            nl();
        case JMP:              /* L4751 */
            ot(lbra);
            label(rtype);
            return;
        case LABEL:            /* Generate a "goto" dest *//* L4760 */
            ot(lbra);
            label(arg = ++curlabel);
            label(rtype);
            ot("leas ");
            od(sp);
            os(",x\n");
            label(arg);
            return;
        case CALL:             /* L47af */
            callflag = 4;

            if ((((expnode *) arg)->op == NAME) &&
                ((sptr = ((expnode *) arg)->val.sp))) {
                ot(lbsr);
                nlabel(sptr->sname, 0);
                return;
            }

            ot("jsr ");
            deref(rtype, arg, 0);
            nl();
            return;
        case CTOI:             /* L4805 */
            ol("sex");
            return;
        case LTOI:             /* L480b */
            ot("ld");
            doref('d', rtype, (expnode *)arg, INTSIZE);
            return;
        case IDOUBLE:          /* L4830 */
            ol("aslb\n rola");  /* jumping to L4840 */
            return;
        case HALVE:            /* L4836 */
            ol("asra\n rorb");  /* jumping to L4840 */
            return;
        case UHALVE:           /* L483c */
            ol("lsra\n rorb");
            return;
        case YREG:             /* L4845 */
            ot("ldy ");
            goto L4861;
        case UREG:             /* L4854 */
            ot("ldu ");
          L4861:
            od(rtype);
            os(spind);
            nl();
            return;
        case LEAX:             /* L487d */
            ot("leax ");

            switch (rtype) {
                case NODE:     /* L488c */
                    fprintf(code, "%d,%c\n", ((expnode *) arg)->val.num,
                            (reg =
                             ((reg =
                               ((expnode *) arg)->op) ==
                              YIND) ? 'y' : ((reg == UIND) ? 'u' : 'x')));
                    break;
                case DREG:     /* L48c9 */
                    fprintf(code, "d,%c\n", regname(arg));
                    break;      /* fall through to default */
                    /*default:
                       return; */
            }
            return;
    }

    reg = regname(rtype);

    if (arg == NODE) {          /* else L4a40 */
        if (val->op == CTOI) {  /* else L4a23 */
            gen(op, DREG, NODE, val->left);

            switch (op) {
                case LOAD:     /* L49ec */
                    ol("sex");
                    break;
                case RSUB:     /* L49f2 */
                case PLUS:
                    ol("adca #0");
                    break;
                case MINUS:    /* L49f8 */
                    ol("sbca #0");
                    break;
            }

            val->op = DREG;
            return;
        } else {
            if ((val->type == CHAR) && (op != LOADIM) && (reg != 'x')) {
                reg = 'b';
            }
        }
    }

    switch(op) {
        case LOAD:             /* L4a45 */
            if (arg == NODE) {
                value = val->val.num;

                switch (temp=val->op) {
                    case XREG: /* L4a5d */
                    case YREG:
                    case UREG:
                        if (rtype!=DREG) {
                            /* gen "leaRG? value,temp" */
                            outlea(reg);
                            fprintf(code,"%d,%c\n",value,regname(temp));
                        } else {
                            transfer(regname(temp),'d');
                            /* L4a82 */

                            if (value != 0) {
                                /* gen "addd ...whatever" */
                                gen(PLUS, DREG, CONST, (expnode *)value);
                            }
                        }

                        return;
                    case DREG: /* L4ab4 */
                        if (rtype != DREG) {
                            /* gen "tfr d,RG?" */
                            transfer('d', reg);
                        }

                        return;
                    case STRING:       /* L4ac8 */
                        /* gen "leaRG? ?,pc */
                        outstr(reg, val->val.num);
                        return;
                    case CONST:        /* L4ad6 */
                        arg = CONST;
                        val = (expnode *) value;
                        break;
                }
            }

            /* L4b04 */
            if ((rtype == DREG) && (arg == CONST) && (val == 0)) {
                ol("clra\n clrb");
                return;
            } else {
                ot("ld");       /* gen "ldRG? ..." */
                goto L4bc9;
            }

            /*return; */
        case COMPARE:          /* L4b30 */
            if ((arg == CONST) && (val == 0)) {
                /* Compare a function return with 0 */
                fprintf(code, " st%c -2,s\n", reg);
                return;
            }

            ot("cmp");

            if (reg == 'b') {
                reg = 'd';
            }

            goto L4bc9;
        case STORE:            /* L4b6f */
            if ((arg == NODE) && (isreg(temp = val->op))) {
                if (temp != rtype) {
                    transfer(reg, regname(temp));
                }

                return;
            } else {
                ot("st");
                goto L4bc9;
            }

        case MINUS:            /* L4ba9 */
            ot("sub");
            goto L4bc9;
        case RSUB:             /* L4bb8 */
            gen(NEG NUL3);
        case PLUS:             /* L4bc2 */
            ot("add");
          L4bc9:
            doref(reg, arg, val, 0);
            return;             /*break; */
        case LOADIM:           /* L4bce */
            if (arg == NODE) {  /* else break */
                switch (temp = ((val->val.sp))->storage) {
                    case DIRECT:       /* L4be3 */
                    case EXTERND:
                    case STATICD:
                        outlea(reg);
                        ob('>');

                        if (temp == STATICD) {
                            olbl(val->val.sp->offset);
                        } else {
                            on((val->val.sp)->sname);
                        }

                        addoff(val->modifier);
                        os(yind);
                        nl();
                        return;
                }
            }

            ot("lea");
            doref(reg, arg, val, 0);
            return;

        case EXG:              /* L4c5d */
            fprintf(code, " exg %c,%c\n", reg, regname(arg));
            return;             /*break; */
        case LEA:              /* L4c7d */
            outlea(reg);

            switch (arg) {
                case DREG:     /* L4c8b */
                    os("d,");
                    goto prtregnam;
                case CONST:    /* L4c9a */
                    od((int) val);      /* cast to satify prototype */
                    ob(',');
                  prtregnam:
                    ob(reg);
                    nl();
                    return;
                default:       /* L4cbe */
                    error("LEA arg");
            }

            break;
        default:               /* L4cd8 */
            error(unkopr);
            break;
    }
}

static int
#ifndef __STDC__
regname(rgcode)
int rgcode;
#else
regname(int rgcode)
#endif
{
    switch (rgcode) {
        case DREG:             /* L4d2d */
            return 'd';
        case XREG:             /* L4d32 */
            return 'x';
        case YREG:             /* YREG */
            return 'y';
        case UREG:             /* UREG */
            return 'u';
        default:
            return ' ';
    }
}

static void
#ifndef __STDC__
transfer(rgfrom, rgto)
int rgfrom;
int rgto;
#else
transfer(int rgfrom, int rgto)
#endif
{
    fprintf(code, " tfr %c,%c\n", rgfrom, rgto);
}

/* ******************************************************************** *
 * dolongs() - Generate code for longs                              *
 * ******************************************************************** */

static void
#ifndef __STDC__
dolongs(typ, longptr)
int typ;
int *longptr;
#else
dolongs(int typ, int *longptr)
#endif
{
    switch (typ) {
        case STACK:            /* L4d80 */
            /* cast parm 4 to satify prototype */
            gen(LOAD, DREG, XIND, (expnode *)2);   /* ldd 2,x */
            gen(PUSH, DREG NUL2);
            gen(LOAD, DREG, XIND, 0);
            gen(PUSH, DREG NUL2);
            return;
        case TEST:             /* L4dd0 */
            ol("lda 0,x\n ora 1,x\n ora 2,x\n ora 3,x");
            return;             /*break; */
        case MOVE:             /* L4ddc */
            call_longfunc("_lmove");
            sp -= INTSIZE;      /* WARNING sp _may_ be an int * */
            return;
        case PLUS:             /* L4de2 */
            call_longfunc("_ladd");
            return;
        case MINUS:            /* L4de8 */
            call_longfunc("_lsub");
            return;
        case TIMES:            /* L4dee */
            call_longfunc("_lmul");
            return;
        case DIV:              /* L4df4 */
            call_longfunc("_ldiv");
            return;
        case MOD:              /* L4dfa */
            call_longfunc("_lmod");
            return;
        case AND:              /* L4e00 */
            call_longfunc("_land");
            return;
        case OR:               /* L4e06 */
            call_longfunc("_lor");
            return;
        case XOR:              /* L4e0c */
            call_longfunc("_lxor");
            return;
        case SHL:              /* L4e12 */
            call_longfunc("_lshl");
            sp -= INTSIZE;      /* WARNING sp _may_ be an int * */
            return;
        case SHR:              /* L4e18 */
            call_longfunc("_lshr");
            sp -= INTSIZE;      /* WARNING sp _may_ be an int * */
            return;
        case EQ:
        case NEQ:
        case GEQ:
        case LEQ:              /* L4e2b */
        case GT:
        case LT:
            call_longfunc("_lcmpr");
            return;
        case NEG:              /* L4e37 */
            call_longfunc("_lneg");
            sp -= LONGSIZE;
            return;
        case COMPL:            /* L4e3d */
            call_longfunc("_lcompl");
            sp -= LONGSIZE;
            return;
        case ITOL:             /* L4e43 */
            call_longfunc("_litol");
            sp -= LONGSIZE;
            return;
        case UTOL:             /* L4e49 */
            call_longfunc("_lutol");
            sp -= LONGSIZE;
            return;
        case INCBEF:           /* L4e4f */
        case INCAFT:
            call_longfunc("_linc");
            sp -= LONGSIZE;
            return;
        case DECBEF:           /* L4e55 */
        case DECAFT:
            call_longfunc("_ldec");
            sp -= LONGSIZE;
            return;
        case LCONST:           /* L4e68 */
            getcon(longptr, LONGSIZE / 2);
            return;
        default:               /* L4e74 */
            error("codgen - longs");
            os(unkopr);
            od(typ);
            nl();
    }
}

/* ******************************************************************** *
 * dofloats () - Generate code to handle floats                      *
 * Note : in four of the below tests, real_array is compared to         *
 *        FLOAT.  FLOAT is cast to int * to satisfy gcc's         *
 *        prototypes.                                                   *
 * ******************************************************************** */

static void
#ifndef __STDC__
dofloats(op, arg)
int op;
register int arg;
#else
dofloats(int op, int arg)
#endif
{
    switch (op) {
        case FCONST:           /* L4f6b */
            getcon((int *)arg, DOUBLESIZE / 2);
            /*getcon (((struct dbltree *)arg)->cocoarr, DOUBLESIZE/2);  was for not coco */
            return;             /* break */
        case STACK:            /* L4f79 */
            call_realfunc("_dstack");
            sp -= DOUBLESIZE;
            return;
        case 139:              /* L4f8c */
            fprintf(code, " lda %c,x\n", ((arg == FLOAT) ? '3' : '7'));
            return;
        case MOVE:             /* L4fad */
            call_realfunc((arg == FLOAT) ? "_fmove" : "_dmove");
            sp += INTSIZE;      /* jump to L5205 */
            return;
        case PLUS:             /* L4fc7 */
            call_realfunc("_dadd");
            sp += DOUBLESIZE;
            return;
        case MINUS:            /* L4fcd */
            call_realfunc("_dsub");
            sp += DOUBLESIZE;
            return;
        case TIMES:            /* L4fd3 */
            call_realfunc("_dmul");
            sp += DOUBLESIZE;
            return;
        case DIV:              /* L4fd9 */
            call_realfunc("_ddiv");
            sp += DOUBLESIZE;
            return;
        case EQ:               /* L4fdf */
        case NEQ:
        case GEQ:
        case LEQ:
        case GT:
        case LT:
            call_realfunc("_dcmpr");
            sp += DOUBLESIZE;
            return;
        case NEG:              /* L4ff2 */
            call_realfunc("_dneg");
            return;
        case INCBEF:           /* L4ff8 */
        case INCAFT:
            call_realfunc((arg == FLOAT) ? "_finc" : "_dinc");
            return;
        case DECBEF:           /* L500a */
        case DECAFT:
            call_realfunc((arg == FLOAT) ? "_fdec" : "_ddec");
            return;
        case DTOF:             /* L5020 */
            call_realfunc("_dtof");
            return;
        case FTOD:             /* L5026 */
            call_realfunc("_ftod");
            return;
        case LTOD:             /* L502c */
            call_realfunc("_ltod");
            return;
        case ITOD:             /* L5032 */
            call_realfunc("_itod");
            return;
        case UTOD:             /* L5038 */
            call_realfunc("_utod");
            return;
        case DTOL:             /* L503e */
            call_realfunc("_dtol");
            return;
        case DTOI:             /* L5044 */
            call_realfunc("_dtoi");
            return;
        default:               /* L5050 */
            error("codgen - floats");
            os(unkopr);
            od(op);
            nl();
            break;
    }
}
static void
#ifndef __STDC__
getcon(p, count)
int *p;
int count;
#else
getcon(int *p, int count)
#endif
{
    int temp;

    ot("bsr ");
    label(temp = ++curlabel);
    defcon((INTTYPE *)p, count);
    olbl(temp);
    ol("puls x");
}

void
#ifndef __STDC__
defcon(p, n)
register INTTYPE *p;
int n;
#else
defcon(INTTYPE *p, int n)
#endif
{
    int var0;

    defword();

    if (n == 1) {
        od((int) p);
    } else {
        if (p == 0) {           /* L518b */
            /* else L51bc */
            var0 = 1;

            while (var0++ < n) {
                os("0,");
            }

            ob('0');
        } else {
            var0 = 0;           /* L51bc */

            while (var0 < n) {
#ifdef _BIG_END
                od(*(p++));
#else
                od(p[n - var0 - 1]);
#endif

                if ((n - 1) != var0) {
                    ob(',');
                }

                ++var0;
            }
        }
    }

    nl();
}

/* ******************************************************************** *
 * call_intfunc (), call_longfunc(), call_realfunc ()                   *
 *                                                                      *
 *      - Print code to call library functions for the corresponding    *
 *        data type.                                                    *
 * Passed : The name of the library function                            *
 * ******************************************************************** */

static void
#ifndef __STDC__
call_intfunc(fncnam)
char *fncnam;
#else
call_intfunc(char *fncnam)
#endif
{
    ot(lbsr);
    ol(fncnam);
    sp += INTSIZE;
}

static void
#ifndef __STDC__
call_longfunc(fncnam)
char *fncnam;
#else
call_longfunc(char *fncnam)
#endif
{
    ot(lbsr);
    ol(fncnam);
    sp += LONGSIZE;
}

static void
#ifndef __STDC__
call_realfunc(strng)
char *strng;
#else
call_realfunc(char *strng)
#endif
{
    ot(lbsr);
    ol(strng);
}

static void
#ifndef __STDC__
trouble(op, arg, val)
register int op;
int arg;
expnode *val;
#else
trouble(int op, int arg, expnode * val)
#endif
{
    char *s;
    int cflag = 0, temp;

    if (arg == NODE) {
        /* else L5274 */
        cflag = (val->type == CHAR);
        arg = val->op;
        val = (expnode *) val->val.sp;
    }

    switch (op) {
        case AND:
            /* L5278 */
            s = "and";
            break;
        case OR:
            s = "or";
            break;
        case XOR:
            s = "eor";
            break;
    }

    switch (arg) {
        case NAME:             /* L52a0 */
        case YIND:
        case UIND:
        case XIND:
            if (cflag) {
                if (op == AND)
                    ol(clra);

                ot(s);
                doref('b', arg, val, 0);
            } else {
                ot(s);
                /* L52c2 */
                doref('a', arg, val, 0);
                ot(s);
                doref('b', arg, val, 1);
            }

            break;

        case CONST:            /* L5308 */

            /* Handle MS-byte */

            switch (temp = ((int) val >> 8) & 0xff) {
                case 0:        /* L5319 */
                    if (op == AND) {
                        ol(clra);
                    }

                    break;
                case 255:      /* L5327 */
                    if (op == AND) {
                        break;
                    }

                    if (op == XOR) {
                        ol("coma");
                        break;
                    }

                    /* fall through to default */
                default:       /* L5340 */
                    ot(s);
                    /* cast to satify prototype */
                    doref('a', CONST, (expnode *)temp, 0);
                    break;
            }

            /* Now handle LS-byte */

            switch (temp = ((int) val & 0xff)) {        /* L536d */
                case 0:        /* L5376 */
                    if (op == AND) {
                        ol("clrb");
                    }

                    break;
                case 255:      /* L5384 */
                    if (op == AND) {
                        break;
                    }

                    if (op == XOR) {
                        ol("comb");
                        break;
                    }

                default:       /* L53a0 */
                    ot(s);
                    /* cast to satify prototype */
                    doref('b', CONST, (expnode *)temp, 0);
                    break;
            }

            break;
        case STACK:            /* L53ce */
            fprintf(code, " %sa ,s+\n %sb ,s+\n", s, s);
            sp += INTSIZE;
            break;
        default:               /* L53ee */
            error("compiler trouble");
            break;
    }
}

static void
#ifndef __STDC__
doref(reg, arg, val, offset)
int reg;
int arg;
expnode *val;
int offset;
{
    ob(reg);
    ob(' ');
    deref(arg, val, offset);
    nl();
}

#else
doref(int reg, int arg, expnode * val, int offset)
{
    /* I have no idea why the following commented-out stuff was there.
     * it seems to have caused things like "lda #xx" to go signed,
     * where standard coco compiler didn't
     * I'll leave it here till I know it isn't necessary
     */

/*
//    int intval;
//
//    if (arg == CONST)
//    {
//        intval = (int)val;
//
    //        switch (reg)*//* sign extend the value */
/*        {
//            case 'a':
//            case 'b':
//                if (intval & 0x80)
    //                {
*//* We go through the following convolutions to
       //                     * make gcc happy
       //                     */
/*
//                    intval |= (-1 ^ 0x7f);
//                    val = intval;
//                }
//
//                break;
//            default:
//                if (intval & 0x8000)
//                {
//                    intval |= (-1 ^ 0x7fff);
//                    val = intval;
//                }
//                
//                break;
//        }
//    }
*/

    ob(reg);
    ob(' ');
    deref(arg, (int) val, offset);
    nl();
}
#endif

/* **************************************************************** *
 * addoff () - Prints value to add to another                   *
 * Passed:  Value to add.  If value is 0, nothing function exits    *
 *          without doing anything.                                 *
 * **************************************************************** */

static void
#ifndef __STDC__
addoff(offset, parm2, parm3)
register int offset;
#else
addoff(int offset)
#endif
{
    if (offset) {
        if (offset > 0)
            ob('+');

        od(offset);
    }
}


void
#ifndef __STDC__
deref(arg, val, offset)
int arg;
int val;
int offset;
#else
deref(int arg, int val, int offset)
#endif
{
#ifdef DEVEL
    int var2;                   /* unused - can delete when finished debugging */
#endif
    int sc;

    if (arg & INDIRECT)
        ob('[');

    switch (arg & NOTIND) {
            /* Reuse cref name for coco to save space.  For other compilers,
             * create another var to avoid so much casting
             */

        case NODE:
            /* L5492 */
            {
                register expnode *node;

                node = (expnode *) val;

                if (node->op == AMPER) {
                    ob('#');
                    deref(NODE, (int) (node->left), offset);
                } else {
                    deref(node->op,
                               node->val.num, offset + node->modifier);
                }
            }

            return;
        case CONST:
            /* L54c9 */
            ob('#');
            od(val);
            break;
        case FREG:
            os("_flacc");
            addoff(offset);
            os(yind);
            break;
        case NAME:
            /* L54f8 */
            {
                register symnode *sn;

                if ((sn = (symnode *) val)) {   /* else L5603 (= break) */
                    switch (sc = sn->storage) {
                        case AUTO:
                            /* L5507 */
                            val = (sn->offset - sp + offset);
                            od(val);
                            os(spind);
                            break;
                        case STATICD:
                            /* L551d */
                            if (!datflag)
                                ob((arg & 0x8000) ? '>' : '<');

                        case STATIC:
                            /* L5539 */
                            olbl(sn->offset);
                            goto dooff;
                        case DIRECT:
                            /* L5546 */
                        case EXTERND:
                            if (!datflag)
                                ob((arg & INDIRECT) ? '>' : '<');
                        case EXTERN:   /* L5562 */
                        case EXTDEF:
                            on(sn->sname);
dooff:
                            addoff(offset);
                            if (!(arg & INDIRECT )) {      /* If not offset addr */
                                if ((datflag) ||
                                    (sc == DIRECT) ||
                                    (sc == EXTERND) || (sc == STATICD)) {
                                    break;
                                }
                            }

                            if (isftn(sn->type))
                                os(",pcr");
                            /* L55c3 */
                            else
                                os(yind);

                            break;
                        default:
                            /* L55c9 */
                            error("storage error");
                            break;
                    }
                }               /* end initial "if" */
                else {
                    od(offset);
                }
            }

            break;
        case XIND:             /* L560d */
        case YIND:
        case UIND:
            od(val += offset);
            ob(',');

            switch (arg & 0x7fff) {
                case XIND:     /* L562c */
                    ob('x');
                    break;
                case YIND:     /* L5631 */
                    ob('y');
                    break;
                case UIND:     /* L5636 */
                    ob('u');
                    break;
                default:       /* L56b8 */
                    break;
            }

            break;
        case STACK:
            /* L5654 */
            os(spind);
            os("++");
            sp += 2;
            break;
        default:
            error("dereference");
            break;
    }

    if (arg & INDIRECT)
        ob(']');
}

static char *
#ifndef __STDC__
getrel(op)
int op;
#else
getrel(int op)
#endif
{
    switch (op) {
        default:               /* L56d6 */
            error("rel op");
        case EQ:               /* L56e1 */
            return "eq ";
        case NEQ:              /* L56e7 */
            return "ne ";
        case LEQ:              /* L56ed */
            return "le ";
        case LT:               /* L56f3 */
            return "lt ";
        case GEQ:              /* L56f9 */
            return "ge ";
        case GT:               /* L56ff */
            return "gt ";
        case ULEQ:             /* L5705 */
            return "ls ";
        case ULT:              /* L570b */
            return "lo ";
        case UGEQ:             /* L5711 */
            return "hs ";
        case UGT:              /* L5717 */
            return "hi ";
    }
}

void
#ifndef __STDC__
ot(s)
char *s;
#else
ot(char *s)
#endif
{
    putc(' ',code);
    os(s);
}

void
#ifndef __STDC__
ol(s)
char *s;
#else
ol(char *s)
#endif
{
    ot(s);
    nl();
}

void
#ifndef __STDC__
nl()
#else
nl(void)
#endif
{
    putc('\n', code);
}

static void
#ifndef __STDC__
ob(b)
int b;
#else
ob(int b)
#endif
{
    putc(b, code);
}

void
#ifndef __STDC__
os(s)
char *s;
#else
os(char *s)
#endif
{
    fprintf(code, s);
}

void
#ifndef __STDC__
od(n)
int n;
#else
od(int n)
#endif
{
#ifdef UNIX
    fprintf(code, "%hd", n & 0xFFFF);
#else
    fprintf(code, "%d", n);
#endif
}

void
#ifndef __STDC__
label(n)
int n;
#else
label(int n)
#endif
{
    olbl(n);
    lastst = 0;
    nl();
}

void
#ifndef __STDC__
olbl(n)
int n;
#else
olbl(int n)
#endif
{
    ob(UNIQUE);
    od(n);
}

void
#ifndef __STDC__
on(s)
char *s;
#else
on(char *s)
#endif
{
#ifdef COCO
    fprintf(code, "%.8s", s);
#else
    fprintf(code, "%.12s", s);
#endif
}

int
#ifndef __STDC__
modstk(nsp)
int nsp;
#else
modstk(int nsp)
#endif
{
    int x;

    if ((x = nsp - sp)) {
        ot("leas ");
        od(x);
        os(spind);
        nl();
    }

    return nsp;
}

void
#ifndef __STDC__
nlabel(ptr, scope)
char *ptr;
int scope;
#else
nlabel(char *ptr, int scope)
#endif
{
    on(ptr);

    if (scope)
        ob(':');

    nl();
}

static void
#ifndef __STDC__
outlea(reg)
int reg;
#else
outlea(int reg)
#endif
{
    fprintf(code, " lea%c ", reg);
}

static void
#ifndef __STDC__
outstr(reg, l)
int reg;
int l;
#else
outstr(int reg, int l)
#endif
{
    outlea(reg);
    fprintf(code, "%c%d,pcr\n", UNIQUE, l);
}

