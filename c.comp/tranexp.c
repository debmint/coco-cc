/* ************************************************************************ *
 * comp_13.c - part 13 for c.comp  >>> tranexp.c <<<                        *
 *                                                                          *
 * comes from p2_02.c                                                       *
 *                                                                          *
 * $Id:: comp_13.c 73 2008-10-03 20:20:29Z dlb                            $ *
 * ************************************************************************ */

#include "cj.h"
expnode *tranexp();

/* We do the following to maintain data ordering of original
 * after verifying code is identical, this can be eliminated
 */
#ifdef COCO
extern int masks[];
#else
int masks[] = {
    0x0002, 0x0004, 0x0008, 0x0010,
    0x0020, 0x0040, 0x0080, 0x0100,
    0x0200, 0x0400, 0x0800, 0x1000,
    0x2000, 0x4000
};
#endif


static void doload(
#ifdef __STDC__
                     expnode *
#endif
    );

static void tranbinop(
#ifdef __STDC__
                     int, expnode *
#endif
    );

static void dobool(
#ifdef __STDC__
                      expnode *
#endif
    );

static int L771d(
#ifdef __STDC__
                    expnode *
#endif
    );

static int is_ref(
#ifdef __STDC__
                     expnode *
#endif
    );

static void L77e6(
#ifdef __STDC__
                     expnode *
#endif
    );

static void assop(
#ifdef __STDC__
                     expnode *, int
#endif
    );

static void L7b83(
#ifdef __STDC__
                     expnode *
#endif
    );

static void dotoggle(
#ifdef __STDC__
                     expnode *, int
#endif
    );

static void loadxexp(
#ifdef __STDC__
                     expnode *
#endif
    );

static void tranxexp(
#ifdef __STDC__
                     expnode * cref
#endif
    );

/* ************************************************************ *
 * lddexp () - generates code to tfr R,d, addd <something>   * 
 * ************************************************************ */

void
#ifndef __STDC__
lddexp(tree)
register expnode *tree;
#else
lddexp(expnode * tree)
#endif
{
    loadexp(tree);

    if (tree->op != DREG) {
        gen(LOAD, DREG, NODE, tree);
        tree->op = DREG;
    }
}

void
#ifndef __STDC__
ldxexp(tree)
register expnode *tree;
#else
ldxexp(expnode * tree)
#endif
{
    tranxexp(tree);

    if (tree->op != XREG) {
        gen(LOAD, XREG, NODE, tree);
        tree->op = XREG;
    }
}

void
#ifndef __STDC__
loadexp(tree)
register expnode *tree;
#else
loadexp(expnode * tree)
#endif
{
    switch(tree->type) {
        case LONG:
            /* L6eba */
            lload(tree);
            break;
        case FLOAT:
        case DOUBLE:
            dload(tree);
            break;
        default:
            tranexp(tree);
            doload(tree);
    }
}

static void
#ifndef __STDC__
doload(tree)
register expnode *tree;
#else
doload(expnode * tree)
#endif
{
    switch (tree->op) {
        case STRING:           /* L6efb */
            gen(LOAD, XREG, NODE, tree);
            tree->op = XREG;
            tree->val.num = 0;
            break;
        case AMPER:            /* L6f1d */
            gen(LOADIM, XREG, NODE, tree->left);
            tree->val.num = 0;
            tree->op = XREG;
            break;
        case XREG:
        case DREG:
        case UREG:
        case YREG:
            break;
        default:               /* L6f3e */
            gen(LOAD, DREG, NODE, tree);
#ifdef COCO
          vtytoRG_D:
#endif
            tree->op = DREG;
            break;
    }
}

expnode *
#ifndef __STDC__
tranexp(tree)
register expnode *tree;
#else
tranexp(expnode * tree)
#endif
{
    int op;

    /* if it's a series of entries create a tree */

    if ((op = tree->op) == COMMA) {       /* else L6fcc */
        expnode *p = tree->right;

        reltree(tranexp(tree->left));
        nodecopy(p,tree);
        release(p);
        tranexp(tree);
    /* go to L7176 */
    } else if (islong(tree))
        tranlexp(tree);     /* go to L7176 */
    else if (isfloat(tree))
        trandexp(tree); /* go to L7176 */
    else if (numeric_op(op))
        tranbinop(op, tree);      /* go to L7176 */
    else {        /* L7011 */
        switch (op) {
            case STRING:   /* L7176 */
            case NAME:
            case YREG:
            case UREG:
            case AMPER:
            case CONST:
                break;
            case ASSIGN:   /* L7016 */
                L77e6(tree);
                break;
            case CTOI:
                tranexp(tree->left);
                break;
            case LTOI:
                tranlexp(tree->left);
#ifdef COCO
                gen(LTOI, NODE, (int)(tree->left));
#else
                gen(LTOI, NODE, (int)(tree->left), 0);
#endif
                tree->op = DREG;
                break;
            case DTOI:
                /* L7046 */
                dload(tree->left);
#ifdef COCO
                gen(DBLOP, DTOI);
#else
                gen(DBLOP, DTOI, 0, 0);
#endif
                tree->op = DREG;
                break;
            case STAR:     /* L705e */
                dostar(tree);
                break;
            case NOT:      /* L7065 */
            case DBLAND:
            case DBLOR:
                dobool(tree);
                break;
            case COMPL:    /* L706c */
            case NEG:
                lddexp(tree->left);
#ifdef COCO
                gen(op);
#else
                gen(op, 0, 0, 0);
#endif
                tree->op = DREG;
                break;
            case QUERY:    /* L7080 */
                /* warning from gcc - but this will work.
                 * doquery will provide tree as a parameter
                 * for lddexp when it calls it
                 */
                doquery(tree,lddexp);
                tree->op = DREG;
                break;
            case INCBEF:   /* L7095 */
            case INCAFT:
            case DECBEF:
            case DECAFT:
                dotoggle(tree, DREG);
                break;
            case CALL:     /* L70a1 */
                docall(tree);
                break;
            default:
                /* L70ab */
                if (op >= ASSPLUS) {
                    assop(tree,op);
                break;
                }

                comperr(tree,"translation");
                printf("%x\n",op);
        }
    }

    return tree;
    /* L7176 */
}

static void
#ifndef __STDC__
tranbinop(op, node)
int op;
expnode *node;
#else
tranbinop(int op, expnode * node)
#endif
{
    expnode *rhs;
#ifdef COCO
    int v2;                     /* unused - may delete when debugging is complete */
#endif
    unsigned int s;

    register expnode *lhs;

    lhs = node->left;
    rhs = node->right;

    if (lhs->type == UNSIGN)
    {
        switch(op) {
            case DIV:
                /* L719c */
                op = UDIV;
                break;
            case MOD:
                op = UMOD;
                break;
            case SHR:
                op = USHR;
                break;
            default:           /* L71f2 */
                break;
        }
    } else {
        switch (op) {
            case PLUS:         /* L71c2 */
            case MINUS:
                if ((lhs->op == AMPER) && (rhs->op != CONST)) {
                    tranxexp(node);
                    shiftflag = 0;
                    return;
                }

                break;
        }
    }

    switch (op) {               /* L748c = "break" */
        case MINUS:
            /* L71f7 */
            if (!isaleaf(rhs) ) {        /* else L7229 */
                loadexp(rhs);
#ifdef COCO
                gen(PUSH, rhs->op);
#else
                gen(PUSH, rhs->op, 0, 0);
#endif
                rhs->op = STACK;
                lddexp(lhs);
            } else {
                lddexp(lhs);    /* L7229 */
                tranexp(rhs);
            }

            gen(MINUS, DREG, NODE, rhs);
            break;
        case AND:
        case OR:
        case XOR:              /* L724d */
        case PLUS:
            if (((isaleaf(lhs)) && !(isaleaf(rhs))) || (lhs->op == CONST)) {
                expnode *__tmpref = lhs;

                lhs = rhs;
                rhs = __tmpref;
            }

            if (isreg(lhs->op)) {       /* L7275 */
                /* else L72a0 */
                gen(PUSH, lhs->op NUL2);        /* L7282 */
                lddexp(rhs);
                rhs->op = STACK;        /* go to L72d1 */
            } else {
                lddexp(lhs);    /* L72a0 */

                if (!isaleaf(rhs)) {
                    gen(PUSH, DREG NUL2);
                    lddexp(rhs);
                    rhs->op = STACK;    /* go to 1052 */
                } else {
                    tranexp(rhs); /* L72b7 */

                    if (op != PLUS) {
                        getinx(rhs);
                    }
                }
            }

            gen(op, DREG, NODE, rhs);
            break;
        case EQ:               /* L72ec */
        case NEQ:
        case GT:
        case LT:
        case LEQ:
        case GEQ:
        case UGT:
        case ULT:
        case ULEQ:
        case UGEQ:
            dobool(node);
            break;
        case USHR:             /* L72f6 */
        case SHR:
        case SHL:
            if ((rhs->op) == CONST) {   /* else L735a */
                s = rhs->val.num;
              L730a:
                if (s <= STRUCT) {   /* 4 */
                    /* else L735a */
                    lddexp(lhs);

                    while (s--) {
                        switch (op) {
                            case SHL:  /* L7321 */
                                gen(IDOUBLE NUL3);
                                break;
                            case SHR:  /* L7326 */
                                gen(HALVE NUL3);
                                break;
                            case USHR: /* L732b */
                                gen(UHALVE NUL3);
                                break;
                        }
                    }           /* end while (s--)  */

                    shiftflag = 1;      /* shiftflag _may_ be IsStruct, or the like */
                    goto L7492;
                }
                /*goto L7492; */
            }

            goto L73a8;
        case TIMES:            /* L735e */
            if (lhs->op == CONST) {     /* else L7372 */
                expnode *_tmpref;

                /* Swap lhs for rfRt */

                _tmpref = lhs;
                lhs = rhs;
                rhs = _tmpref;
            }                   /* fall through to next case */
        case UDIV:             /* L7372 */
            if ((rhs->op == CONST) && (s = isashift(rhs->val.num))) {       /* else L73aa */
                rhs->val.num = s;

                op = (op == TIMES) ? SHL : USHR;        /* L73a1 */
                goto L730a;
            }
        case DIV:              /* L73aa */
        case UMOD:
        case MOD:
          L73a8:
            loadexp(lhs);
            gen(PUSH, lhs->op NUL2);
            lddexp(rhs);
            gen(op NUL3);
            break;
        default:               /* L73d4 */
            comperr(node, "binary op.");
            break;
    }                           /* end switch op */

    /* Following jump-label is not needed - vestige from the old source */
#ifdef COCO
  L748c:
#endif
    shiftflag = 0;
  L7492:
    node->op = DREG;
}

int
#ifndef __STDC__
isashift(x)
int x;
#else
isashift(int x)
#endif
{
    /* returns whether x is a suitable candidate for
    * shifting code in multiplication or division
    */
    /* NOTE: Probably masks should be defined in this file and
     * 14 should be changed to sizeof (masks)/sizeof (masks[0])
     */
    register int i;

    for (i=0; i < 14 ;)
        if (x == masks[i++]) return i;

    return 0;
}

static void
#ifndef __STDC__
dobool(cref)
register expnode *cref;
#else
dobool(expnode * cref)
#endif
{
    int l1;
    int l2;

    tranbool(cref, (labstruc *)(l1 = ++curlabel), (labstruc *)(l2 = ++curlabel), 1);
    label(l1);
    gen(LOAD, DREG, CONST, (expnode *)1);

    gen(124, (l1 = ++curlabel), 1 NUL1);
    label(l2);

    gen(LOAD, DREG, CONST, 0);
    label(l1);
    cref->op = DREG;
}

void
#ifndef __STDC__
doquery(cref, fnc)
register expnode *cref;
void (*fnc)();
#else
doquery(expnode * cref, void (*fnc)(expnode *))
#endif
{
    int l1;
    int l2;
    int l3;

    /* This gives code in a bit different order than original, as
     * the following line was included in parameter push of the
     * call to tranbool(), but gcc gave warning.  Some compilers
     * might not do the "right thing", so we'll do it this way
     */
    /*l3 = ++curlabel; */
    tranbool(cref->left, (labstruc *)(l1 = ++curlabel), (labstruc *)(l3 = ++curlabel), 1);
    cref = cref->right;
    label(l1);
    (*fnc) (cref->left);
    gen(JMP, (l2 = ++curlabel), 0 NUL1);
    label(l3);
    (*fnc) (cref->right);
    label(l2);
}

void
#ifndef __STDC__
docall(node)
expnode *node;
#else
docall(expnode * node)
#endif
{
    expnode *lhs;
    int loclstk;

    loclstk = sp;
    lhs = node;

    while ((lhs = lhs->right)) {
        register expnode *rhs = lhs->left; /* L75f8 */ ;

        if (rhs->type == LONG) {        /* else L7627 */
            if (rhs->op == LCONST) {
                pushslong(rhs); /* go to L7680 */
                continue;
            } else {
                lload(rhs);
                gen(LONGOP, STACK NUL2);
                continue;
            }
        } else {                /* L7627 */
            if ((rhs->type == FLOAT) || (rhs->type == DOUBLE)) {
                dload(rhs);
                gen(DBLOP, STACK NUL2);
            } else {
                tranexp(rhs);

                switch (rhs->op) {
                    case DREG:
                    case XREG:
                    case UREG:
                    case YREG:
                        break;
                    default:
                        doload(rhs);
                        break;
                }

                gen(PUSH, rhs->op NUL2);
            }

        }
    }                           /* end while (lhs = cr->right)  ( L7680 ) */

    tranexp(node->left);
    gen(CALL, NODE, (int)(node->left) NUL1);
    sp = modstk(loclstk);
    node->op = DREG;
}


int
#ifndef __STDC__
L76bf(node)
register expnode *node;
#else
L76bf(expnode * node)
#endif
{
    expnode *p;

    switch (node->op) {
        case NAME:             /* L76d9 */
        case CONST:
            return 1;
        case STAR:             /* L76cf */
            switch ((p = node->left)->op) {
                case CONST:    /* L76d9 */
                case NAME:
                case YREG:
                case UREG:
                    return 1;
                default:       /* L76de */
                    return L771d(p);
            }
    }

    return 0;
}

static int
#ifndef __STDC__
L771d(p)
register expnode *p;
#else
L771d(expnode * p)
#endif
{
    switch (p->op) {
        case PLUS:             /* L772b */
        case MINUS:
            if ((isreg((p->left)->op)) && ((p->right)->op == CONST)) {
                return 1;
            }

    }

    return 0;
}

/* **************************************************************** *
 * crf_isint () - Returns TRUE if cref->__cr18 is an int or ?       *
 * **************************************************************** */

int
#ifndef __STDC__
crf_isint(cref)
expnode *cref;
#else
crf_isint(expnode * cref)
#endif
{
    return (cref->sux < 2);
}

static int
#ifndef __STDC__
is_ref(cref)
register expnode *cref;
#else
is_ref(expnode * cref)
#endif
{
    switch (cref->op) {
        case PLUS:             /* L777f */
        case MINUS:
            if ((cref->left)->op != AMPER) {
                break;
            }
        case STRING:           /* L7789 */
        case AMPER:
            return 1;
            /* The following is just to make the code match */
#ifdef COCO
#asm
            fdb $2018
#endasm
#endif
    }

    return 0;
}

/* ******************************************************************** *
 * getinx () - NOTE: runs only if cref->vartyp & 0x8000                 *
 *      Adds value stored in cref->cmdval to value contained in REG X   *
 * If run:                                                              *
 * Sets     cref->vartyp = XIND                                         *
 *          cref->cmdval = 0                                            *
 * ******************************************************************** */

void
#ifndef __STDC__
getinx(cref)
register expnode *cref;
#else
getinx(expnode * cref)
#endif
{
    if (cref->op & 0x8000) {
        cref->op &= 0x7fff;
        /* following generates "leax <cref->cmdval>,x"
         *      if cref->op != CTOI
         * otherwise:
         *  sex
         *  tfr x,d
         *  addd #<cref->cmdval>
         */
        gen(LOAD, XREG, NODE, cref);
        cref->op = XIND;
        cref->val.num = 0;
    }
}

static void
#ifndef __STDC__
L77e6(node)
expnode *node;
#else
L77e6(expnode * node)
#endif
{
#ifdef COCO
    int var2;                   /* unused - may delete when debugging is complete */
#endif
    expnode *rhs;

    register expnode *lhs;

    rhs = node->right;
    lhs = node->left;

    if (isreg(lhs->op)) {       /* else L787c */
        if (L771d(rhs)) {
            tranxexp(rhs);
        } else {
            tranexp(rhs);
        }

        switch (rhs->op) {
            case AMPER:        /* L782a */
                gen(LOADIM, lhs->op, NODE, rhs->left);
                break;
            case CTOI:         /* L783e */
                gen(LOAD, DREG, NODE, rhs);
                /* fall through to default */
            default:           /* L7856 */
                gen(LOAD, lhs->op, NODE, rhs);
                break;
        }

        node->op = lhs->op;     /* L7a13 */
        node->val.num = 0;
        return;
    }

    /* L787c */
    if (((L76bf(lhs)) && (lhs->type != CHAR)) && ((L771d(rhs)) || (is_ref(rhs)))) {     /* else L78b4 */
        tranexp(lhs);             /* L78a3 */
        loadxexp(rhs);             /* go to L792a */
    } else {
        /* L78b4 */
        if (isreg(rhs->op)) {   /* else L78db */
            tranexp(lhs);

            if (lhs->type == CHAR) {    /* else L792c */
                lddexp(rhs);    /* go to L792c (L792a) */
            }
        } else {
            if (crf_isint(lhs)) {       /* L78db */
                lddexp(rhs);
                tranexp(lhs);     /* go to L792a */
            } else {
                tranexp(lhs);     /* L78f4 */

                if (!L76bf(rhs)) {      /* else L7923 */
                    switch (lhs->op & 0x7fff) {
                        case UIND:     /* L7923 */
                        case YIND:
                            break;
                        default:       /* L790e */
                            L7b83(lhs); /* RegX + lhs->cmdval, pshs x */
                            break;
                    }
                }

                loadexp(rhs);     /* L7923 - I didn't have this in before */
            }
        }

    }

    gen(STORE, rhs->op, NODE, lhs);     /* L792c */
    node->op = rhs->op;         /* L7a15 */
    node->val.num = 0;
    return;                     /* L7d15 */
}

/* FIXME - the loop above may not be right.. some loops may end with L792c
 *          I think it's fixed */

/* ******************************************************************** *
 * assop () - Handles operations where a variable is modified by some   *
 *            math operation, such as "+=", "%=" or any of the other    *
 *            ops.
 * ******************************************************************** */

static void
#ifndef __STDC__
assop(node, op)
expnode *node;
int op;
#else
assop(expnode * node, int op)
#endif
{
    expnode *rhs;
    int var0;
    register expnode *lhs;

    rhs = node->right;
    lhs = node->left;
    tranexp(lhs);
    op = op - 80;               /* rest to simple math function */

    if (lhs->type == UNSIGN) {  /* else L7998 */
        switch (op) {
            case DIV:          /* L7978 */
                op = UDIV;
                break;
            case SHR:          /* L797d */
                op = USHR;
                break;
            case MOD:          /* L7982 */
                op = UMOD;
                break;
        }
    }

    if (isreg(var0 = (lhs->op & 0x7fff))) {     /* else L7a6e */
        switch (op) {
            case PLUS:         /* L79ae */
            case MINUS:
                if (rhs->op == CONST) { /* else L79e5 */
                    /* cast parm 4 to satify prototype */
                    gen(LEA, var0, CONST,
                        (expnode *)(op == PLUS ? rhs->val.num : -(rhs->val.num)));
                } else {
                    lddexp(rhs);

                    if (op == MINUS) {
                        gen(NEG NUL3);
                    }

                    gen(LEA, var0, DREG NUL1);
                }

                node->op = lhs->op;     /* L7a13 */
                node->val.num = 0;
                return;
            case AND:          /* L7a22 */
            case OR:
            case XOR:
                goto L7a6c;
            default:           /* L7a26 */
                gen(PUSH, lhs->op NUL2);
                lddexp(rhs);
                gen(op NUL3);
                break;          /* go to L7b63 */
        }
    } else {
      L7a6c:
        gen(LOAD, DREG, NODE, lhs);

        if (lhs->type == CHAR) {
            gen(CTOI NUL3);
        }

        switch (op) {
            case AND:          /* L7a9b */
            case OR:
            case XOR:
            case PLUS:
            case MINUS:
                if (L76bf(rhs)) {       /* else L7aec (default) */
                    tranexp(rhs);

                    switch (op) {
                        case AND:      /* L7ab3 */
                        case OR:
                        case XOR:
                            getinx(rhs);
                            break;
                    }

                    gen(op, DREG, NODE, rhs);
                    break;
                }
                /* Fall through to default */
            default:           /* L7aec */
                if ((var0 != YIND) && (var0 != UIND)) {
                    L7b83(lhs);
                }

                gen(PUSH, DREG NUL2);
                lddexp(rhs);

                if (op == MINUS) {
                    op = 79;
                }

                gen(op, DREG, STACK NUL1);
                break;
        }                       /* end switch */
    }                           /* end else = (lhs->op & 0x7fff) == 0  */

    /* L7b63 */
    gen(STORE, DREG, NODE, lhs);
    node->op = DREG;
}

/* **************************************************************** *
 * L7b83 () - Adds value stored in cref->cmdval to value in RegX    *
 *      then pushes RegX onto stack                                 *
 *      for variables NOT NAME                                  *
 * **************************************************************** */

static void
#ifndef __STDC__
L7b83(cref)
register expnode *cref;
#else
L7b83(expnode * cref)
#endif
{
    if ((cref->op & 0x7fff) == NAME) {
        return;
    }
    getinx(cref);               /* RegX + cref->cmdval if cref->op & 0x8000 */

    if (cref->val.num) {        /* If getinx () ran */
        /* following gen generates
         *  leax <cref->cmdval>,x
         */
        /* cast parm 4 to satisfy prototype */
        gen(LEA, XREG, CONST, (expnode *)(cref->val.num));
    }

    gen(PUSH, XREG NUL2);       /* "pshs x" */
    cref->op = STACK | 0x8000;  /* -32658 */
}

/* ******************************************************************** *
 * dostar () Handles a pointer reference                           *
 * Passed: The expnode to be processed                                  *
 * ******************************************************************** */

void
#ifndef __STDC__
dostar(node)
register expnode *node;
#else
dostar(expnode * node)
#endif
{
    expnode *lhs;
    int op;

    tranxexp(lhs = node->left);

    if ((op = lhs->op) & 0x8000) {      /* else L7c56 */
        switch (op &= 0x7fff) {
            case NAME:         /* L7c02 */
            case XIND:
            case YIND:
            case UIND:
                /* generate "ldx ... */
                gen(LOAD, XREG, op, (expnode *)(lhs->val.num));
                break;
            default:           /* L7c1d */
                comperr(lhs, "indirection");
                break;
        }

        op = XIND | 0x8000;     /* -32621 *//* L7c4a */
        node->val.num = 0;
        /* go to L7d11 */
    } else {                    /* else ! op & 0x8000 */
        /* L7c56 */
        switch (op) {
            case UREG:         /* L7c5b */
                op = UIND;
                goto cp_cmdval;
            case YREG:         /* L7c60 */
                op = YIND;
                goto cp_cmdval;
            case XREG:         /* L7c65 */
                op = XIND;
                goto cp_cmdval;
            case XIND:         /* L7c6a */
            case YIND:
            case UIND:
                op |= 0x8000;
                goto cp_cmdval;
            case NAME:         /* L7c74 */
                op = NAME | 0x8000;     /* -32716 */
                node->modifier = lhs->modifier;
              cp_cmdval:
                node->val.sp = lhs->val.sp;
                break;
            case DREG:         /* L7c8b */
                gen(LOAD, XREG, NODE, lhs);
                op = XIND;
                node->val.num = 0;
                break;
            case CONST:        /* L7cae */
                node->val.num = 0;
                node->modifier = lhs->val.num;
                op = NAME;
                break;
            default:           /* L7cbe */
                comperr(node, "indirection");
                op = XIND;
                break;
        }
    }                           /* end else i.e. lhs->op) & 0x8000 == 0 */

    node->op=op;
}

static void
#ifndef __STDC__
dotoggle(node, dest)
expnode *node;
int dest;
#else
dotoggle(expnode * node, int dest)
#endif
{
    int size;
#ifdef COCO
    int var4;                   /* unused - may delete when debugging is complete */
#endif
    int op, reg;
    register expnode *lhs;

    lhs = node->left;

    tranexp(lhs);
    op = node->op;

    if (isreg(reg = lhs->op)) {
        /* else L7d79 */
        switch (op) {
            case INCAFT:
            case DECAFT:
                if (dest == DREG) {     /* else L7d65 */
                    gen(LOAD, dest, NODE, lhs);
                    break;
                }
            default:
                dest = reg;
                break;          /* go to L7daa */
        }
    } else {
        if ((dest == XREG) && ((reg & 0x7fff) != NAME)) { /* L7d79 */
            dest = DREG;
        }

        gen(LOAD, dest, NODE, lhs);
        reg = dest;
    }

    size = node->val.num;
    /* L7daa */

    switch (op) {
        case DECBEF:           /* L7db4 */
        case DECAFT:
            size = -size;
        default:
			reg == DREG ? gen(PLUS, reg, CONST, (expnode *)size)
						: gen(LEA, reg, CONST, (expnode *)size);
            break;
    }

    gen(STORE, reg, NODE, lhs);

    switch (op) {
        default:               /* L7e16 */
            node->val.num = 0;
            break;
        case INCAFT:           /* L7e1a */
        case DECAFT:
            if (reg == DREG) {
                /* cast parm 4 to satify prototype */
                gen(MINUS, DREG, CONST, (expnode *)size);
            } else {
                node->val.num = -size;
            }
            break;
    }

    node->op = dest;
    shiftflag = 0;
}

static void
#ifndef __STDC__
loadxexp(p)
register expnode *p;
#else
loadxexp(expnode * p)
#endif
{
    tranxexp(p);

    if ((p->op != XREG) || (p->val.num != 0)) { /* else L7e95 */
        gen(LOAD, XREG, NODE, p);
    }

    p->op = XREG;
}

static void
#ifndef __STDC__
tranxexp(node)
expnode *node;
#else
tranxexp(expnode * node)
#endif
{
    expnode *rhs;
    int rhsval;
    int op;
    int newop;

    register expnode *lhs;

    lhs = node->left;
    rhs = node->right;
    newop = XREG;

    switch (op = node->op) {
        case STAR:
            /* L7ec0 */
            dostar(node);
        case NAME:             /* L812e */
        case YREG:
        case UREG:
        case CONST:
        case STRING:
            return;
        case INCAFT:           /* L7eca */
        case INCBEF:
        case DECBEF:
        case DECAFT:
            dotoggle(node,XREG);
            return;
        case AMPER:
            gen(LOADIM, XREG, NODE, lhs);
            node->val.num = 0;
            break;
        case CTOI:
            /* L7ef6 */
            lddexp(node);
            return;
        case MINUS:
            if ((lhs->op != AMPER) && (rhs->sux)) {     /* go to L7faf */
                goto cant;
            }
        case PLUS:
            /* L7f11 */
            if (isreg(lhs->op) || (lhs->op == AMPER)) {       /* else L7f78 */
                /* L7f24 */
                if (rhs->op == CONST) {
                    /* else L7f40 */
                    tranxexp(lhs);
                    newop = lhs->op;
                    rhsval = rhs->val.num;

                    /* jump to L80a5 */

                    /*if (op == PLUS)
                       {
                       lhs->cmdval += rhsval;
                       }
                       else
                       {
                       lhs->cmdval -= rhsval;
                       }

                       break; */
                } else {
                    lddexp(rhs);
                    /* L7f40 */
                    tranxexp(lhs);

                    if (op == MINUS) {     /* else L7f62 */
                        gen(NEG NUL3);
                    }

                    gen(LEAX, DREG, lhs->op NUL1);      /* L7f62 */
                    rhsval = 0;
                }
            } else {            /* L7f78 */
                if ((op == PLUS) && (lhs->sux < rhs->sux)) {
                    expnode *_tmpref = lhs;

                    /* Swap left for right */

                    lhs = rhs;
                    rhs = _tmpref;
                }

                if (!L76bf(rhs)) {      /* L7f97 */
                    /* else L7fb4 */
                    if (!isreg(rhs->op)) {
                        goto cant;
                    }
                }

                tranxexp(lhs);     /* L7fb4 */

                switch(lhs->op & NOTIND) {
                    case DREG:
                        /* L7fc4 */
                        if ((op == PLUS) && (rhs->op != CONST)) {
                                /* else L7fe8 */
                            ldxexp(rhs);
                            lhs->val.num = 0;
                            goto L8089;
                        }


                    case NAME:
                    case STRING:
                    case XIND: /* L7fe8 */
                    case YIND:
                    case UIND:
                        gen(LOAD, XREG, NODE, lhs);
                        lhs->val.num = 0;
                        break;
                    case XREG: /* L8054 */
                        break;
                    case YREG: /* L8004 */
                    case UREG:
                        newop = lhs->op;
                        break;
                    default:   /* L800a */
                        comperr(lhs, "x translate");
                        break;
                }

                if (rhs->op == CONST) { /* L8054 */
                    rhsval = rhs->val.num;
                } else {
                    lddexp(rhs);

                    if (op == MINUS) {     /* else L808b */
                        gen(NEG, 0, 0 NUL1);
                    }

                  L8089:
                    gen(LEAX, DREG, newop NUL1);
                    newop = XREG;
                    rhsval = 0;
                }
            }

            /* L80a5 */
            node->val.num = lhs->val.num +
                ((op == PLUS) ? rhsval : -(rhsval));

            break;
        default:
            /* L80c3 */
cant:       tranexp(node);
            return;
    }

    node->op=newop;
    /* L8128 */
}

int
#ifndef __STDC__
isreg(r)
int r;
#else
isreg(int r)
#endif
{
    return (r == UREG) || (r == YREG);
}
