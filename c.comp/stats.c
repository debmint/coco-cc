/* ******************************************************** *
 * comp_05.c                                                *
 * Corresponds to p1_04.c                                   *
 * $Id::                                                    $
 * ******************************************************** */

#include "cj.h"

#define CASESIZE sizeof(casnode)

casnode *deflabel;                   /* switch default label */
labstruc *breakptr,      /* break label */
         *contptr;       /* continue label */

static symnode *checklabel();
static expnode *getptest();
static expnode *gettest();

static expnode *sidexp(
#ifdef __STDC__
                         int p1
#endif
    );
static void dotest(
#ifdef __STDC__
                      register expnode * regptr, labstruc * p2,
                      labstruc * p3, int p4
#endif
    );
static void doif();
static void dowhile();
static void doswitch();
static void docase();
static void dodefault();
static void swcherr();
static void dodo();
static void dofor();
static void doreturn();
static void dobreak();
static void docont();
static void dogoto();
static void dolabel();
static expnode *emit(
#ifdef __STDC__
                         expnode *
#endif
    );


void
#ifdef __STDC__
statement(void)
#else
statement()
#endif
{
    if (sym != SEMICOL)
        lastst = 0;

contin:
    switch (sym) {
        case SEMICOL:          /* L2b10 *//* '(' */
            break;
        case KEYWORD:
            switch (symval) {
                case IF:       /* L29f6 */
                    doif();
                    return;
                case WHILE:    /* L29fb */
                    dowhile();
                    return;
                case RETURN:   /* L2a00 */
                    doreturn();
                    break;      /* go to L2b10 */
                case CASE:     /* L2a06 */
                    docase();
                    return;
                case SWITCH:   /* L2a0b */
                    doswitch();
                    return;
                case BREAK:    /* L2a10 */
                    dobreak();
                    break;      /* go to L2b10 */
                case CONTIN:   /* L2a16 */
                    docont();
                    break;
                case DEFAULT:  /* L2a1c */
                    dodefault();
                    return;
                case FOR:      /* L2a21 */
                    dofor();
                    return;
                case DO:       /* L2a26 */
                    dodo();
                    break;
                case GOTO:     /* L2a2c */
                    dogoto();
                    break;      /* go to L2b10 */
                case ELSE:     /* L2a32 */
                    error("no 'if' for 'else'");
                    getsym();
                    goto contin;
                default:       /* L2a43 */
                    goto what;
            }

            break;
        case LBRACE:           /* L2a86 */
            block();
            getsym();
            return;
        case -1:               /* L2a8e */
            return;
        case NAME:
            /* L2a90 */
            blanks();

            if (cc==':') {
                dolabel();
                goto contin;
            }
            goto tryexp;
        default:
            /* L2aa7 */
what:
            if (issclass() || istype()) {
                error("illegal declaration");

                do {
                    getsym();
                } while (sym != SEMICOL);

                break;
            } else {
tryexp:
                if (!sidexp(0)) {
                    error("syntax error");
                    junk();
                    return;
                }
            }

            break;
    }

    need(SEMICOL);
    /* L2b10 */
}

static void
#ifdef __STDC__
doif(void)
#else
doif()
#endif
{
    register labstruc *tl, *fl, *savcont;
    expnode *ptr;

    getsym();
    ptr = getptest();
    tl = (labstruc *)(++curlabel);
    fl = (labstruc *)(++curlabel);

    if (sym == SEMICOL) {       /* else L2b6b */
        getsym();
        dotest(ptr, tl, fl, FALSE);
        label((int)fl);
        savcont = tl;
    } else {
        dotest(ptr, tl, fl, TRUE);
        label((int)tl);
        statement();
        savcont = fl;
    }

    /* L2b8d */
    if ((sym == KEYWORD) && (symval == ELSE)) {
        getsym();

        if (sym != SEMICOL) {   /* else L2bd2 */
            if (tl != savcont) {     /* else L2bcf */
                gen(124, (int)(savcont = (labstruc *)(++curlabel)), 0 NUL1);
                label((int)fl);
            }

            statement();
        }
    }

    label((int)savcont);                  /* L2bd2 */
}

static void
#ifdef __STDC__
dowhile(void)
#else
dowhile()
#endif
{
    labstruc *v8;
    int v6;
    labstruc *savbreak, *savcont;
    expnode *ptr;
    register int tlab;

    v8 = D0033;
    v6 = D0035;
    savbreak = breakptr;
    savcont = contptr;

    getsym();

    D0033 = (labstruc *)(++curlabel);
    breakptr = contptr = (labstruc *)sp;
    D0035 = ++curlabel;
    ptr = getptest();
    if (sym == SEMICOL)
        tlab = D0035;
    else {
        gen(JMP, D0035, 0 NUL1);
        label(tlab = ++curlabel);
        statement();
    }

    label(D0035);               /* L2c52 */
    dotest(ptr, (labstruc *)tlab, D0033, FALSE);
    label((int)D0033);
    D0033 = v8;

    D0035 = v6;
    breakptr = savbreak;
    contptr = savcont;
}

static void
#ifdef __STDC__
doswitch(void)
#else
doswitch()
#endif
{
    register expnode *ptr;
    labstruc *old_D0033;
    casnode *savcase;
    casnode *savdef;
    int tests;
    casnode *temp;
    labstruc *savbreak;
    casnode *savlast;

    getsym();
    ++swflag;
    savlast = lastcase;
    savcase = caseptr;
    caseptr = 0;
    old_D0033 = D0033;
    savdef = deflabel;
    savbreak = breakptr;
    D0033 = (labstruc *)(++curlabel);
    breakptr = (labstruc *)sp;
    deflabel = 0;

    /* get the case value */

    need(LPAREN);
    if ((ptr = optim(parsexp(0)))) {    /* else L2d43 */
        chkdecl(ptr);

        switch (ptr->type) {
            case CHAR:         /* L2cfd */
            case LONG:
                cvt(ptr, INT);
                break;
            case INT:          /* L2d33 */
            case UNSIGN:
                break;
            default:           /* L2d0b */
                notintegral(ptr);
                makedummy(ptr);
                break;
        }

        ldxexp(ptr);
        reltree(ptr);
    } else {
        experr();             /* L2d43 */
    }

    need(RPAREN);
    gen(JMP, (tests = ++curlabel), 0 NUL1);
    statement();

    if (!lastst) {
        gen(JMP, (int)D0033, 0 NUL1);
    }

    label(tests);               /* L2d82 */
    ptr = (expnode *) caseptr;

    while (ptr) {               /* L2dad */
        temp = ((casnode *) ptr)->clink;
        gen(JMPEQ, ((casnode *) ptr)->clab, ((casnode *) ptr)->cval NUL1);
        ((casnode *) ptr)->clink = freecase;
        freecase = (casnode *) ptr;
        ptr = (expnode *) temp;
    }

    /* done with ptr, release it */

    if (deflabel) {
        gen(JMP, (int)deflabel, 0 NUL1);
    }

    label((int)D0033);               /* L2dcb */
    caseptr = savcase;
    deflabel = savdef;
    --swflag;
    lastcase = savlast;
    D0033 = old_D0033;
    breakptr = savbreak;
}

static void
#ifdef __STDC__
docase(void)
#else
docase()
#endif
{
    int v0;
    register casnode *ptr;

    getsym();
    v0 = getconst(0);
    need(COLON);

    if (swflag) {               /* else L2e5a */
        if ((ptr = freecase)) {
            freecase = ptr->clink;
        } else {
            ptr = (casnode *)grab(CASESIZE);       /* L2e26 */
        }

        if (caseptr) {         /* L2e32 */
            lastcase->clink = ptr;
        } else {
            caseptr = ptr;
        }

        lastcase = ptr;          /* L2e3e */
        ptr->clink = 0;
        ptr->cval = v0;
        label((ptr->clab = ++curlabel));
    } else {
        swcherr();
    }
}

static void
#ifdef __STDC__
dodefault(void)
#else
dodefault()
#endif
{
    getsym();

    if (!swflag) {
        swcherr();
    }

    if (deflabel) {
        error("multiple defaults");
    } else {
        label((int)(deflabel = (casnode *)(++curlabel)));
    }

    need(COLON);
}

static void
#ifdef __STDC__
swcherr(void)
#else
swcherr()
#endif
{
    error("no switch statement");
}

static void
#ifdef __STDC__
dodo(void)
#else
dodo()
#endif
{
    labstruc *v8;
    int v6;
    int v4;
    labstruc *savbreak, *savcont;

    v8 = D0033;
    v6 = D0035;
    savcont = contptr;
    savbreak = breakptr;
    breakptr = contptr = (labstruc *)sp;
    D0035 = ++curlabel;
    D0033 = (labstruc *)(++curlabel);
    getsym();
    label(v4 = getlabel(x));
    statement();

    if ((sym != KEYWORD) || (symval != WHILE)) {
        error("while expected");
    }

    getsym();
    label(D0035);
    dotest(getptest(), (labstruc *)v4, D0033, 0);
    label((int)D0033);
    D0033 = v8;
    D0035 = v6;
    breakptr = savbreak;
    contptr = savcont;
}

static void
#ifdef __STDC__
dofor(void)
#else
dofor()
#endif
{
    int v12;
    int slab;
    int v8;
    labstruc *savbreak, *savcont;
    expnode *tptr;
    expnode *uptr;
    register labstruc *regptr;

    tptr = 0;
    uptr = 0;
    regptr = D0033;
    v12 = D0035;
    savbreak = breakptr;
    savcont = contptr;
    contptr = breakptr = (labstruc *)sp;
    slab = ++curlabel;
    D0033 = (labstruc *)(++curlabel);
    getsym();
    need(LPAREN);
    sidexp(0);
    need(SEMICOL);

    if (sym != SEMICOL) {
        tptr = gettest();
        gen(JMP, (v8 = ++curlabel), 0 NUL1);
    }

    /* L2fe5 */
    need(SEMICOL);

    if ((uptr = optim(parsexp(0)))) {
        chkdecl(uptr);
        D0035 = ++curlabel;
    } else {
        D0035 = slab;
    }

    need(RPAREN);
    label(slab);
    statement();

    if (uptr) {
        label(D0035);
        emit(uptr);
    }

    if (tptr) {                 /* L3043 */
        label(v8);
        dotest(tptr, (labstruc *)slab, D0033, 0);
    } else {
        gen(JMP, slab, 0 NUL1);
    }

    label((int)D0033);
    D0033 = regptr;
    D0035 = v12;
    breakptr = savbreak;
    contptr = savcont;
}

static void
#ifdef __STDC__
doreturn(void)
#else
doreturn()
#endif
{
    register expnode *ptr;

    getsym();

    if ((sym != SEMICOL) && (ptr = parsexp(0))) {       /* else L3197 */
        ptr = optim(ptr);
        chkdecl(ptr);
        ckstrct(ptr);

        cvt(ptr, ((isptr(FuncGentyp)) ? 1 : FuncGentyp));

        switch (FuncGentyp) {
            case LONG:         /* L30f5 */
                lload(ptr);
                goto L3109;
            case FLOAT:        /* L3100 */
            case DOUBLE:
                dload(ptr);
              L3109:
                if (ptr->op != 128) {
                    gen(127, UREG, 128 NUL1);
                    gen(122, UREG NUL2);

                    switch (FuncGentyp) {
                        case FLOAT:    /* L313c */
                        case DOUBLE:
                            gen(DBLOP, MOVE, FuncGentyp NUL1);
                            break;
                        default:       /* L3151 */
                            gen(LONGOP, MOVE NUL2);
                            break;
                    }
                }

                break;
            default:           /* L3170 */
                lddexp(ptr);
                break;
        }

        reltree(ptr);
    }

    modstk(0);
    gen(RETURN, 0, 0 NUL1);
    lastst = RETURN;
}

static void
#ifdef __STDC__
dobreak(void)
#else
dobreak()
#endif
{
    getsym();

    if (!D0033) {
        error("break error");
    } else {
        modstk((int)breakptr);
        gen(124, (int)D0033, 0 NUL1);
    }

    lastst = BREAK;
}

static void
#ifdef __STDC__
docont(void)
#else
docont()
#endif
{
    getsym();

    if (!D0035) {
        error("continue error");
    } else {
        modstk((int)contptr);
        gen(124, D0035, 0 NUL1);
    }

    lastst = CONTIN;
}

static void
#ifdef __STDC__
dogoto(void)
#else
dogoto()
#endif
{
    register symnode *ptr;

    getsym();

    if (sym != NAME) {
        error("label required");
    } else {
        if ((ptr = checklabel())) {
            /* For non-COCO systems, we need to do the following
             * to avoid type error for assignment
             */
            gen(GOTO, ptr->offset, 0 NUL1);
            ptr->x.labflg |= 2;
        }

        getsym();
    }

    lastst = GOTO;
}

static void
#ifdef __STDC__
dolabel(void)
#else
dolabel()
#endif
{
    register symnode *ptr;

    if ((ptr = checklabel())) {
        if (ptr->storage == STATIC) {   /* STATIC */
            multidef();
        } else {

            ptr->storage = STATIC;
            gen(LABEL, ptr->offset, 0 NUL1);
            ptr->x.labflg |= 1;
        }
    }

    getsym();
    getsym();
}

static symnode *
#ifdef __STDC__
checklabel(void)
#else
checklabel()
#endif
{
    register symnode *ptr = (symnode *) symval;

    if (ptr->type != LABEL) {   /* else L33d3 */
        if (ptr->type) {
            if (ptr->blocklevel) {
                error("already a local variable");
                return 0;
            }

            pushdown(ptr);
        }

        ptr->type = LABEL;
        ptr->storage = AUTO;
        ptr->offset = ++curlabel;
        ptr->x.labflg = 0;
        ptr->blocklevel = blklev;
        ptr->snext = labelist;
        labelist = ptr;
    }

    return ptr;
}

static expnode *
#ifdef __STDC__
sidexp(int p1)
#else
sidexp(p1)
int p1;
#endif
{
    register expnode *ptr;

    if ((ptr = parsexp(p1))) {  /* else return ptr */
        ptr = emit(optim(ptr));
    }

    return ptr;
}

static expnode *
#ifdef __STDC__
emit(register expnode * ptr)
#else
emit(ptr)
register expnode *ptr;
#endif
{
    chkdecl(ptr);

    switch (ptr->op) {
            /* L3357 *//* '>' */
        case INCAFT:
            ptr->op = INCBEF;
            break;
        case DECAFT:           /* L335c *//* '?' */
            ptr->op = DECBEF;
            break;
    }

    ptr = tranexp(ptr);
    reltree(ptr);
    return ptr;
}

static expnode *
#ifdef __STDC__
getptest(void)
#else
getptest()
#endif
{
    expnode *ptr;

    need(LPAREN);
    ptr = gettest();
    need(RPAREN);
    return ptr;
}

static expnode *
#ifdef __STDC__
gettest(void)
#else
gettest()
#endif
{
    register expnode *ptr;

    if ((ptr = optim(parsexp(0)))) {
        chkdecl(ptr);
    } else {
        error("condition needed");
    }

    return ptr;
}

/* ******************************************************************** *
 * dotest () -                                                          *
 * Passed : (1) regptr - expnode under consideration                    *
 *          (2) p1 - labstruc *                                         *
 *          (3) p2 - labstruc *                                         *
 *          (4) p4 - Flag                                               *
 * ******************************************************************** */

static void
#ifdef __STDC__
dotest(register expnode *ptr, labstruc *tl, labstruc *fl, int nj)
#else
dotest(ptr, tl, fl, nj)
register expnode *ptr;
labstruc *tl;
labstruc *fl;
int nj;
#endif
{
    if (ptr) {
        tranbool(ptr,tl,fl,nj);
        reltree(ptr);
    }
}
