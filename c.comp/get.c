/* ******************************************************** *
 * ccomp_09.c - Functions dealing with reading input file   *
 * comes from p1_09.c                                       *
 * $Id:: comp_09.c 71 2008-10-01 21:12:03Z dlb            $ *
 * ******************************************************** */

#ifdef UNIX
#   include <stdlib.h>
#endif
#include "cj.h"

#define        ESCHAR          '#'
#define        CPERR          '0'
#define        FATERR         '1'
#define        ASSLINE        '2'
#define        NEWLINO        '5'
#define        SOURCE         '6'
#define        NEWFNAME       '7'
#define        ARTHFLG        '8'
#define        PSECT          'P'

#define isdigit(c) (chartab[c] == DIGIT)

static char *maxx = &line[255];
char temp[256];

extern char chartab[];
static char getline2();
#ifndef UNIX
static int atoi();
#endif

static char *cgets();

/* ******************************************************************** *
 * initbuf0() - startup routine to initialize certain buffers           *
 * ******************************************************************** */

void
#ifdef __STDC__
initbuf0(void)
#else
initbuf0()
#endif
{
    lptr = line;
    line[0] = '\0';
    cc=' ';
}

/* ************************************************************* *
 * blanks () a getch() function that bypasses whitespaces  *
 * ************************************************************* */

void
#ifdef __STDC__
blanks(void)
#else
blanks()
#endif
{
    while ((cc == ' ') || (cc == '\t')) {
        getch();
    }
}

/* ***************************************************************** *
 * getch () - places next character into the variable cc.     *
 *    gets next applicable line if current line is exhausted..       *
 * ***************************************************************** */

void
#ifdef __STDC__
getch(void)
#else
getch()
#endif
{
    if ((cc = *(lptr++)) == '\0') {
        cc = getline2();
    }
}

static char
#ifdef __STDC__
getline2(void)
#else
getline2()
#endif
{
    int lno, n, x;
#ifdef DEVEL
    int v0;                     /* unused - may delete when finished debugging */
#endif

    if(lineflg == 0) {
        lineflg = 1;
        lptr = "";
        return ' ';
    }

    lineflg = 0;
    /* L42f9 */
    strcpy(lastline,line);

    for(;;) {
        /* L430e */
        if (!(lptr = cgets())) {
            return -1;
        }

        if(*lptr == ESCHAR) {
            n = lptr[1];

            if (!(cgets()))        /* else L4403 */
                return -1;

            switch(n) {
                case NEWLINO:
                    lineno=atoi(line);
                    continue;
                case SOURCE:
                    comment();
                case ASSLINE:
                    fprintf(code,"%s\n",line);
                    continue;
                case NEWFNAME:
                    /* '8' is not a standard COCO directive, but is from
                     *     OSK "cpp".  It appears to signify the filename
                     *     to return to from an #include.  COCO simply
                     *     uses '7', but we will use it in order to try
                     *     to use OSK's "cpp".
                     */
#ifndef COCO
                case '8':
#endif
                    /* L4354 */
                    strcpy(filename,line);

                    if(cgets() == NULL)
                        return EOF;
                    continue;
                case PSECT:
                    strcpy(temp,line);

                    if(cgets() == NULL)
                        return EOF;

                    fprintf(code," psect %s,0,0,%d,0,0\n",
                            temp, atoi(line));
                    fprintf(code," nam %s\n",temp);
                    continue;
                case CPERR:
                case FATERR:
                    strcpy(temp,line);

                    if(cgets() == NULL)
                        return EOF;

                    lno = atoi(line);

                    if(cgets() == NULL)
                        return EOF;

                    x = atoi(line);

                    if(cgets() == NULL)
                        return EOF;

                    if (lno)
                        printf("%s : line %d ",filename,lno );
                    else
                        printf("argument : ");

                    printf("**** %s ****\n",line);

                    if(temp[0]){
                        puts(temp);

                        for(;x--;)
                            putchar(' ');

                        puts("^");
                    }

                    if (n == '1') {
#ifndef DEVEL
                        tidy();
#endif
                        exit(1);
                    }

                    continue;
            }
        }
        else {
            /* L44c5 here */
            ++lineno;
            return ' ';
        }
    }
}

#ifndef UNIX
static int
atoi(s)
register char *s;
{
    int c;
    int n = 0;

    while (isdigit(c = *(s++))) {
        n = (n * 10) + (c - '0');
    }

    return n;
}
#endif

static char *
#ifdef __STDC__
cgets(void)
#else
cgets()           /* Function to input a line of source.
                         * Returns a pointer to the start of the line
                         * if all is well.
                         * Returns NULL on end of file.
                         * 'exit' on error or line too long
                         */
#endif
{
    int c;
    register char *lp = line;

    if((c = getc(in)) == EOF){
        /* else L4579 */
        if(ferror(in)) {
            fputs("INPUT FILE ERROR : TEMPORARY FILE\n",stderr);
            exit(1);
        }

        return NULL;
    }

    while(lp != maxx) {
        switch (c) {
            case '\n':
            case EOF:
                *lp = '\0';
                return line;    /* normal return point */
            default:
                *(lp++) = c;
                c = getc(in);
        }
    }

        /* if we get here, lp == max; i.e. too far */
    symline = ++lineno;             /* fix up for 'error' */
    symptr = line;
    fatal("input line too long");   /* NO RETURN ! */
#ifndef DEVEL
    return NULL;
#endif
}
