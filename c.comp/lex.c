/* **************************************************************** *
 * comp_12.c - part 12 for c.comp                                   *
 *                                                                  *
 * comes from p1_08.c                                               *
 *                                                                  *
 *           SCANNER OR LEXICAL ANALYZER                            *
 *                                                                  *
 *                                                                  *
 * $Id:: comp_12.c 73 2008-10-03 20:20:29Z dlb                  $   *
 * **************************************************************** */


#include <stdlib.h>
#include "cj.h"

extern char *grab();

#ifndef COCO
/*#   include <ctype.h>*/
#include <unistd.h>
int *cocodbl(
#ifdef __STDC__
                char *, int *
#endif
    );
#endif

typedef union {
    INTTYPE *ip;
    LONGTYPE *lp;
    FLOATYPE *fp;
    DBLETYPE *dp;
} numptrs;

union data_tys {
    long l;
    double d;
    int i;
    char c[9];
};

#ifdef COCO
extern int _atoftbl[];
#else
int _atoftbl[] = {
    0x0000, 0x0000, 0x0000, 0x0081,
    0x2000, 0x0000, 0x0000, 0x0084,
    0x4800, 0x0000, 0x0000, 0x0087,
    0x7a00, 0x0000, 0x0000, 0x008a,
    0x1c40, 0x0000, 0x0000, 0x008e,
    0x4350, 0x0000, 0x0000, 0x0091,
    0x7424, 0x0000, 0x0000, 0x0094,
    0x1896, 0x8000, 0x0000, 0x0098,
    0x3ebc, 0x2000, 0x0000, 0x009b,
    0x6e6b, 0x2800, 0x0000, 0x009e,
    0x1502, 0xf900, 0x0000, 0x00a2,
    0x2d78, 0xebc5, 0xac62, 0x00c3,
    0x49f2, 0xc9cd, 0x0467, 0x4fe4,
};
#endif

#ifdef __STDC__
symnode *lookup(char *);
static int numshf(char *);
static double scale0(double, int, int);
#else
symnode *lookup();
static double scale();
static double scale0();
#endif

static int an(
#ifdef __STDC__
                 int
#endif
    );

#ifdef COCO
double _dnorm();
#else
#ifndef OSK
#ifndef direct
#define direct
#endif
#endif
#endif

char chartab[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    109, 64, 105, 0, 0, 84, 65, 104, 45, 46, 66, 80, 48, 67, 69, 83,
    107, 107, 107, 107, 107, 107, 107, 107, 107, 107, 47, 40, 93, 120, 95,
        100,
    0, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106,
        106, 106,
    106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 43, 102, 44, 89,
        106,
    0, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106,
        106, 106,
    106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 41, 88, 42, 68,
        0,
};

char valtab[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 9, 0, 0, 0, 13, 14, 0, 0, 0, 14, 12, 1, 14, 15, 13,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 10, 2, 10, 3,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 14, 0
};

#define isletter(c) (chartab[c]==(char)LETTER)
#define isdigit(c) (chartab[(int)c]==DIGIT)

static void getword(
#ifdef __STDC__
                       char *
#endif
    );

static int number(
#ifdef __STDC__
                     int, DBLETYPE *
#endif
    );

static void pstr();
static void qstr();

static void install(
#ifdef __STDC__
                       char *, int
#endif
    );

static unsigned int hash(
#ifdef __STDC__
                            char *
#endif
    );

static int dobslash();

static void oc(
#ifdef __STDC__
                  int
#endif
    );

static int isoct(
#ifdef __STDC__
                    char
#endif
    );

static int ishex(
#ifdef __STDC__
                    char
#endif
    );





void
getsym()
{
    int numtype;
    char name[NAMESIZE + 1];
    numptrs np;
    DBLETYPE n;
#ifndef COCO
    INTTYPE coc_dbl[4];
#endif

    blanks();

    if (cc == EOF)
    {
        sym = EOF;
        return;
    }

    symptr = lptr - 1;
    symline = lineno;

    while (!(sym = chartab[(int)cc]))
    {
        error("bad character");
        getch();
        symptr = lptr;
    }

    symval = valtab[(int)cc];

    switch(sym)
    {
            register symnode *ptr;

        case LETTER:
            getword(name);
            ptr=lookup(name);

            if ((sym=ptr->type)==KEYWORD)
            {
                if ((symval=ptr->storage)==SIZEOF)
                {
                    sym = SIZEOF;
                    symval = EXTERN;    /* 14 */
                }
            } else {
                sym = NAME;
                symval = (int)ptr;
            }

            break;

        case DIGIT:
            /* L5fc4 */
            numtype = number(INT, &n);
donum:
            np.dp = &n;

            switch(numtype) {
                    /* L5fe3 */
                case INT:
#ifdef COCO
                    symval = *np.ip;
#else
                    symval = *np.dp;
#endif
                    sym = CONST;
                    break;
                case LONG:
                    /* L5fe8 */
                    ptr = (symnode *)grab(LONGSIZE);
#ifdef COCO
                    *(long *)ptr = *np.lp;
#else
                    *(long *)ptr = *np.dp;
#endif
#ifdef DEBUG
                    fprintf(stderr,"getsym: symval=%04X, *longp=%08lX\n",symval,*longp);
#endif
                    symval = (int) ptr;
/*#else
                    symval = *np.ip;
#endif*/
                    sym = LCONST;
                    break;
                case DOUBLE:
                    /* L6004 */
/*#ifdef COCO*/
                    ptr = (symnode *)grab(DOUBLESIZE);
                    *(double *)ptr = *np.dp;
/*#else
                    ptr = grab(sizeof (struct dbltree));
                    ((struct dbltree *)ptr)->native = *np.dp;
                    memcpy(((struct dbltree *)ptr)->cocoarr, coc_dbl,
                            sizeof (coc_dbl));
#endif*/
                    symval = (int)ptr;
                    sym = FCONST;
                    break;
                default:
                    /* L6020 */
                    error("constant overflow");
                    symval = 1;
                    sym = CONST;
                    break;
            }

            break;

        case PRIME:            /* ' (single quote) *//* L6049 */
            pstr();
            sym = CONST;
            break;
        case QUOTE:            /* " (double quote) *//* L6051 */
            qstr();
            sym = STRING;
            break;
        default:               /* L605c */
            getch();

            switch(sym) {
                case DOT:
                    /* L6064 */
                    if(isdigit(cc)) {
                        numtype=number(DOUBLE,&n);
                        goto donum;
                    }

                    break;
                case AMPER:
                    /* L608c */
                    switch(cc) {
                        case '&':
                            sym=DBLAND;
                            getch();
                            symval=5;
                            break;
                        case '=':
                            /* L60a0 */
                            sym=ASSAND;
                            symval=2;
                            getch();
                            break;
                    }

                    break;
                case ASSIGN:
                    /* L60b8 */
                    if (cc=='=') {
                        sym=EQ;
                        symval=9;
                        getch();
                    }

                    break;
                case OR:       /* L60cb */
                    switch (cc) {
                        case '|':
                            /* L60d2 */
                            sym=DBLOR;
                            getch();
                            symval=4;
                            break;
                        case '=':
                            sym=ASSOR;
                            getch();
                            symval=2;
                            break;
                    }

                    break;
                case NOT:
                    /* L60fc */
                    if(cc=='=') {
                        sym=NEQ;
                        getch();
                        symval=9;
                    }

                    break;
                case STAR:
                    /* L6111 */
                    if (cc=='=') {
                        sym=ASSMUL;
                        getch();
                        symval=2;
                    }

                    break;
                case DIV:
                case MOD:
                case XOR:
                    if(cc=='=') {
                        sym+=(ASSPLUS-PLUS);
                        getch();
                        symval=2;
                    }

                    break;
                case LT:
                    /* L612e */
                    switch (cc) {
                        case '<':
                            sym=SHL;
                            symval=11;
                            getch();

                            if (cc=='='){
                                sym=ASSSHL;
                                symval=2;
                                getch();
                            }

                            break;
                        case '=':
                            /* L6155 */
                            sym=LEQ;
                            symval=10;
                            getch();
                            break;
                    }

                    break;
                case GT:
                    /* L616d */
                    switch (cc) {
                        case '>':
                            sym=SHR;
                            symval=11;
                            getch();

                            if (cc=='=') {
                                sym=ASSSHR;
                                symval=2;
                                getch();
                            }

                            break;
                        case '=':
                            /* L6194 */
                            sym=GEQ;
                            symval=10;
                            getch();
                            break;
                    }

                    break;
                case PLUS:     /* L61ac */
                    switch(cc){
                        case '+':
                            /* L61b3 */
                            sym=INCBEF;
                            symval=14;
                            getch();
                            break;
                        case '=':
                            /* L61bd */
                            sym=ASSPLUS;
                            symval=2;
                            getch();
                            break;
                    }

                    break;
                case NEG:
                    /* L61d4 */
                    switch(cc){
                            /* L61db */
                        case '-':
                            sym=DECBEF;
                            symval=14;
                            getch();
                            break;
                            /* L61e5 */
                        case '=':
                            sym=ASSMIN;
                            symval=2;
                            getch();
                            break;
                        case '>':
                            sym=ARROW;
                            symval=15;
                            getch();
                            break;
                    }

                    break;
            }

            break;
    }
}

/* ******************************************************************** *
 * lexinit () - Installs built-in names into the tables                *
 * ******************************************************************** */

void
#ifdef __STDC__
lexinit(void)
#else
lexinit()
#endif
{
    register symnode *ptr;

    install("double",DOUBLE);
    install("float",FLOAT);
    install("typedef",TYPEDEF);
    install("static",STATIC);
    install("sizeof",SIZEOF);
    install("int",INT);
    mosflg=1;
    install("int",INT);
    install("float",FLOAT);
    mosflg=0;
    install("char",CHAR);
    install("short",SHORT);
    install("auto",AUTO);
    install("extern",EXTERN);
    install("direct",DIRECT);
    install("register",REG);
    install("goto",GOTO);
    install("return",RETURN);
    install("if",IF);
    install("while",WHILE);
    install("else",ELSE);
    install("switch",SWITCH);
    install("case",CASE);
    install("break",BREAK);
    install("continue",CONTIN);
    install("do",DO);
    install("default",DEFAULT);
    install("for",FOR);
    install("struct",STRUCT);
    install("union",UNION);
    install("unsigned",UNSIGN);
    install("long",LONG);

    ptr=lookup("errno");
    ptr->type=INT;
    ptr->storage=EXTERN;
    ptr->size=INTSIZE;
    ptr=lookup("lseek");
    ptr->type=LONG | FUNCTION;
    ptr->storage=EXTDEF;
    ptr->size=LONGSIZE;
}

/* **************************************************************** *
 * getword () - read a label name from input stream int _dest.     *
 *      ( up to LBLLEN chars), and set file position to end of      *
 *      the string                                                  *
 * **************************************************************** */

static void
#ifdef __STDC__
getword(register char *name)
#else
getword(name)
register char *name;
#endif
{
    int count;

    for (count=1; an(cc) && count<=NAMESIZE ; ++count) {
        *(name++) = cc;
        getch();
    }

    if(count == 2) *(name++) = UNIQUE;
    *name='\0';

    while(an(cc)) getch();
}

/* A chartab type function */

static int
#ifdef __STDC__
an(int c)
#else
an(c)
int c;
#endif
{
    return ((chartab[c] == LETTER) || (chartab[c] == DIGIT));
}

symnode *
#ifdef __STDC__
lookup(char *name)
#else
lookup(name)
char *name;
#endif
{
    /* return a pointer to a symbol table entry for 'name'  */
    /* if one is not found create one                       */
    register symnode *nptr,**tptr,**tab;
#ifdef COCO
    int v0;                     /* not used, but to keep code */
#endif

    /* which symbol table is it in? */
    tab = mosflg ? mostab : hashtab;
    /* point to hash table entry */
    tptr = &tab[hash(name)];

    /* chunter down the list until found or run off the end */
    for (nptr = *tptr; nptr; nptr = nptr->hlink) {
        if (nptr->sname[0] == name[0]
            && strncmp(name, nptr->sname, NAMESIZE) == 0)
            return nptr;
    }

    /* the entry cannot be found; make a new one */
    if ((nptr = freesym)) freesym = nptr->hlink;
    else nptr = (symnode *) grab(SYMSIZE);

    strncpy(nptr->sname,name,NAMESIZE);
    nptr->type = UNDECL;
    nptr->storage = 0;

    /* put the new entry on the front of the list;  the chance  */
    /* that a lookup is for a recently declared variable is     */
    /* high - this should therefore speed things up             */
    nptr->hlink = *tptr;
    *tptr = nptr;

    /* prepare for release to free list by 'clear()' */
    nptr->downptr = (symnode *) tptr;

    return nptr;
}

static void
#ifdef __STDC__
install(char *word, int typ)
#else
install(word, typ)
char *word;
int typ;
#endif
{
    register symnode *cptr;
    char neword[NAMESIZE+1];

    /* cut the word down to size */
    strncpy(neword,word,NAMESIZE);
    neword[NAMESIZE] = '\0';
    cptr=lookup(neword);

    cptr->type=KEYWORD;
    cptr->storage = typ;
}

static unsigned int
#ifdef __STDC__
hash(register char *word)
#else
hash(word)
register char *word;
#endif
{
    unsigned int n = 0, c;

    while ((c = *(word++))) n += c;

    return (n & 127);
}


#ifdef _WIN32
#define MEMSIZ 100000

void *grab(int size)
{
    register int newLoc;

    if (!lobrk) {
        if (!(lobrk = malloc(MEMSIZ))) {
            char msg[100];

            sprintf(msg, "Cannot allocate %d bytes for Lbls Buffer",
                    MEMSIZ);
            fatal(msg);
        }

        hibrk = lobrk;
    }

    newLoc = (int) hibrk;
    hibrk = &((char *) hibrk)[size];

    if (((int) hibrk - (int) lobrk) > MEMSIZ) {
        char msg[100];

        sprintf(msg, "Memory Buffer full - %d Bytes not enough", MEMSIZ);
        fatal(msg);
    }

    return (void *) newLoc;
}

#else

char *
#ifdef __STDC__
grab(int size)
#else
grab(size)
int size;
#endif
{
    char *oldptr;

    /* OSK doesn't provide enough contiguous memory for sbrk () to work.
     * use ibrk () which gives memory from _inside_ the data area and
     * provide enough stack space to accomodate its use when compiling
     */
#ifndef _OSK
#ifdef COCO
    if ((oldptr = sbrk(size)) == (char *) -1)
#else
    if (!(oldptr = malloc(size)))
#endif
#else
    if ((oldptr = ibrk(size)) == -1)
#endif
    {
        fatal("out of memory");
    }
#ifndef COCO
    /* Hopefully, this will never happen */

    if (oldptr < (char *) lobrk) {
        fprintf(stderr, "           Warning: oldptr < lobrk\n");
    }
#endif

    if (!lobrk)
        lobrk = oldptr;

    hibrk = oldptr + size;

    return oldptr;
}
#endif


#ifdef COCO

/* COCO version of number () */


static int
#ifdef __STDC__
number(int type, register DBLETYPE * np)
#else
number(type, np)
int type;
register DBLETYPE *np;
#endif
{
    double n;
    int exp, esign, digcount = 0;
    char *cp;

    n = 0;

    cp = (char *) &n;

    if (type == DOUBLE) goto fraction;

    /* L6684 */
    if (cc == '0') {
        /* else L6791 */
        long i;
        long *_xo_numptr;

        getch();

        if (cc == '.') {
            getch();
            goto fraction;
        }

        i = 0;                  /* L66a0 */

        if ((cc == 'x') || (cc == 'X')) {       /* else L6733 */
            int c;

            getch();

            while ((c = ishex(cc))) {
                /* L66f6 */
                i = (i << 4) + (c - (c >= 'A' ? 'A' - 10 : '0'));
                getch();
            }
        } else
            /*L6733 */
            while (isoct(cc)) {
                i = (i << 3) + (cc - '0');
                getch();
            }

        /* L673f */
        _xo_numptr = &i;

        if ((cc == 'L') || (cc == 'l')) {       /* else L6757 */
            getch();
            goto retrnlong;
        }

        if ((*(int *) _xo_numptr) == 0) {       /* L6757 */
            *(int *) np = (int) i;
            return INT;
        }

      retrnlong:
        *(long *) np = i;
        return LONG;
    }

    /* here all types still possible; collect in 8 byte integer */
    while (isdigit(cc)) {
        /* L6791 */
        if (addin(&n,cc)) { /* overflow */
            getch();
            return 0;
        }
           /* L67d5 */
        getch();
    }

    /* if no previous dp,
     * there must be either a '.' or 'e'
     * here if it is to be double
     */
    if ((cc == '.') || (cc == 'e') || (cc == 'E')) {    /* else L68bb */
        /* L67b4 */
        if (cc == '.') {
            /* else L67f4 */
            getch();
            /* go to L67e5 */

fraction:
            while (isdigit(cc)) {
                /*L67e5 */
                if (addin(&n,cc)) { /* overflow */
                    getch();
                    return 0;
                }
                getch();
                ++digcount;
            }
        }

        /*L67f4 */
        cp[7] = 184;
        n = _dnorm(&n);

        if ((cc == 'E') || (cc == 'e')) {       /* else L6887 */
            esign=1;
            getch();

            if (cc=='+') getch();
            else if(cc=='-') {
                getch();
                esign=0;
            }
            exp=0;

            while(isdigit(cc)) {
                exp = (exp * 10) + (cc - '0');
                getch();
            }

            if (exp>=40) return 0; /* overflow */

            digcount += esign ? -exp : exp;
        }
        /* L6887 */
        if (digcount<0) {
            digcount= -digcount;
            esign=1;
        } else esign=0;

        *np = scale(n, digcount, esign);
        return DOUBLE;
    }

    /* L68bb */
    if (((cp)[0] != '\0') || ((cp)[1] != '\0') || ((cp)[2] != '\0')) {
        return 0;
    }

    if ((cc == 'l') || (cc == 'L')) {   /* L68d0 */
        char *e0;

        getch();
      longint:
        e0 = &((cp)[3]);
        *(long *) np = *(long *) e0;
        return LONG;
    }

    /*L68fb */
    if (((cp)[3]) || ((cp)[4])) {
        goto longint;
    } else {                    /*L690c */
        /* This seems a round-about-way, but it's the first way I
         * got the code to come out like the source */
        union {
            char *cp;
            int *ip;
        } b0;

        b0.cp = (cp) + 5;
        /*np->i = *(b0.ip); */
        *(int *) np = *(b0.ip);

        return INT;
    }
}

/* The following two functions _may_ be converted to C
 * We'll check later */

/*#asm
scale pshs  u 
 ldd   12,s 
 cmpd  #9 
 ble   L6955 
 leax  4,s 
 pshs  x 
 ldd   16,s 
 pshs  d 
 ldd   16,s 
 pshs  d 
 ldd   #10 
 lbsr  ccdiv 
 addd  #9 
 pshs  d 
 leax  10,s 
 lbsr  _dstack 
 bsr   L6970 
 leas  12,s 
 lbsr  _dmove 
L6955 ldd   14,s 
 pshs  d 
 ldd   14,s 
 pshs  d 
 ldd   #10 
 lbsr  ccmod 
 pshs  d 
 leax  8,s 
 lbsr  _dstack 
 bsr   L6970 
 leas  12,s 
 puls  u,pc 
L6970 pshs  u 
 ldd   12,s 
 beq   L69b6 
 leas  -2,s 
 leax  _atoftbl,y 
 stx   0,s 
 ldd   0,s 
 pshs  d 
 ldd   16,s 
 lslb   
 rola   
 lslb   
 rola   
 lslb   
 rola   
 addd  ,s++ 
 std   0,s 
 ldd   16,s 
 beq   L69a0 
 leax  6,s 
 lbsr  _dstack 
 ldx   8,s 
 lbsr  _dmul 
 bra   L69aa 
L69a0 leax  6,s 
 lbsr  _dstack 
 ldx   8,s 
 lbsr  _ddiv 
L69aa leau  _flacc,y 
 pshs  u 
 lbsr  _dmove 
 leas 2,s
 puls u,pc
L69b6 leax  4,s 
 leau  _flacc,y 
 pshs  u 
 lbsr  _dmove 
 puls  u,pc 
#endasm*/

#else



/* Non-native version */
/* Convert a series of valid numerical digits to a number.
 * At entry, cc holds the first digit of the number.
 */
static int number(int type, DBLETYPE *np)
{
    long i;
    char num[128], *nump = num;
    double n, scale(), normaliz();
    int exp, esign, digcount = 0;
    register char *cp;

    i = 0;
    n = 0;
    cp = (char *)&n;

    if (type == DOUBLE) goto fraction;

    if (cc == '0')
    {
        getch();
        if (cc == '.')
        {
            getch();
            goto fraction;
        }
        if (cc == 'x' || cc == 'X')
        {
            int c;
            getch();
            while ((c = ishex(cc)))
            {
                i = (i << 4) + (c - (c >= 'A' ? 'A' - 10 : '0'));
                getch();
            }
        }
        else
            while (isoct(cc))
            {
                i = (i << 3) + (cc - '0');
                getch();
            }

        if (cc == 'L' || cc == 'l')
        {
            getch();
            goto retlong;
        }

        if ((i & 0xFFFF0000) == 0)
        {
            *(np) = i;
            return INT;
        }

retlong:
        *(np) = i;
        return LONG;
    }

    /* here all types still possible; collect in 8 byte integer */
    while (isdigit(cc))
    {
        *nump = cc;
        nump++;
        getch();
    }

    /* if no previous decimal point, there must be either a '.' or 'e'
     * here if it is to be double.
     */
    if (cc == '.' || cc == 'e' || cc == 'E')
    {
        /* double */
        if (cc == '.')
        {
            *(nump++) = cc;
            getch();
fraction:
            while (isdigit(cc))
            {
                *nump = cc;
                *nump++;
                getch();
                ++digcount;
            }
        }

/*        n = normaliz(&n);*/

        if (cc=='E' || cc=='e')
        {
            esign = 1;
            getch();
            if (cc == '+')
            {
                getch();
            }
            else if(cc == '-')
            {
                getch();
                esign=0;
            }
            exp = 0;

            while (isdigit(cc))
            {
                exp = exp * 10 + (cc - '0');
                getch();
            }

            if (exp >= 40) return 0; /* overflow */

            digcount += esign ? -exp : exp;
        }

        if (digcount < 0)
        {
            digcount = -digcount;
            esign = 1;
        }
        else esign=0;

        /* *(np) = fadjust(scale(n, digcount, esign)); */
        *nump = '\0';
        *np = fadjust(atof(num));

        return DOUBLE;
    }


    /* At this point, the number is either a long or an int */
    /* Make number nul terminated and convert */
    *nump = '\0';
    *np = (double)atoi(num);

    /* Check limits of the number */
    /* Is it larger than a 32 bit signed long? */
    if (*np > 2147483647 || *np < -2147483647)
    {
/*        getch();*/
        return 0;               /* overflow */
    }

    /* Is it explicityly specified as a long or is larger than a 16 bit signed integer? */
    if (cc == 'l' || cc == 'L')
    {
        /* explicitly long */
        getch();

        return LONG;
    }

    if (*np > 65535 || *np < -32768)
    {
        return LONG;
    }
    else
    {
        /*  int  */

        return INT;
    }
}


static int addin(char *n, char c)
{
    register int i,x;
    char ntemp[8];

    if (numshf(n)) return 1;
    for (i = 7; i > 0; --i) ntemp[i] = n[i];
    if (numshf(n)) return 1;
    if (numshf(n)) return 1;
    x = c - '0';
    for (i = 7; i > 0; --i) {
        n[i] = x = (n[i] & 0xFF) + (ntemp[i] & 0xFF) + x;
        x >>= 8;
    }
    return x;
}


static int numshf(char *n)
{
    register int i, x = 0;

    for (i = 7; i > 0; --i) {
        n[i] = x = ((n[i] & 0xFF) << 1) | x;
        x >>= 8;
    }
    return x;
}

#endif

#ifdef COCO
static double
#ifdef __STDC__
scale(double n, int power, int esign)
#else
scale(n, power, esign)
double n;
int power, esign;
#endif
{
    if (power > 9)
        n = scale0(n,(power / 10) + 9,esign);

    return scale0(n, power % 10, esign);
}
#endif

static double
#ifdef __STDC__
scale0(double n, int power, int esign)
#else
scale0(n, power, esign)
double n;
int power, esign;
#endif
{

    if (power) {
        double *p;

        p = (double *)_atoftbl;
        p += power;
        return (esign ? n * *p
                : n / *p);
    }

    return n;
}


static void pstr()
{
    getch();

    if (cc=='\\')
        symval=dobslash();
    else {
        symval=cc;
        getch();
    }

    if (cc!='\'')
        error("unterminated character constant");
    else
        getch();
}

static void qstr()
{
    switch(datstring) {
        case 0:
        case 2:
            if(!strfile) {
                if (!(strfile = fopen(strname, "w+")))
                    fatal("can't open strings file");
            }

            sfile = strfile;
            break;
        case 1:
            /* L6a21 */
            sfile = code;
    }

    stringlen = 0;

    if(datstring != 1) {
        fprintf(sfile,"%c%d",UNIQUE,symval = ++curlabel);
    }

    getch();

    while(cc!='"') {
        /*if (lptr == inpbuf) */
        if (!(lptr - line)) {
            error("unterminated string");
            break;
        }

        if (cc=='\\')
            oc(dobslash());
        else {
            oc(cc);
            getch();
        }
    }

    oc('\0');
    putc('\n',sfile);
    scount = 0;
    getch();
}

static void
#ifdef __STDC__
oc(int c)
#else
oc(c)
int c;
#endif
{
    if (!scount) {
        fprintf(sfile, " fcc \"");
    }

    if ((c < ' ') || (c == '"')) {      /* L6ad6 */
        fprintf(sfile,"\"\n fcb $%x\n", c);
        scount = 0;             /* Reset flag to start new "fcc" line */
    } else {                    /* L6afb */
        putc(c,sfile);

        if ((scount++) >= 75) {
            fprintf(sfile, "\"\n");
            scount = 0;
        }
    }

    ++stringlen;
}

void
#ifdef __STDC__
oz(int n)
#else
oz(n)
int n;
#endif
{
    fprintf(sfile, " rzb %d\n", n);

}

static int
#ifdef __STDC__
dobslash(void)
#else
dobslash()
#endif
{
    register int c,n;

    getch();
    c=cc;
    getch();

    /* If it's a standard escape char, simply return */

    /* case @L6091 */
    switch(c) {
        /* L6b74 */
        case 'n': return NEWLINE;
        case 'l': return LINEFEED;
        case 't': return TAB_CHAR;
        case 'b': return BACKSPACE;
        case 'v': return VTAB_CHAR;
        case 'r': return RET_CHAR;
        case 'f': return FORMFEED;
        case '\n':return ' ';
#ifndef COCO
#ifndef OSK
        case '\r':
#endif
#endif
            return ' ';
    }

    if (c=='x') {
        /* else L6c0c */
        int c1;

        c = n = 0;

        while ((c1 = ishex(cc)) && (n++ < 2)) {
            /* L6bc2 */
            c = (c << 4)+ ((c1 < 'A') ? (c1 - '0') : c1 - ('A' - 10));
            getch();
        }
    }
    else if (c=='d') {
        /*L6c0c */
        c=n=0;

        while(isdigit(cc) && (n++<3)) {
            c = ((c * 10) + cc - '0');
            getch();
        }

    }
    else if (isoct(c)) {
        c -= '0';
        n=0;

        while(isoct(cc) && (n++ < 3)) {
            c = ((c << 3) + (cc - '0'));
            getch();
        }
    }

    return c;
}

static int
#ifdef __STDC__
isoct(char c)
#else
isoct(c)
char c;
#endif
{
    return ((c <= '7') && (c >= '0'));
}

static int
#ifndef __STDC__
ishex(c)
char c;
#else
ishex(char c)
#endif
{
    return ( isdigit(c) || (((c &= 0x5f) >= 'A') && (c <= 'F')))
        ? c : 0;
}
