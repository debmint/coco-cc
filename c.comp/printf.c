/* ******************************************** *
 * This is a custom printf routine for c.pass1  *
 *                                              *
 * It follows part 7 in the linkage             *
 * ******************************************** */

/* $Id: printf.c 14 2008-04-17 02:41:13Z dlb $ */

/* printf cut down for compiler use */

/* DO NOT USE FOR ANYTHING ELSE */
#include "cj.h"
# define isdigit(c)  (chartab[c]==DIGIT)

#ifndef COCO
#ifndef direct
#define direct
#endif
#else

/* We place this here, instead of in comp_13.c in order
 * to maintain the same order for vsect data */
int masks[] = {
    0x0002, 0x0004, 0x0008, 0x0010,
    0x0020, 0x0040, 0x0080, 0x0100,
    0x0200, 0x0400, 0x0800, 0x1000,
    0x2000, 0x4000
};
#endif
union mptr {
    FILE *FI;
    char *CR;
};

static void putstr();
static void putch();
static void putstr1();
static void _dofp();
static void utoa();
static char *xtoa();



static direct union mptr pointer;

static direct int leftflag,        /* D0011 */
 pad,                      /* callflag */
 flag;                       /* D0015 */
static char buf0[10];
static char D0894[10];

static char *itoa(
#ifdef __STDIO__
                         int
#endif
    );


/* We could move the definition for tens here except that it would
 * rearrange the order of the variables from the original source */

extern char chartab[];
#define file    1
#define string  2

#ifndef UNIX
int
printf(fmt,args)
char *fmt;
int args;
{
    pointer.FI = stdout;
    flag = file;
    _dofp(fmt,&args);
}

int
fprintf(f,fmt,args)
FILE *f;
char *fmt;
int args;
{
    pointer.FI = f;
    flag = file;
    _dofp(fmt,&args);
}

int
sprintf(s, fmt, args)
char *s;
char *fmt;
int args;
{
    pointer.CR = s;
    flag = string;
    _dofp(fmt,&args);
    *(pointer.CR) = '\0';
}

static void
_dofp(fmt,args)
register char *fmt;
int *args;
{
    char *_strng;
    char c;
    int _fldwdth;
    int _havedec;
    int _decwdth;
    int v0;

    for(;;){
        while ((c = *(fmt++)) != '%') {
            if (c == '\0')
                return;

            putch(c);
        }

        c = *(fmt++);
        _fldwdth = _decwdth = 0;

        if (c=='-') {
            leftflag=1;
            c = *(fmt++);
        } else {
            leftflag = 0;
        }

        pad = (c=='0') ? '0' : ' ';

        while (chartab[c] == 0x6b) {
            _fldwdth = (_fldwdth * 10) + (c - '0');
            c = *(fmt++);
        }

        /* Get decimal width if any */

        if (c == '.') {
            _havedec = 1;
            c = *(fmt++);

            while (chartab[c] == 0x6b) {
                _decwdth = (_decwdth * 10) + (c - '0');
                c = *(fmt++);
            }
        } else {
            _havedec = 0;
        }

        switch (c) {
            case 'd':          /* L51d9 */
                putstr(itoa(*(args++)), _fldwdth);
                break;
            case 'x':          /* L51ee */
                putstr(xtoa(*(args++)), _fldwdth);
                break;
            case 'c':          /* L5206 */
                putch(*(args++));
                break;
            case 's':          /* L5213 */
                _strng = (char *) (*(args++));

                if (_havedec) { /* else L5256 */
                    _havedec = (int) _strng;    /* use "_havedec" as ptr to base */

                    while (_decwdth--) {        /* L5335 */
                        if (*_strng == '\0') {  /* L5229 */
                            break;
                        }

                        ++_strng;
                    }

                    putstr1((char *) _havedec, ((int) _strng - _havedec), _fldwdth);      /* L5241 */
                } else {
                    putstr(_strng, _fldwdth);        /* L5256 */
                }

                break;
            default:           /* L5266 */
                putch(c);
                break;
        }
    }
}

static char *
itoa(n)
int n;
{
    char *p = buf0;

    if (n<0) {
        if ((n = -n) < 0)
            return (strcpy(buf0, "-32768"));   /* goto L546c */
        *(p++) = '-';
    }

    utoa(p,n);
    return buf0;
}

static int tens[] = {
    10000,
    1000,
    100,
    10
};

static direct int *tensend = &tens[4];

static void
utoa(s, n)
char *s;
int n;
{

    int *ip;
    int flag;
    int count;
    register char *p = s;

    flag=0;

    count=0;
    while(n<0) {
        count++;
        n -= tens[0];
    }
    for(ip=tens;ip!=tensend;++ip) {
        /* L533e */
        while ((n -= *ip) >= 0) {        /* L530f */
            ++count;        /* L5308 */
        }

        n += *ip;


        if (count)
            flag = 1;

        if (flag) {       /*L5328 */
            *(p++) = count + '0';
        }

        count=0;
    }

    *(p++) = n + '0';
    *p='\0';

#ifdef DEVEL                     /* This isn't used... */
    return s;
#endif
}


static char *
xtoa(n)
unsigned int n;
{
    register char *p,*s1;
    int n1;

       s1 = buf0;
    p = D0894;

    do {
        *(p++) = (n1 = n & 0x0f) + ((n1 > 9) ? 'W' : '0');
    } while ((n = ((n >> 4) & 0x0fff)));

    while ((--p) >= D0894) {
        *(s1++) = *p;       /* L5391 */
    }

    *s1 = '\0';
    return buf0;
}


static void
putstr(s,len)
char *s;
register int len;
{
    int c;

    len -= strlen(s);

    if (leftflag == 0) {
        while (len-- > 0) {
            putch(pad);
        }
    }

    while ((c = *(s++)))
        putch(c);

    if (leftflag) {                /* else L546c */
        while (len-- > 0) {
            putch(pad);
        }                       /* go to L546c */
    }
}

static void
putstr1(s, len, len1)
register char *s;
int len, len1;
{
    int padding = len1 - len;

    if(!leftflag)
        while(padding-- > 0)
            putch(pad); /* L5427 */

    while(len--)
        putch(*(s++));

    if(leftflag)
        while(padding-- > 0)
            putch(pad);
}


static void
putch(c)
int c;
{
    if(flag == string)
        *(pointer.CR++) = c;
    else {
        putc(c,pointer.FI);
    }
}
#endif
