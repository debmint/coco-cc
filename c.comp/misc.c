/* ******************************************************************** *
 * misc.c - part 2 of c.comp source                                     $
 *                                                                      $
 * $Id:: misc.c 94 2014-05-04 22:34:25Z David                           $
 * ******************************************************************** */

/*#include <ctype.h>*/
#include "cj.h"

extern char chartab[];

#ifdef __STDC__
static void e_putc(char);
#else
static void e_putc();
#endif

static void pfile();

static void displerr(
#ifdef __STDC__
                        char *, char *
#endif
    );

static void doerr(
#ifdef __STDC__
                     register int _linpos, char *txt, int _line
#endif
    );

static void eputs(
#ifdef __STDC__
                     char *
#endif
    );


/* ******************************************************************** *
 * pushdown () - Push down an outer block declaration                   *
 *                                                                      *
 * ******************************************************************** */

void
#ifdef __STDC__
pushdown(register symnode * sptr)
#else
pushdown(sptr)
register union {
    symnode *sp;
    int *ip;
} *sptr;
#endif
{
    symnode *_origdef;
    symnode *nptr;
#ifndef UNIX
    int __count;                /* used in memcpy routine */
#endif

    if ((nptr = FreeDown)) {
        FreeDown = nptr->snext;
    } else {
        nptr = (symnode *)grab(DOWNSIZE);
    }

    /*_origdef = sptr;  doesn't belong... */

    move((char *)(_origdef = sptr), (char *)nptr, DOWNSIZE);
#ifndef UNIX
    __count = DOWNSIZE / INTSIZE;

    while (__count--) {
        *sptr++ = 0;
    }
#else
    memset(sptr, 0, DOWNSIZE);
#endif

    _origdef->downptr = nptr;
}

/* **************************************************************** *  
 * pullup () - Move "g18" data from base of symnode tree           *
 * Exit Conditions:                                                 *
 *      (1) *FreeDown is base->lblprev                            *
 *      (2) FreeDown now points to base of this tree              *
 * **************************************************************** */

void
#ifdef __STDC__
pullup(register symnode * dstdef)
#else
pullup(dstdef)
register symnode *dstdef;
#endif
{
    symnode *nptr;

    nptr = dstdef->downptr;     /* Point to base of lbldef tree */

    move((char *)nptr, (char *)dstdef, DOWNSIZE);

    nptr->snext = FreeDown;
    FreeDown = nptr;
}

/* Not needed for other systems, as we have lib function memcpy ()
 * (COCO does, too, but it's not used in original code
 */

void
move(_src, _dest, siz)
register char *_src, *_dest;
int siz;
{
    while (siz--) {
        *(_dest++) = *(_src++);
    }
}

static void
#ifdef __STDC__
pfile(void)
#else
pfile()
#endif
{
    displerr("%s : ", filename);
}

void
#ifdef __STDC__
fatal(char *errstr)
#else
fatal(errstr)
char *errstr;
#endif
{
    error(errstr);
    fflush(stderr);	/* because 'tidy()' uses '_exit()' i.e. no flush */
    tidy();			/* get rid of temp files and exit */
}

void
#ifdef __STDC__
multidef(void)
#else
multidef()
#endif
{
    error("multiple definition");
}

void
#ifdef __STDC__
error(char *_str)
#else
error(_str)
char *_str;
#endif
{
    doerr((int)symptr - (int) line, _str, symline);
}

void
#ifdef __STDC__
comperr(expnode * node, char *errstr)
#else
comperr(node, errstr)
expnode *node;
char *errstr;
#endif
{
    char newstr[50];

    strcpy(newstr, "compiler error - ");
    strcat(newstr, errstr);
    terror(node, newstr);
}

void
#ifdef __STDC__
terror(register expnode * node, char *errstr)
#else
terror(node, errstr)
register expnode *node;
char *errstr;
#endif
{
    doerr(((int) node->pnt - (int) line), errstr, node->lno);
}

static void
#ifdef __STDC__
doerr(register int _linpos, char *txt, int _line)
#else
doerr(_linpos, txt, _line)
register int _linpos;
char *txt;
int _line;
#endif
{
    pfile();
    displerr("line %d  ", (char *) _line);      /* to satisfy prototype */
    displerr("****  %s  ****\n", txt);

    if (_line == lineno) {      /* else L04f1 */
        eputs(line);
        goto dopoint;
    } else {
        if ((lineno - 1) == _line) {    /* else _20 (L0528) */
            eputs(lastline);
          dopoint:
            for (; _linpos > 0; --_linpos) {    /* Space over to pos in line */
                e_putc(' ');
            }

            eputs("^");
        }
    }

    if ((++errcount) > 30) {
        fflush(stderr);
        eputs("too many errors - ABORT");
        tidy();
    }

}

static void
#ifdef __STDC__
displerr(char *pmpt, char *val)
#else
displerr(pmpt, val, p3)
char *pmpt;
char *val;
#endif
{

#ifndef COCO
    fprintf(stderr, pmpt, val);
#else
    fprintf(stderr, pmpt, val, p3);
#endif
}

static void
#ifdef __STDC__
eputs(char *str)
#else
eputs(str)
char *str;
#endif
{
    fputs(str, stderr);
    e_putc('\n');
}

static void
#ifdef __STDC__
e_putc(char ch)
#else
e_putc(ch)
char ch;
#endif
{
    putc(ch, stderr);
}

/* **************************************************************** *
 * reltree () - Appends freenode onto bottom left-most cr_Left  *
 *      with all its parameters.                                    *
 *      Wherever in the tree that there is a right in a expnode,    *
 *      the cr_Right is copied to the cr_Left of the "parent".  If  *
 *      there was a cr_Left at this point, is becomes the cr_Left   *
 *      of the just-copied cr_Right. IOW, the cr_Right is inserted  *
 *      into the left side, between the parent and the old cr_Left. *
 *                                                                  *
 *      Examples:                                                   *
 *              old                             new                 *
 *              p1                            -  p1 -               *
 *          /        \                      /         \             *
 *        p1L       p1R                    p1R  <-    p1R           *
 *       /             \                  /  \       /   \          *
 *     p1LL           p1RR              p1L   *    p1L  p1RR <-     *
 *                                     /                            *
 *                                   p1LL                           *
 *                                  /                               *
 *                                CCREF                             *
 *   NOTE (*): The right side here is unchanged from the cr_Right   *
 *   version.  Actually, in the above, the two p1R's are one and    *
 *   the same.                                                      *
 *   As another explanation, reltree recursively calls itself   *
 *   and walks itself to the bottom of the tree, leftward first,    *
 *   does the same for each right branch.  Note in the above, that  *
 *   if there was a p1RL (p1R->L), that the p1L would come under    *
 *   this (actually, it would be the be the cr_Left for the         *
 *   leftmost element of the right branch, similar to the way       *
 *   CCREF is inserted in the left branch.                          *
 * **************************************************************** */

void
#ifdef __STDC__
reltree(register expnode * myref)
#else
reltree(myref)
register expnode *myref;
#endif
{
    if (myref) {
        reltree(myref->left);
        reltree(myref->right);
        release(myref);
    }
}

/* ************************************************************ *
 * release () - Makes the expnode stored in freenode the    *
 *      cr_left for the expnode passed as a parameter,          *
 *      and stores the parameter expnode into freenode          *
 * ************************************************************ */

void
#ifdef __STDC__
release(register expnode * node)
#else
release(node)
register expnode *node;
#endif
{
    if (node) {
        node->left = freenode;
        freenode = node;
    }
}

void
#ifdef __STDC__
nodecopy(expnode * n1, expnode * n2)
#else
nodecopy(n1, n2)
expnode *n1;
expnode *n2;
#endif
{
#ifdef COCO
    move(n1, n2, sizeof(expnode));
#else
    memcpy(n2, n1, sizeof(expnode));
#endif
}

/* ******************************************************** *
 * istype () - Returns TRUE if the definition is a       *
 *              variable definition or a typedef            *
 * ******************************************************** */

int
#ifdef __STDC__
istype(void)
#else
istype()
#endif
{
    if (sym == KEYWORD) {       /* else L062d */
        switch (symval) {
            case INT:
            case CHAR:
            case UNSIGN:
            case SHORT:
            case LONG:
            case STRUCT:
            case UNION:
            case DOUBLE:
            case FLOAT:
                return 1;
            default:
                return 0;
        }
    } else {                    /* L062d */
    if (sym == NAME) {          /* else _67 (L06cd) */
        if (((symnode *) symval)->storage == TYPEDEF) {
            return 1;
        }
    }
    }

    return 0;
}

/* ********************************************************** *
 * issclass () - Returns true if a the word is a       *
 *      storage class specification                           *
 * ********************************************************** */

int
#ifdef __STDC__
issclass(void)
#else
issclass()
#endif
{
    if (sym == KEYWORD) {
        switch (symval) {
            case EXTERN:
            case AUTO:
            case TYPEDEF:
            case REG:
            case STATIC:
            case DIRECT:
                return 1;
        }
    }

    return 0;
}

int
#ifdef __STDC__
decref(int t)
#else
decref(t)
int t;
#endif
{
    return (((t >> 2) & 0xfff0) + (t & 0x0f));
}

/* **************************************************************** *
 * incref () - Increments the pointer depth flagging.           *
 *          Shift the original value less 4 LSB left 2 bits,        *
 *          and adding 0x10                                         *
 * Passed: the original value to shift                              *
 * Returns: The updated value                                       *
 * **************************************************************** */

int
#ifdef __STDC__
incref(int p1)
#else
incref(p1)
int p1;
#endif
{
    return ((((p1 & 0xfff0) << 2) + 0x10) + (p1 & 0x0f));
}

/* **************************************************************** *
 * numeric_op () -                                                  *
 * Passed:  C_* vartyp                                              *
 * **************************************************************** */

int
#ifdef __STDC__
numeric_op(int op)
#else
numeric_op(op)
register int op;
#endif
{
    return (op >= UMOD) && (op <= UGT);
}

dimnode *
#ifdef __STDC__
prevbrkt(register dimnode * dptr)
#else
prevbrkt(dptr)
register dimnode *dptr;
#endif
{
    return dptr ? dptr->dptr : 0;
}

int
#ifdef __STDC__
need(int key)
#else
need(key)
int key;
#endif
{
    static char ptr[] = "x expected";
    register int i;

    if(sym==key) {
        getsym();
        return 0;
    }


    for (i = 0; i < 128; ++i) {
        if (chartab[i] == key)
            break;
    }
    *ptr=i;
    error(ptr);

    return 1;
}

void
#ifdef __STDC__
junk(void)
#else
junk()
#endif
{
    while ((sym != SEMICOL) && (sym != RBRACE) && (sym != -1)) {
        getsym();
    }
}
