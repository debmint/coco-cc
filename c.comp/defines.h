/* ************************************************************* *
 * defines.h - Contains definitions that may make things clearer *
 * ************************************************************* */

/* $Id: defines.h 73 2008-10-03 20:20:29Z dlb $ */

/* COCO variable sizes */

#define CHARSIZ 1
#define INTSIZE 2
#define UNSGNSIZ 2
#define LONGSIZE 4
#define FLOATSIZ 4
#define DOUBLESIZE 8

/* Define substitute for '\n'. We may later allow for output compatible
 * with different systems
 */

#define NEWLINE 0x0d

#define isptr(s) (((s) & 0x30) == 0x10)
#define isary(s) (((s) & 0x30) == 0x20)
#define isftn(s) (((s) & 0x30) == 0x30)

/* Label definition structure */

#ifdef OLDWAY
typedef struct lbl_def {
    int gentyp;
    int vsize;
    /* arrmbrs: array member defs */
    struct brktdef *arrmbrs;
    int lbl_nbr;
    int fnccode;
    /*void *w8; */
    struct memberdef *mbrlist;
    int isftn;
    struct lbl_def **ftop;      /* +14 */
    struct lbl_def *lblprev;    /* +16 */
    struct lbl_def *fnext;      /* +18 */
    char fnam[LBLLEN];          /* +20 */
} LBLDEF;

/* This is a "cmdref" structure.  This is a structure that is defined for the
 * current cmd, and is distinct from the "lbl_def" structure.  It is a 22(?)
 * byte structure, as opposed to the 28-byte "lbldef" structure */

typedef struct cmd_ref {
    /* The first 3 members are often pretty much copies of the first 3
     * members of the corresponding lbl_def struct
     */
    int ft_Ty;
    int varsize;
    struct brktdef *arrdefs;
    int vartyp;                 /* +6 */
    /* cmdval MAY be a pointer to the value of the expression
     * See pass1_07.c around line 375 (after else L4f8b) */
    int cmdval;
    struct cmd_ref *cr_Left;
    struct cmd_ref *cr_Right;
    int _cline;                 /* Current line # *//* +14 */
    char *_lpos;                /* Current position in line *//* +8 */
    int __cr18;
    int ptrdstval;
} CMDREF;


/* Case reference */
struct case_ref {
    struct case_ref *case_nxt;
    int cas02;
    int cas04;
};
#endif

/* Storage for a double.  This structure stores a double in the native
 * format for the host, (native) plus a coco array stored as a set of
 * 4 ints.  This is not a direct representation of a true coco double,
 * due to big-endian storages, and size of int, but in the format we use,
 * we can directly write the 4 int's as "fdb"'s
 */

struct dbltree {
    double native;              /* Host native value          */
    int cocoarr[4];             /* Coco version of the double */
};

/* Struct/union member definition */

#ifdef OLDWAY
struct memberdef {
    struct memberdef *mbrPrev;
    LBLDEF *mmbrlbl;
};

/* Array member definition */

struct brktdef {
    int elcount;                /* count of elements for this member */
    struct brktdef *brPrev;     /* ptr to prev member of this array  */
};
#endif

/* Another structure of some sort found in p1_01.c
 * - We'll call it g18 for now */
/*struct g18 {
    struct g18 *g18Nxt;
    int g18_2;
    int g18_4;
    int g18_6;
    int g18_8;
    int g18_10;
    int g18_12;
    struct g18 * g18_14;
    struct g18 *g18Prev;
};*/

struct lng_something {
    int smptr;
    long sm_val;
};

/* The following is a structure found in p2_01.c */

/*struct nwref {
    int ftyp;
    int nw02;
    union {
        char st[9];
        int wrd;
    } rfdat;
}; */

/*  symbols  */
#define			POINTER			0x10
#define			ARRAY			0x20
#define			FUNCTION		0x30

#define			XTYPE		   0x30
#define			BASICT		   0x0F


/* lbltype->fnccodes */

#define INT      1
#define CHAR     2
#define UNION    3
#define STRUCT   4
#define FLOAT    5
#define DOUBLE   6
#define UNSIGN   7
#define LONG     8
#define LABEL    9
#define USTRUCT	10

#define	STRTAG   8
#define SHORT   10
#define ARG     11
#define EXTDEF  12
#define AUTO    13
#define EXTERN  14
#define STATIC  15
#define REG     16
#define RETURN  18
#define MOS     17
#define IF      19
#define WHILE   20
#define ELSE    21
#define SWITCH  22
#define CASE    23
#define BREAK   24
#define CONTIN  25
#define DO      26
#define DEFAULT 27
#define FOR     28
#define GOTO    29
#define TYPEDEF 30
#define DEFTYPE 31
#define CAST    32
#define DIRECT  33
#define EXTERND 34
#define STATICD 35

/* _chcode values found in D005f */

#define SEMICOL 40
#define LBRACE  41
#define RBRACE  42
#define LBRACK  43
#define RBRACK  44
#define LPAREN 45
#define RPAREN 46
#define COLON 47
#define COMMA 48
#define KEYWORD 51
#define NAME 52
#define CONST 54
#define STRING 55
#define SIZEOF 59
#define INCBEF 60
#define DECBEF 61
#define INCAFT 62
#define DECAFT 63
#define NOT 64
#define AMPER 65
#define STAR 66
#define NEG 67
#define COMPL   68
#define DOT     69
#define ARROW   70
#define DBLAND  71
#define DBLOR   72
#define LCONST  74
#define FCONST  75
#define UMOD    76
#define USHR    77
#define UDIV  78
#define RSUB  79
#define PLUS  80
#define MINUS 81
#define TIMES 82
#define DIV 83
#define MOD   84
#define SHR   85
#define SHL   86
#define AND   87
#define OR    88
#define XOR   89
#define EQ    90
#define NEQ   91
#define LEQ   92
#define LT    93
#define GEQ   94
#define GT    95
#define ULEQ  96
#define ULT   97
#define UGEQ  98
#define UGT   99
#define ASSIGN 120
#define QUERY  100
#define CALL   101
#define BSLASH 102
#define	SHARP  103
#define PRIME  104
#define QUOTE  105
#define LETTER 106
#define DIGIT  107
#define	NEWLN  108
#define SPACE  109
/* regname register codes */
#define STACK  110
#define UREG   111
#define DREG   112
#define XREG   113
#define EXG    115
#define LEA    116
#define LOAD   117
#define YREG   118
/*#define C_RGWRD 111
#define C_X_RGWRD 118*/
#define NODE   119
#define STORE  121
#define PUSH   122
#define LEAX   123
#define JMP    124
#define JMPEQ  125
#define	JMPNE  126
#define LOADIM 127
#define FREG 128                /* Flag Need FLACC */
#define COMPARE 129
#define CNDJMP  130
#define ITOL    131
#define LTOI    132
#define CTOI    133
#define UTOL    134
/* Flags specific to gen () */
#define DBLOP   135
#define LONGOP  136
#define MOVE 137                /* flag call to _fmove, _dmove, or _lmove */
#define STOI    138
#define TEST    139
#define FTOD    140
#define DTOF    141
#define ITOD    142
#define DTOI    143
#define LTOD    144
#define DTOL    145
#define UTOD    146
#define XIND    147
#define YIND    148
#define UIND    149
#define HALVE   150
#define UHALVE  151
#define IDOUBLE 152
#define ASSPLUS 160
#define ASSMIN  161
#define ASSMUL  162
#define ASSDIV  163
#define ASSMOD  164
#define ASSSHR  165
#define ASSSHL  166
#define ASSAND  167
#define ASSOR   168
#define ASSXOR  169

/* dp inicators */
#define DDP 1                   /* on dp */
#define NDP 0                   /* not on dp */

#define	   UNKN	   0
#define	   UNDECL  0


#ifndef TRUE
#define			TRUE			1
#define			FALSE			0
#endif
#ifndef ERR
#define			ERR				0xFFFF
#endif

/* lbldef->gentype codes */

#define G_ERRNO 1
#define G_STRCT 4
#define G_STRCNAM 10
#define G_LSEEK 56

/* pointer depth/parenthesis masks */
#define POINTER 0x10
#define P_BRACE 0x20
#define P_PARENTHESES 0x30
