/* **************************************************************** *
 * cj.h - general header file for cocomp                         *
 *                                                                  *
 * $Id:: cj.h 73 2008-10-03 20:20:29Z dlb                      $ *
 * **************************************************************** */
#include <stdio.h>
#include <string.h>
/*#include "defines.h"*/

/*	Canonicalize our system definitions: __unix__, _OS9, _OSK, _OS9000
 */
#if defined(unix) || defined(__linux__) || defined(__MACH__)
#ifndef __unix__
#define __unix__	1
#endif
#endif

#ifdef COCO
#define void int
#define NUL1
#define NUL2
#define NUL3
#else
#define NUL1 , 0
#define NUL2 , 0, 0
#define NUL3 , 0, 0, 0

#ifdef OSK
#ifndef _OSK
#define _OSK	1
#endif
#endif
#endif

#if defined(_OS9) || defined(_OSK) || defined(_OS9000)
#define MWOS	1
#endif

#ifdef MAIN
#define global
#else
#define global extern
#endif
/*	Floating-point format
 *
 *	The native compiler uses a variant of PDP-11 D_floating format for
 *	'double'. The cross-compiler tries to convert the system's native double
 *	format into as close a form as possible. 'float' isn't really a problem
 *	until we have prototypes.
 */
#ifdef __STDC_IEC_559__
#define IEEE_FLOATS
#endif                          /* __STDC_IEC_559__ */

/*	Endianness definitions.
 *
 *	We want the UNIX-style BYTE_ORDER/foo_ENDIAN definitions as well as the
 *	simple flag-based ones OSK/OS-9000 use today.
 */
#ifdef __unix__
#include <unistd.h>
#include <endian.h>
#if BYTE_ORDER == BIG_ENDIAN
#define _BIG_END 1
#elif BYTE_ORDER == LITTLE_ENDIAN
#define _LIL_END 1
#elif BYTE_ORDER == PDP_ENDIAN
#define _PDP_END 1
#else
#error "unknown endianness, can't continue"
#endif
#else                           /* not UNIX */
#define LITTLE_ENDIAN	1234
#define BIG_ENDIAN	4321
#define PDP_ENDIAN	3412
#if defined(MWOS)
#if defined(_OS9) || defined(_OSK) || defined(_BIG_END) /* predefined bigendian */
#define BYTE_ORDER	BIG_ENDIAN
#ifndef _BIG_END
#define _BIG_END 1
#endif
#elif defined(_LIL_END)
#define BYTE_ORDER	LITTLE_ENDIAN
#endif                          /* done with MWOS */
#elif defined(_WIN32)
#define BYTE_ORDER	LITTLE_ENDIAN
#define _LIL_END 1              /* done with Win32 */
#else
/*#warning "unknown system, arbitrarily assuming big-endian"*/
#define BYTE_ORDER	BIG_ENDIAN
#define _BIG_END 1
#endif
#endif                          /* not UNIX */
/* Maximum Label length this needs to be done before "defines.h" is called  */
/* Note: If LBLLEN is changed, "grep '\.8' *.c" ( or one of the values
 * below to find printf formats                                             */
#ifdef COCO
#define LBLLEN 8
#else
#define LBLLEN 12
#endif


#ifdef _OS9
#define INTTYPE   int
#define LONGTYPE  long
#define FLOATYPE float
#define DBLETYPE  double
#endif

#ifdef _OSK
#define INTTYPE   short
#define LONGTYPE  long
#define FLOATYPE float
#define DBLETYPE  double
#endif

#ifdef UNIX
#include <fcntl.h>
#define direct
#define INTTYPE   short
#define LONGTYPE  long
#define FLOATYPE float
#define DBLETYPE  double
#endif

#define FNAMESIZE   30          /* file name size */

#define LINESIZE    256
#define MODNAMSIZE  15          /* module name */

#define NAMESIZE 8              /* identifiers for assembler */
#define		UNIQUE		'_'     /* for local labels etc. */
#define NEWLINE 13

typedef struct structstruct {
    struct structstruct *strnext;       /* next element in structure */
    struct symstr *element;     /* ptr to mos entry */
} elem;

typedef struct dimstr {
    short dim;                  /* size of this dimension */
    struct dimstr *dptr;        /* ptr to next dimension */
} dimnode;

typedef struct downsym {
    short type;                 /* type of entry */
    int size;                   /* size of entry */
    dimnode *dimptr;            /* ptr to list of dimensions */
    int offset;                 /* general purpose */
    int storage;                /* storage class */
    union {
        elem *elems;            /* chain of structure members */
        int labflg;             /* label flags */
    } x;
    short blklev;               /* block level of declaration */
    struct symstr *downptr,     /* ptr to 'hidden' outer declaration */
    *snext;                     /* ptr to entry in various lists */
} pshnode;

#define DOWNSIZE	sizeof(pshnode)

typedef struct symstr {
    short type;                 /* type of entry */
    int size;                   /* size of entry */
    dimnode *dimptr;            /* ptr to list of dimensions */
    int offset;                 /* general purpose */
    int storage;                /* storage class */
    union {
        elem *elems;            /* chain of structure members */
        int labflg;             /* label flag */
    } x;
    short blocklevel;           /* block level of declaration */
    struct symstr *downptr,     /* ptr to 'hidden' outer declaration */
    *snext,                     /* ptr to entry in various lists */
    *hlink;                     /* ptr to next symbol in hash chain */
#ifdef DEVEL
    char sname[NAMESIZE];       /* name entry */
#else
    char sname[NAMESIZE+1];     /* name entry */
#endif
} symnode;

#define SYMSIZE	   sizeof(symnode)

typedef struct expstr {
    short type;                 /* type of node */
    int size;                   /* size of basic type */
    dimnode *dimptr;            /* ptr to list of dimensions */
    short op;                   /* operator */
    union {
        int num;                /* numeric value */
        symnode *sp;            /* symbol pointer */
        LONGTYPE *lp;           /* long constant ptr */
        FLOATYPE *fp;           /* float constant ptr */
        DBLETYPE *dp;           /* double constant ptr */
    } val;                      /* value or symbol ptr */
    struct expstr *left,        /* left node ptr */
    *right;                     /* right node ptr */
    short lno;                  /* line # of token causing this node */
    char *pnt;                  /* ptr to line position of token */
    short sux;                  /* pseudo Sethi-Ullman number */
    short modifier;             /* added to NAME references */
} expnode;

#define NODESIZE	sizeof(expnode)

typedef struct initstruct {
    struct initstruct *initnext;        /* next initializer */
    expnode *initp;             /* ptr to initializer expression */
    symnode *initname;          /* ptr to dude being initialized */
} initnode;

typedef struct casestct {
    struct casestct *clink;     /* next case in case list */
    int cval;                   /* case value */
    short clab;                 /* case label */
} casnode;

/* Storage for a double.  This structure stores a double in the native
 * format for the host, (native) plus a coco array stored as a set of
 * 4 ints.  This is not a direct representation of a true coco double,
 * due to big-endian storages, and size of int, but in the format we use,
 * we can directly write the 4 int's as "fdb"'s
 */

struct dbltree {
    double native;              /* Host native value          */
    int cocoarr[4];             /* Coco version of the double */
};

/*  label state structure  */
typedef struct {
    int      labnum,
             labsp;
    expnode *labdreg,
            *labxreg;
} labstruc;

/* object sizes - char is by definition 1 */
#define CHARSIZ 1
#define INTSIZE 2
#define UNSGNSIZ 2
#define LONGSIZE 4
#define FLOATSIZE   4
#define DOUBLESIZE  8
#define POINTSIZE   2

/*  symbols  */
#define POINTER			0x10    /* type is "pointer to [basic type]" */
#define ARRAY			0x20    /* type is "array of [basic type]" */
#define FUNCTION		0x30    /* type is "function returning [basic type]" */

#define XTYPE		   0x30 /* bit mask for type modifiers */
#define BASICT		   0x0F /* bit mask for just the basic type */

/* basic types */
#define INT      1              /* most convenient integer type */
#define CHAR     2
#define UNION    3
#define STRUCT   4
#define FLOAT    5
#define DOUBLE   6
#define UNSIGN   7
#define LONG     8
#define LABEL    9
#define USTRUCT	10

#define	STRTAG   8
#define SHORT   10
#define ARG     11
#define EXTDEF  12
#define AUTO    13
#define EXTERN  14
#define STATIC  15
#define REG     16
#define RETURN  18
#define MOS     17
#define IF      19
#define WHILE   20
#define ELSE    21
#define SWITCH  22
#define CASE    23
#define BREAK   24
#define CONTIN  25
#define DO      26
#define DEFAULT 27
#define FOR     28
#define GOTO    29
#define TYPEDEF 30
#define DEFTYPE 31
#define CAST    32
#define DIRECT  33              /* direct */
#define EXTERND 34              /* extern direct */
#define STATICD 35              /* static direct */

#define SEMICOL 40
#define LBRACE  41
#define RBRACE  42
#define LBRACK  43
#define RBRACK  44
#define LPAREN  45
#define RPAREN  46
#define COLON   47
#define COMMA   48
#define KEYWORD 51
#define NAME    52
#define CONST   54
#define STRING  55
#define SIZEOF  59
#define INCBEF  60
#define DECBEF  61
#define INCAFT  62
#define DECAFT  63
#define NOT     64
#define AMPER   65
#define STAR    66
#define NEG     67
#define COMPL   68
#define DOT     69
#define ARROW   70
#define DBLAND  71
#define DBLOR   72
#define LCONST  74
#define FCONST  75
#define UMOD    76
#define USHR    77
#define UDIV    78
#define RSUB    79
#define PLUS    80
#define MINUS   81
#define TIMES   82
#define DIV     83
#define MOD     84
#define SHR     85
#define SHL     86
#define AND     87
#define OR      88
#define XOR     89
#define EQ      90
#define NEQ     91
#define LEQ     92
#define LT      93
#define GEQ     94
#define GT      95
#define ULEQ    96
#define ULT     97
#define UGEQ    98
#define UGT     99
#define ASSIGN 120
#define QUERY  100
#define CALL   101
#define BSLASH 102
#define	SHARP  103
#define PRIME  104
#define QUOTE  105
#define LETTER 106
#define DIGIT  107
#define	NEWLN  108
#define SPACE  109

/* code generator symbols */
#define STACK  110
#define UREG   111
#define DREG   112
#define XREG   113
#define EXG    115
#define LEA    116
#define LOAD   117
#define YREG   118
#define NODE   119
#define STORE  121
#define PUSH   122
#define LEAX   123
#define JMP    124
#define JMPEQ  125
#define	JMPNE  126
#define LOADIM 127
#define FREG   128
#define COMPARE 129
#define CNDJMP  130
#define ITOL    131
#define LTOI    132
#define CTOI    133
#define UTOL    134
/* Flags specific to gen () */
#define DBLOP   135
#define LONGOP  136
#define MOVE    137
#define STOI    138
#define TEST    139
#define FTOD    140
#define DTOF    141
#define ITOD    142
#define DTOI    143
#define LTOD    144
#define DTOL    145
#define UTOD    146
#define XIND    147
#define YIND    148
#define UIND    149
#define HALVE   150
#define UHALVE  151
#define IDOUBLE 152
#define ASSPLUS 160
#define ASSMIN  161
#define ASSMUL  162
#define ASSDIV  163
#define ASSMOD  164
#define ASSSHR  165
#define ASSSHL  166
#define ASSAND  167
#define ASSOR   168
#define ASSXOR  169

/* indirection masks */
#define	  INDIRECT 0x8000
#define	  NOTIND 0x7FFF

/* label tests */
#define		   DEFINED 1
#define		   GONETO  2

/* dp indicators */
#define DDP 1                   /* on dp */
#define NDP 0                   /* not on dp */

#define	   UNKN	   0
#define	   UNDECL  0


#ifndef TRUE
#define		TRUE			1
#define			FALSE			0
#endif
#define			ERR				0xFFFF

#define			TAB_CHAR		9
#define			LINEFEED		10
#define			FORMFEED		12
#define			RET_CHAR		13
#define			VTAB_CHAR		11
#define			BACKSPACE		8

#define btype(t) ((t) & BASICT)
#define isptr(t) (((t) & XTYPE) == POINTER)
#define isary(t) (((t) & XTYPE) == ARRAY)
#define isftn(t) (((t) & XTYPE) == FUNCTION)
#define		   modtype(t,b)	   (((t) & (~BASICT)) + b)

#define		   getlabel(a)	   (++curlabel)

#define islong(t)   (t->type == LONG)
#define isfloat(t)   (t->type == FLOAT || t->type == DOUBLE)

global direct FILE  *in,        /* input file pointer */
                    *code;      /* code file pointer */

global direct int    lineno,    /* current line number */
                      errcount,
                      curlabel; /* next internal label no. */
global direct expnode *freenode;/* sp ??? */
global direct int     sp,       /* current stack pointer offset */
                      stlev,		/* stack reservation level */
                      callflag,	/* function call flag */
                      sflag, 		/* suppress stack checking code */
                      pflag,		/* if set generate profile calls */
                      ModName,  /* nocode?? */
                      maxpush,  /* maximum push level */
                       D001d,   /* lastst?? */
                       symline, /* breaklb  */
                       mosflg;  /* cntlab   *//* D0021 */
global direct symnode *FreeDown; /* List of free push-down entries */
global direct casnode *freecase; /* list of spare case nodes *//* Could be static in stats.c */
global direct symnode *arglist,    /* List of arguments */
                      *labelist,   /* List of labels */
                      *vlist;      /* list of declared variables */
global direct int      FuncGentyp, /* symline  */
                       lastst;     /* maxpush  */
global char            filename[FNAMESIZE];  /* current filename buffer */
global direct int      blklev;
global direct labstruc *D0033;
global direct int      D0035,
                       swflag;
global direct casnode *caseptr,   /* D0039 *//* Could be static in stats.c */
                      *lastcase;  /* D003b *//* Could be static in stats.c */
global direct int      scount,
                       sym,      /* current lexical token */
                       symval;   /* and its associated value */
global direct char    *symptr,  /* start of current symbol */
                       cc,      /* current character */
                      *lobrk,   /* lower store bound  */
                      *hibrk,   /* upper store bound  */
                      *lptr;    /* input line pointer */
global direct FILE    *strfile; /* strings output file */
global direct long     D004e;   /* not used ? */
global direct int      D0052;   /* 4 bytes - maybe something else */
global direct int      D0054;
global direct int      datstring,
                       lineflg,     /* Dummy for filler */
                       datflag,
                       stklab,
                       D005e,
                       stringlen;
global direct symnode *freesym;
global direct FILE    *sfile;
global direct int      shiftflag;

global symnode        *hashtab[128],  /* main hash table */
                      *mostab[128];   /* structure hash table */
global char            line[LINESIZE],		/* input line buffer */
                       lastline[LINESIZE]; /* saved last line */

extern char strname[];          /* string file name */


extern symnode sdummy;
expnode *newnode();
void fatal(),on();
#include "proto.h"
