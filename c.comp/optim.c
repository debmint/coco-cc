/* **************************************************************** *
 * ccomp_04.c                                                       *
 *                                                                  *
 * comes from p1_03.c                                               *
 * $Id:: comp_04.c 73 2008-10-03 20:20:29Z dlb                    $ *
 * **************************************************************** */

#include "cj.h"

static expnode *fold(
#ifdef __STDC__
        register expnode * regptr
#endif
        );

static int isleaf(
#ifdef __STDC__
        register expnode * regptr
#endif
        );

static expnode *chtype(
#ifdef __STDC__
        expnode *
#endif
        );

static void cktsttyp(
#ifdef __STDC__
        expnode *
#endif
        );
static int usual(
#ifdef __STDC__
        register expnode * c_ref
#endif
        );

static int tymatch(
#ifdef __STDC__
        register expnode *, expnode *
#endif
        );

static expnode *mult_const(
#ifdef __STDC__
        int, int, expnode *, expnode *
#endif
        );

static void ckif_lvalue(
#ifdef __STDC__
        register expnode *, int
#endif
        );

static int membrsize(
#ifdef __STDC__
        expnode *, int *, int *
#endif
        );


expnode *
#ifdef __STDC__
optim(register expnode * tree)
#else
optim(tree)
register expnode *tree;
#endif
{
    expnode *lhs;
    expnode *rhs;
#ifdef COCO
    int v6;                     /* unused - can delete if not comparing code */
#endif
    int _varty;
    expnode *tptr;
#ifdef DEVEL
    int v0;                     /* unused - can delete if not comparing code */
#endif
    if (tree) {
        /* else L1204 (return) */
        tree->left=optim(tree->left);
        tree->right=optim(tree->right);

        tree = chtype(tree);
        if (isleaf(tree)) {
            goto L1202;         /* Simply return tree */
        }
        /*goto L1202; */

        /* Calculate and combine if they are constants */

        tree = fold(tree);
        /* L115b */
        lhs = tree->left;
        rhs = tree->right;

        if (((_varty = tree->op) == QUERY) && (lhs->op == CONST)) {     /* else L11b9 */
            if (lhs->val.num) { /* else L1192 */
                tptr = rhs->left;
                reltree(rhs->right);
            } else {
                tptr = rhs->right;      /* L1192 */
                reltree(rhs->left);
            }

            /* CurntCmd becomes left for rhs
             * rhs becomes left for cmd_Left
             * _cmd_Left becomes left for tree
             */

            release(rhs);
            release(lhs);
            release(tree);
        } else {                /* Not "? :" */
            /* L11b9 */ if ((_varty != STAR) || (lhs->op != AMPER)) {
                if ((_varty != AMPER) || (lhs->op != STAR)) {
                    /* not "**", "*&", "&*" or "&&" */
                    return tree;
                }
            }

            /* lhs inherit ft_Ty & varsize from tree */

            tptr = lhs->left;       /* L11dd */

            tptr->type = tree->type;
            tptr->size = tree->size;
            release(tree);
            release(lhs);
        }

        tree = tptr;            /* L11f9 */
    }

L1202:
    return tree;
}

/* ******************************************************************** *
 * fold () - Calculates the value of constant expressions if            *
 *              whole expression results in a constant.                 *
 *              Reconstructs expnode tree when appropriate.             *
 * Returns: Updated expnode of focus, with values updated if complete   *
 *          expression results in a constant                            *
 *          In Most cases, the expnode's left and right ref ptrs are    *
 *          nulled  out and a chain with the Left and Right ref's,      *
 *          with the former contents of freenode being the left ref     *
 *          for leftref and freenode pointing to the right-ref.         *
 * ******************************************************************** */

static expnode *
#ifdef __STDC__
fold(register expnode * tree)
#else
fold(tree)
    register expnode *tree;
#endif
{
    expnode *lhs;
    expnode *rhs;
    expnode *ptr;
    int op;
#ifdef COCO
    int v4;                     /* Not used - may delete if no more comparing with orig. */
#endif
    int opl;                    /* TRUE if  lhs->vartyp == CONST */
    int opr;                    /* TRUE if rhs->vartyp == CONST */

    if (!tree) {
        return 0;
    }

    lhs = tree->left;           /* L121f */
    rhs = tree->right;

    if ((numeric_op(op = tree->op)) || (op == DBLAND) || (op == DBLOR)) {       /* else L145d */
        /* if both are constant ints, merge and return */

        if ((opl = (lhs->op == CONST)) &        /* L1246 */
                (opr = (rhs->op == CONST))) {
            goto mrg_n_exit;
        }

        if (opl) {              /* I.E. <constant int> ... <some-other-type> */
            /* L127b */
            /* else L12bb */
            switch (op) {
                case PLUS:     /* L1283  _44 */
                case TIMES:
                case AND:
                case OR:
                case XOR:

                    /* The above are transmutable.
                     * Swap left for right so we have constant last */

                    ptr = lhs;
                    tree->left = lhs = rhs;
                    tree->right = rhs = ptr;
                    opr = 1;
                    break;
            }
        }

        switch (op) {
            case OR:           /* L12c0 (_51) */
            case XOR:

                /* constant is 0 */

                if ((opr) && !(rhs->val.num)) {
                    goto swap_n_retrn;
                }

                break;          /*goto swap_n_retrn; */
            case PLUS:         /* L12d1 (_60) */

                /* if both are non-const, return doing nothing */

                if (opr) {
                    /* + 0 is a no-op.. Update crefs and return */

                    if (rhs->val.num) {
                        switch (lhs->op) {
                            case AMPER:        /* L12e6 */
                                (lhs->left)->modifier += rhs->val.num;
swap_n_retrn:
                                ptr = lhs;
                                release(tree);
                                release(rhs);
                                tree = ptr;
                                return tree;
                            case PLUS: /* L1317 */
                                if ((lhs->right)->op == CONST) {
                                    ((lhs->right)->val.num) +=
                                        rhs->val.num;
                                    goto swap_n_retrn;
                                }

                                return tree;
                            default:   /* L1522 */
                                return tree;
                        }
                    } else {
                        goto swap_n_retrn;
                    }
                }

                break;
            case MINUS:        /* L1347 ( _71 ) */

                /* If right is constant, negate right, switch to ADD
                 * call self to add
                 */
                if (opr) {
                    rhs->val.num = -(rhs->val.num);
                    tree->op = PLUS;
                    return (fold(tree));
                }

                if (opl) {      /* L1366 */
                    if (!lhs->val.num) {        /* 0 - <something> */
                        tree->op = NEG;
                        tree->left = rhs;
                        tree->right = 0;
                        release(lhs);
                    }
                }

                break;          /*return tree; */
            case AND:          /* L1386 */
                if ((opr) && !(rhs->val.num)) {
                    goto retrn_0;
                    /*return 0; */
                }
                /*else
                  {
                  return tree;
                  } */
                break;

            case TIMES:        /* _97  (L1395) */
                if (!opr) {
                    return tree;
                }

                switch (rhs->val.num) {
                    case 0:    /* L13a1 ( _106 ) */
                        goto retrn_0;
                    default:   /* L13a6 */
                        if ((lhs->op != TIMES) ||
                                ((lhs->right)->op != CONST)
                           ) {
                            return tree;
                        }

                        (lhs->right)->val.num *= rhs->val.num;
                    case 1:    /* "* 1" is a no-op *//* L13ee ( _113 ) */
                        goto swap_n_retrn;
                }
            case DIV:          /* L13e0 ( _104 ) */
                if (opr && (rhs->val.num == 1)) {
                    goto swap_n_retrn;
                }

                if (opl && !(lhs->val.num)) {   /* L13f3 */
retrn_0:
                    tree->op = CONST;
                    tree->val.num = 0;  /* L140c */
                    tree->left = tree->right = 0;

                    /* Curexpnode becomes left for lhs */

                    release(lhs);
                    release(rhs);
                    /*return tree; */
                }
        }                       /* end switch (ope) */

        /*return tree; *//* L1522 */
    }                           /* end if math function */
    else {

        /* L145d */
        switch (op) {           /* L150d ( _124 ) */
            case NOT:          /* L1462 ( _125 ) */
            case COMPL:
            case NEG:
                if (lhs->op == CONST) { /* else L1492 */
mrg_n_exit:
                    tree->op = CONST;
                    tree->sux = 0;

                    /* calculate the value of the cmdvals */
#ifdef COCO
                    tree->val.num =
                        constop(op, lhs->val.num, rhs->val.num);
#else
                    tree->val.num = constop(op, lhs->val.num,
                            ((rhs ==
                              0) ? 0 : rhs->val.num));
#endif
                    /* bra L140c */
                    /* eliminate left & right crefs of tree
                     * left and right values combined into tree
                     */

                    tree->left = tree->right = 0;
                    release(lhs);
                    release(rhs);
                } else {        /* L1492 */
                    if (lhs->op == LCONST) {    /* else L14e1 */
                        long *lngptr;

                        if (!(lngptr = lhs->val.lp)) {  /* else L14b2 */
                            lngptr = (long *)grab(LONGSIZE);
                        }

                        switch (op) {
                            case NEG:  /* L14b6 */
                                *lngptr = -(*lngptr);
                                break;
                            case COMPL:        /* L14c1 */
                                *lngptr = ~(*lngptr);
                                break;
                        }

                        lhs->val.lp = (long *)lngptr;     /* go to L1501 */

                        release(tree);      /* L1501 */
                        tree = lhs;
                    } else {
                        if (lhs->op == FCONST) {        /* L14e1 */
                            double *dptr;

                            if ((dptr = (double *)(lhs->val.dp))) {
                                *dptr = -(*dptr);
                            }

                            release(tree);  /* L1501 */
                            tree = lhs;
                        }
                    }
                }
        }
    }

    return tree;
}

static int
#ifdef __STDC__
isleaf(register expnode * node)
#else
isleaf(node)
    register expnode *node;
#endif
{
    if (node) {
        switch (node->op) {
            case YREG:         /* L153a */
            case UREG:
            case CONST:
            case LCONST:
            case FCONST:
            case NAME:
            case STRING:
                return 1;
        }
    }

    return 0;
}

void
#ifdef __STDC__
diverr(void)
#else
diverr()
#endif
{
    error("divide by zero");
}


/* mammoth tree type checker.
 * checks for correct descendant nodes,
 * rewrites tree and converts types where neccessary,
 * fills in various node fields according to types and operators.
 * tree should not present any problems to the translator after this.
 */
static expnode *
#ifdef __STDC__
chtype(expnode * node)
#else
chtype(node)
    expnode *node;
#endif
{
    register expnode *lhs,*rhs;
    symnode *sptr;
    int op, t, size, xtype;
    dimnode *dptr;
    int tl, tr, mosar;

    lhs = node->left;
    rhs = node->right;

	/* defaults */
    xtype = size = INTSIZE;
    dptr = 0;
    t = INT;

    switch (op = node->op) {
        case NAME:
            /* L15bb */
            sptr = node->val.sp;

            if (sptr->storage == TYPEDEF) {
                /* else L15eb */
                terror(node,"typedef - not a variable");
                makedummy(node);
                break;
            }

            t = sptr->type;
            /* L15eb */
            dptr = sptr->dimptr;
            if (btype(t) == USTRUCT) {
                /* else L162c */
                register symnode *p = (symnode *) sptr->size;
				t = sptr->type = modtype(t,p->type);
                sizeup(sptr, dptr, p->size);
            }

            size = sptr->size;

            if ((isary(t)) || (isftn(t))) {     /*else L16ae */
                lhs = node;
                lhs->size = size;

                node = newnode(AMPER,lhs,0,0,node->lno,node->pnt);

                if (isary(t)) {
                    /* else L1699 */
                    dptr = prevbrkt(dptr);
                    t = incref(lhs->type = decref(t));
                } else {
                    t = incref(lhs->type = t);
                }

                lhs->sux = 1;
            } else {            /* Not array or parentheses */
                switch (tl = sptr->storage) {   /* L16ae */
                    case YREG: /* L16b9 */
                    case UREG:
                        node->op = tl;
                        node->val.num = 0;
                        break;
                }
            }

            xtype = 1;       /* L16d5 */
            break;
        case CONST:            /* L16da */
            xtype = 0;
            break;
        case LCONST:           /* _182  L16e1 */
            t = LONG;
            xtype = 1;
            size = LONGSIZE;
            break;
        case FCONST:           /* L16f1 */
            t = DOUBLE;
            xtype = 1;
            size = DOUBLESIZE;
            break;
        case STRING:           /* L1701 */
            t = RETURN;
            size = CHARSIZ;
            break;
        case CAST:             /* L170c */
            chkdecl(lhs);

            /* Result of a cast cannot be array or function */

            if ((isftn(t = node->type)) || (isary(t))) {
                terror(node, "cannot cast");
                t = incref(t);
            }

            if (isptr(t)) {
                cvt(lhs, INT);
            } else {
                t = cvt(lhs, t);
            }

            dptr = node->dimptr;        /* L176d */
            size = node->size;
            release(node);
            node = lhs;
            break;
        case AMPER:            /* L178b */
            ckif_lvalue(lhs, 1);

            if ((lhs->op == YREG) || (lhs->op == UREG)) {
                terror(node, "can't take address");
                makedummy(lhs);
            }

            xtype = lhs->sux;
            size = lhs->size;
            t = incref(lhs->type);
            dptr = lhs->dimptr;
            break;
        case STAR:             /* L17d9 */
            dptr = lhs->dimptr;

            if (isptr(t = lhs->type)) { /* else L181d */
                t = decref(t);

                if (isary(t)) { /* else L183f */
                    t = incref(decref(t));
                    release(node);
                    node = lhs;
                }
            } else {
                terror(lhs, "pointer required");
                makedummy(lhs);
                lhs->type = 17;
                t = INT;
                dptr = 0;
            }

            xtype = lhs->sux;        /* L183f */
            size = lhs->size;
            break;
        case DOT:              /* L184b */
            size = membrsize(rhs, &t, &mosar);
            dptr = rhs->dimptr;
            ckif_lvalue(lhs, 1);

            if (!(rhs->val.num) && !(mosar)) {
                release(rhs);
                release(node);
                node = lhs;
                xtype = node->sux;   /* go to L1902 */
            } else {
                xtype = lhs->sux;    /* L189f */

                if (lhs->op == STAR) {  /* else L18c4 */
                    /* ERROR ?? should 2 eptr be sptr?? */
                    sptr = (symnode *)lhs->left;
                    release(lhs);
                    node->left = lhs = (expnode *)sptr;
                } else {
                    tl = lhs->type;     /* L18c4 */
                    lhs = node->left = newnode(AMPER, lhs, 0, 0,
                            lhs->lno, lhs->pnt);
                    lhs->type = incref(tl);
                }

                lhs->sux = xtype;
                goto strcommon;
            }

            break;
        case ARROW:            /* L1905 */
            size = membrsize(rhs, &t, &mosar);
            dptr = rhs->dimptr;

            if (((tl = lhs->type) != INT)) {    /* else L1959 */
                if (isptr(tl) == 0) {   /* Trying to match code */
                    terror(lhs, "pointer or integer required");
                    lhs->op = CONST;
                    lhs->val.num = 0;
                    lhs->sux = 0;
                }
            }

            xtype = lhs->sux;
strcommon:
            node->op = PLUS;
            node = fold(node);

            if (!mosar) {       /* else L19c0 */
                node->type = incref(t);
                node->sux = xtype;
                node->size = size;
                node = newnode(STAR, node, 0, 0, node->lno, node->pnt);
            }

            break;
        case CALL:             /* L19c3 */
            t = lhs->type;

            if ((lhs->op == AMPER) && (isftn(decref(t)))) {     /* else L19ff */
                sptr = (symnode *)(node->left = lhs->left);
                release(lhs);
                t = decref(sptr->type);
                /* go to L1a63 */
            } else {
                if (isftn(t)) {
                    t = decref(t);      /* go to L1a63 */
                } else {
                    if (!t) {   /* else L1a51 */
                        sptr = lhs->val.sp;
                        sptr->type = 0x30 + INT;
                        sptr->size = 2;
                        sptr->dimptr = 0;
                        sptr->storage = EXTERN;
                        sptr->blocklevel = 0;

                        move((char *)sptr, (char *)lhs, 6);
                        t = INT;
                    } else {
                        terror(lhs, "not a function");
                        t &= 0x0f;
                    }
                }
            }

            dptr = lhs->dimptr; /* L1a63 */
            size = lhs->size;   /* Fall through to next case */
        case ARG:              /* L1a6d */
            chkdecl(lhs);
            ckstrct(lhs);

            switch (lhs->type) {
                case CHAR:     /* L1a7f */
                    cvt(lhs, INT);
                    break;
                case FLOAT:    /* L1a84 */
                    cvt(lhs, DOUBLE);
                    break;
            }

            break;
        case COMMA:            /* L1a9f */
            size = rhs->size;
            t = rhs->type;
            dptr = rhs->dimptr;
            break;
        case NOT:              /* L1ab5 */
            if (!(isptr(lhs->type))) {
                usual(lhs);
            }

            t = INT;
            break;
        case NEG:              /* L1acd */
            t = usual(lhs);
            break;
        case COMPL:            /* L1ad7 */
            t = usual(lhs);

            if (t == DOUBLE) {  /* else L1bc8 */
                notintegral(lhs);
                makedummy(lhs);
            }

            break;
        case INCBEF:           /* _269  (L1af4) */
        case INCAFT:
        case DECBEF:
        case DECAFT:
            ckif_lvalue(lhs, 0);
            size = lhs->size;

            if ((t = lhs->type) & 0x30) {       /* else L1b25 */
                node->val.num = getsize(decref(t),
                        lhs->size, lhs->dimptr);
            } else {
                node->val.num = 1;      /* L1b25 */
            }

            xtype = lhs->sux;

            if (lhs->op == STAR) {      /* else L1cac */
                ++xtype;
            }

            break;
        case DBLAND:           /* _278  (L1b44) */
        case DBLOR:            /* _278 */
            if (!isptr(lhs->type)) {
                usual(lhs);
            }

            if (!(isptr(rhs->type))) {  /* L1b56 */
                usual(rhs);
            }

            break;
        case DIV:              /* L2006 */
        case TIMES:
            t = tymatch(lhs, rhs);
            /*node->val.num = 0; */
            break;
        case AND:              /* _292  (L1b6e) */
        case OR:
        case XOR:
        case MOD:
            if ((t = tymatch(lhs, rhs)) == DOUBLE) {
                goto dblerr;    /* error - both must be integral */
            }

            break;
        case SHL:              /* L1b8a */
        case SHR:
            if (((t = usual(lhs)) == DOUBLE) || (usual(rhs) == DOUBLE)) {       /* else L1bcb */
dblerr:
                terror(node, "both must be integral");
                makedummy(node);
                break;
            }

            cvt(rhs, INT);
            break;
        case LT:               /* L1bdb */
        case GT:
        case LEQ:
        case GEQ:
            if (((isptr(lhs->type)) && (isptr(rhs->type))
                ) || (tymatch(lhs, rhs) == UNSIGN)) {
                node->op = op + 4;       /* L1c04 */
            }


            break;
        case EQ:               /* L1c11 */
        case NEQ:
            if (isptr(lhs->type)) {     /* else L1c4a */
                if ((isptr(rhs->type) == 0)) {  /* else L1c56 */
                    usual(rhs);     /* L1c2f + 1 line */
                    cvt(rhs, INT);      /* go to L1c54 */
                }
            } else {
                tymatch(lhs, rhs);   /* L1c4a */
            }

            break;              /* L1c56 */
        case PLUS:             /* L1c59 */
            if (isptr(rhs->type)) {     /* else L1c78 */
                {
                    expnode *__tmp = lhs;

                    lhs = rhs;
                    rhs = __tmp;
                }

                node->left = lhs;
            }

            size = lhs->size;           /* L1c78 */

            if (isptr(t = lhs->type)) {
                dptr = lhs->dimptr;
                goto dofix;
            } else {
                t = tymatch(lhs, rhs);       /* L1c93 */
            }
            xtype = lhs->sux + rhs->sux;
            /*node->val.num = 0; */
            break;
        case MINUS:                    /* L1caf */
            xtype = lhs->sux + rhs->sux;

            if (isptr((t = lhs->type))) {       /* else L2006 */
                size = lhs->size;
                dptr = lhs->dimptr;

                if (isptr(rhs->type)) { /* else L1d95 */
                    node->sux = xtype;

                    if ((t != rhs->type) || (size != rhs->size)) {
                        terror(node, "pointer mismatch");
                        /* go to L1d83 */
                    } else {
                        t = decref(t);

                        size = getsize(t, size, dptr);

                        if (size != 1) {        /* Not a char array */
                            /* else L1d83 */
                            xtype = 2;

                            sptr = (symnode *)newnode(CONST, 0, 0, size, 0, 0);

                            node = newnode(DIV, node, (expnode *)sptr, 0,
                                    node->lno, node->pnt);
                        }
                    }

                    size = 2;
                    dptr = 0;
                    t = INT;
                    break;
                }
dofix:
                usual(rhs);

                if (!isintegral(rhs->type)) {   /* else L1dbf */
                    notintegral(rhs);
                    makedummy(rhs);
                }

                cvt(rhs, INT);

                node->right = mult_const(size, t, (expnode *)dptr, rhs);
                xtype = lhs->sux + rhs->sux;
            } else {
                t = tymatch(lhs, rhs);       /* copy of L2006 */
                /*goto L2170; */
            }

            break;
        case ASSIGN:                   /* L1df9 */
            ckif_lvalue(lhs, 0);
            chkdecl(rhs);
            tr = ckstrct(rhs);

            if ((isptr(t = lhs->type)) && !(isptr(tr)) &&
                    !(isintegral(tr)) || (isptr(tr) && !isptr(t) && !isintegral(t))) {
                goto asserr;
            }

            cvt(rhs, t);                /* L1e62 */
            goto asscommon;
chass:
            ckif_lvalue(lhs, 0);
            usual(rhs);
            tr = ckstrct(rhs);

            if (isptr((t = lhs->type))) {
                switch (op) {
                    case ASSMIN:       /* L1eab */
                    case ASSPLUS:
                        cvt(rhs, INT);
                        goto do_cht;
                    default:           /* L1ebf */
                        goto asserr;
                }
            }

            switch (op) {
                default:               /* L1ed8 */
                    if (t == FLOAT) {
                        cvt(rhs, DOUBLE);
                    } else {
                        cvt(rhs, t);
                    }
                case ASSSHL:           /* L1ef8 */
                case ASSSHR:
do_cht:
                    node->op = op - 80;  /* make simple *SH */
                    node = chtype(node);        /* call self */

                    if (t == CHAR) {    /* else L1f2f */
                        node->left = lhs->left;
                        release(lhs);
                        lhs = node->left;
                        t = INT;
                    }

                    if ((op - 80) == node->op) { /* else L1f57 */
                        node->op = op;   /* restore to *EQ if valid */
                    }
            }

asscommon:
            size = lhs->size;
            xtype = lhs->sux + rhs->sux;
            dptr = lhs->dimptr;
            break;

            /*goto L2170; *//* L1ebf */
asserr:
            terror(node, "type mismatch");     /* go to L2049 */
            break;
        case COLON:                    /* L1f7f */
            if (isptr((t = lhs->type))) {       /* else L1fde */
                size = lhs->size;
                xtype = lhs->sux;
                dptr = (dimnode *)(lhs->size);

                if (isptr(rhs->type)) { /* else L1fd1 */
                    if ((t != rhs->type) || (lhs->size != rhs->size)) {
                        terror(node, "pointer mismatch");
                    } else {
                        dptr = 0;
                    }
                } else {
                    cktsttyp(rhs);      /* Insure that "? :" pair match */
                }

                /*break; */
            } else {
                if (isptr((t = rhs->type))) {   /*L1fde  else L2006 */
                    size = rhs->size;
                    xtype = rhs->sux;
                    dptr = rhs->dimptr;
                    cktsttyp(lhs);
                    /*break; */
                } else t = tymatch(lhs,rhs);
                /* L2006 */
            }

            break;
        case QUERY:
            /* L2017 */
            t = rhs->type;
            size = rhs->size;
            dptr = rhs->dimptr;
            break;
        default:
            /* L202d */
            if (op >= ASSPLUS) goto chass;
            comperr(node,"type check");
            break;
    }

    node->type = t;
    /* L2170 */
    node->size = size;
    node->sux = xtype;
    node->dimptr = dptr;
    return node;
}

/* *********************************************************** *
 * cktsttyp () - Checks that 2nd & 3rd expressions of a        *
 *      "? ... :" term is of correct type.                     *
 * *********************************************************** */

static void
#ifdef __STDC__
cktsttyp(register expnode * regptr)
#else
cktsttyp(regptr)
    register expnode *regptr;
#endif
{
    if ((regptr->op != CONST) || (regptr->val.num)) {
        terror(regptr, "should be NULL");
        regptr->op = CONST;     /* Store correct value */
        regptr->val.num = 0;
    }
}


static int
#ifdef __STDC__
usual(register expnode * node)
#else
usual(node)
register expnode *node;
#endif
{
    int t;

    chkdecl(node);

    switch (t = node->type) {
        case CHAR:             /* L21dc */
            t = INT;
            cvt(node, t);
            break;
        case FLOAT:            /* L21e1 */
            t = DOUBLE;
            cvt(node, t);
            break;
        case DOUBLE:           /* L2224 */
        case LONG:
        case INT:
        case UNSIGN: break;
        default:               /* L21f0 */
            terror(node, "type error");
            t = INT;
            break;
    }

    return (node->type = t);
}


int
#ifdef __STDC__
cvt(register expnode * node, int t)
#else
cvt(node, t)
    register expnode *node;
    int t;
#endif
{
    /* 4 bytes stack */
    union {
        int       i;
        INTTYPE  *ip;
        LONGTYPE *lp;
        DBLETYPE *dp;
        expnode  *ep;
    } ptr;
    int op;

    op = 0;

    switch (node->type) {
        case CHAR:
            /* L2241 */
            switch (t) {
                case INT:
                    /* _434 (L2245) */
                case UNSIGN:
                    op=CTOI;
                    break;
                case LONG:
                    /* L224a */
                    cvt(node,INT);
                    op=ITOL;
                    break;
                case DOUBLE:
                    /* _437  (L225d) */
                case FLOAT:
                    cvt(node,INT);
                    cvt(node,t);
                    break;
                default:
                    break;
            }

            break;
        default:
            if (!isptr(node->type)) break;
        case INT:
            /* _442 (L22a4) */
            switch(t) {
                case LONG:
                    /* L22a9 */
                    if (node->op == CONST) {
                        /* else L22e4 */
                        ptr.ip = (INTTYPE *)grab(LONGSIZE);

                        if (((ptr.ip)[1] = (node->val.num)) < 0) {
                            *ptr.ip= -1;
                        } else {
                            *ptr.ip = 0;
                        }

                        node->val.lp = ptr.lp;
flag_long:
                        node->op = LCONST;
                        node->size = LONGSIZE;
                    } else {
                        op = ITOL;
                    }

                    break;
                case CHAR:     /* L22e9 */
                    t = INT;
                    break;
                case FLOAT:    /* L22f1 */
                    cvt(node, DOUBLE);
                    op = DTOF;
                    break;
                case DOUBLE:   /* L2302 */
                    if (node->op == CONST) {    /* else L2334 */
                        ptr.dp = (DBLETYPE *)grab(DOUBLESIZE);
                        *ptr.dp = (DBLETYPE)node->val.num;
flag_dbl:
                        node->val.dp = ptr.dp;
                        node->op = FCONST;
                        node->size = DOUBLESIZE;
                        break;
                    }

                    op = ITOD;
                    break;
            }

            break;
        case UNSIGN:           /* L2357 */
            switch (t) {
                case LONG:     /* L235b */
                    op = UTOL;
                    break;
                case FLOAT:    /* L2360 */
                    cvt(node, DOUBLE);
                    op = DTOF;
                    break;
                case DOUBLE:   /* L2371 */
                    op = UTOD;
                    break;
                case CHAR:     /* L2379 */
                    t = INT;
                    break;
            }

            break;
        case LONG:             /* L2398 */
            switch (t) {

                default:       /* L239d */
                    if (!isptr(t)) {
                        break;
                    }
                case INT:      /* _477 (L23aa) */
                case UNSIGN:
                case CHAR:
                    if (node->op == LCONST) {
                        /* Discard MSB of long */
                        ptr.ip = (INTTYPE *)(node->val.num);
                        node->val.num = (ptr.ip)[1];
flag_int:
                        node->op = CONST;
                        node->size = INTSIZE;
                    } else {
                        op = LTOI;
                    }

                    break;
                case FLOAT:    /* L23d2 */
                    cvt(node, DOUBLE);
                    op = DTOF;
                    break;
                case DOUBLE:   /* L23e3 */
                    if (node->op == LCONST) {
                        ptr.dp = (DBLETYPE *)grab(DOUBLESIZE);
                        *ptr.dp = (DBLETYPE)*(node->val.lp);
                        goto flag_dbl;
                    }

                    op = LTOD;  /* L2408 */
                    break;
            }

            break;
        case FLOAT:            /* L2432 */
            switch (t) {
                case LONG:     /* _496 (L2436) */
                case UNSIGN:
                case CHAR:
                case INT:
                    cvt(node, DOUBLE);
                    cvt(node, t);
                    break;
                case DOUBLE:   /* L2450 */
                    op = FTOD;
                    break;
            }

            break;
        case DOUBLE:           /* L247a */
            switch (t) {
                case CHAR:     /* _504 (L247e) */
                case UNSIGN:
                case INT:
                    if (node->op == FCONST) {   /* else L2492 */
                        node->val.num =
                            (int) (*((double *) (node->val.sp)));
                        goto flag_int;
                    } else {
                        op = DTOI;
                    }

                    break;
                case LONG:     /* L2497 */
                    if (node->op == FCONST) {   /* else L24b0 */
                        *(long *) (node->val.lp) =
                            *(double *) (node->val.num);
                        /**(node->val.lp) = *(long *)(node->val.num);*/
                        goto flag_long;
                    } else {
                        op = DTOL;
                    }

                    break;

                case FLOAT:    /* L24b5 */
                    op = DTOF;

                    break;
            }
    }

    /* _429 (L2508) */
    if (op) {
        ptr.ep=newnode(0,0,0,0,0,0);
        nodecopy(node,ptr.ep);
        node->op=op;
        node->left=ptr.ep;
        node->right=0;
    }

    return node->type = t;
}

static int
#ifdef __STDC__
tymatch(register expnode * lhs, expnode * rhs)
#else
tymatch(lhs,rhs)
register expnode *lhs,*rhs;
#endif
{
    int tl,tr;

    tl = usual(lhs);
    tr = usual(rhs);

    if(tl == DOUBLE) return cvt(rhs,DOUBLE);
    else {
        if (tr == DOUBLE) {
            return (cvt(lhs, DOUBLE));
        } else {
            if (tl == LONG) {
                return (cvt(rhs, LONG));
            } else {
                if (tr == LONG) {
                    return (cvt(lhs, LONG));
                } else {
                    if ((tl == UNSIGN) || (tr == UNSIGN)) {
                        return (lhs->type = rhs->type = UNSIGN);
                    } else {
                        return INT;
                    }
                }
            }
        }
    }

    /*return 1; */
}

/* ************************************************************************ *
 * mult_const() - Multiplies a constant by value of myref                   *
 * This may be strictly for pointers                                        *
 * ************************************************************************ */

static expnode *
#ifdef __STDC__
mult_const(int size, int t, expnode * dptr, expnode * rhs)
#else
mult_const(size, t, dptr, rhs)
    int size;
    int t;
    expnode *dptr;                  /*arrmbrs */
    expnode *rhs;
#endif
{
    register expnode *ptr;

    /* If multiplying by 1, return rhs unchanged */

    if ((size = getsize(decref(t), size, (dimnode *)dptr)) == 1) {
        return rhs;
    }

    /* L25f3 */

    ptr = newnode(CONST, 0, 0, size, rhs->lno, rhs->pnt);
    ptr->type = INT;

    ptr = fold(newnode(TIMES, rhs, ptr, 0, rhs->lno, rhs->pnt));

    if (ptr->op == CONST) {
        ptr->sux = 0;
    } else {
        ptr->sux = 2;
    }

    ptr->type = INT;            /* L2657 */
    ptr->size = INTSIZE;

    return ptr;
}

/* **************************************************************** *
 * ckif_lvalue () - Checks if the vartyp requires an lvalue and     *
 *          warns if needed.                                        *
 * Returns: Nothing                                                 *
 * **************************************************************** */

static void
#ifdef __STDC__
ckif_lvalue(register expnode * node, int flag)
#else
ckif_lvalue(node, flag)
    register expnode *node;
    int flag;
#endif
{
    int op;

    switch (op = node->op) {
        case YREG:             /* L27b6 */
        case UREG:
        case STAR:
            return;
    }

    if (op == NAME) {           /* else L26c1 ("lvalue required") */
        chkdecl(node);

        /* If it's a reference or array/struct element, it's OK */

        if (flag) {
            return;
        }

        /* Anything but an array or structure is permissible */

        if ((isary(node->type) == 0) && (node->type != STRUCT)) {
            return;
        }
    }

    terror(node, "lvalue required");   /* L26c1 */
    makedummy(node);

}

void
#ifdef __STDC__
chkdecl(register expnode * node)
#else
chkdecl(node)
    register expnode *node;
#endif
{
    if ((node->op == NAME) && !(node->type)) {  /* else L27b8 */
        terror(node, "undeclared variable");
        makedummy(node);
    }
}

/* ******************************************************************** *
 * membrsize () - Update a struct/union member                          *
 *      If it's an address (pointer), copy Left ref to current CREF     *
 *      Else ifs it's a USRLBL, set vartyp, ft_Ty to "int", cmdval to   *
 *        label number, and __cr18 = 0                                  *
 *      In any other case, error, reset expnode members to default      *
 * Returns: size of struct/union member                                 *
 * ******************************************************************** */

static int
#ifdef __STDC__
membrsize(expnode * node, int *t, int *mosar)
#else
membrsize(node, t, mosar)
    register expnode *node;
    int *t;
    int *mosar;
#endif
{
    register expnode *eptr;

    *t = node->type;

    if (node->op == AMPER) {    /* else L2746 */
        *mosar = 1;

        eptr = node->left;
        eptr->dimptr = node->dimptr;
        nodecopy(eptr, node);
        release(eptr);
    } else {
        *mosar = 0;
    }

    chkdecl(node);              /* L274b */

    if (node->op == NAME) {     /* else L278b */
        eptr = (expnode *) (node->val.sp);

        if (((symnode *) eptr)->storage != MOS) {       /* else L276c */
            goto needstruct;
        }

        node->op = CONST;
        node->val.num = ((symnode *) eptr)->offset;
        node->sux = 0;
        node->type = INT;
        return ((symnode *) eptr)->size;
    }

needstruct:
    terror(node, "struct member required");
    makedummy(node);
    node->op = CONST;
    node->val.num = 0;
    node->sux = 0;
    *t = INT;
    return INTSIZE;
}

int
#ifdef __STDC__
ckstrct(register expnode * node)
#else
ckstrct(node)
    register expnode *node;
#endif
{
    if ((node->type == STRUCT) || (node->type == UNION)) {
        terror(node, "structure or union inappropriate");
        makedummy(node);
    }

    return node->type;
}

/* ************************************************************ *
 * makedummy () - Reset data in a expnode after an error        *
 *                                                              *
 * ************************************************************ */

void
#ifdef __STDC__
makedummy(register expnode * node)
#else
makedummy(node)
    register expnode *node;
#endif
{
    move((char *)&sdummy, (char *)node, 6);
    node->sux = 1;
    reltree(node->left);
    reltree(node->right);
    node->left = node->right = 0;
    node->op = NAME;
    node->val.sp = (symnode *)(&sdummy);
}

/* ************************************************************ *
 * isintegral() - checks that a command is an integral type.    *
 * Returns: TRUE if it is integral                              *
 *          FALSE on anything else                              *
 * ************************************************************ */

int
#ifdef __STDC__
isintegral(int tstval)
#else
isintegral(tstval)
    int tstval;
#endif
{
    switch (tstval) {
        case INT:
        case CHAR:
        case LONG:
        case UNSIGN:
            return 1;
    }

    return 0;
}

void
#ifdef __STDC__
notintegral(expnode * node)
#else
notintegral(node)
    expnode *node;
#endif
{
    terror(node, "must be integral");
}
