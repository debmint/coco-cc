/* **************************************************************** *
 * ccomp_08.c                                                       *
 * from p1_06.c                                                     *
 *                                                                  *
 *  $Id:: comp_08.c 73 2008-10-03 20:20:29Z dlb                   $ *
 * **************************************************************** */

#ifndef COCO
#ifndef OSK
#ifndef direct
#define direct
#endif
#endif
#endif

/*direct int datstring,
           datflag;*/

#include "cj.h"

void _coc_dtof(
#ifdef __STDC__
                  int *, int *
#endif
    );

static void rsrvnulls(
#ifdef __STDC__
                         int
#endif
    );

static int ilist(
#ifdef __STDC__
                        int type, symnode *, dimnode *, int
#endif
    );

static int prtconst(
#ifdef __STDC__
                       int
#endif
    );

static void datdef(
#ifdef __STDC__
                           void *, int
#endif
    );

static void iskip();

/* ******************************************************************** *
 * signextend () - Returns the sign-extended value for the number       *
 *                 passed as a parameter                                *
 * Passed:  (1) Value to extend                                         *
 *          (2) COCO size of the value in bytes                         *
 *                1 for char, 2 for int                                 *
 * Returns: The value with sign extended                                *
 * ******************************************************************** */

#ifndef COCO
int
#ifdef __STDC__
signextend(int val, int varsiz)
#else
signextend(val, varsiz)
int val, varsiz;
#endif
{
#ifndef _OSK
    int _mask[2] = { 0xff, 0xffff };
#else
    /* OSK cannot preinitialize static variables ??? */
    int _mask[2];

    _mask[0] = 0xff;
    _mask[1] = 0xffff;
#endif

    if (val & (0x80 << ((varsiz - 1) * 8))) {
        return (val | (-1 ^ _mask[varsiz - 1]));
    } else {
        return val;
    }
}
#endif

/* **************************************************************** *
 * do_initvar () - Process a label name when there is an assignment *
 * Passed : (1) - * lblstruct (for the label)                *
 *          (2) - FT_type for label                                 *
 *          (3) - gntyp - the gentype for the label                 *
 * **************************************************************** */

void
#ifdef __STDC__
do_initvar(register symnode * ptr, int tsc, int type)
#else
do_initvar(ptr, tsc, type)
register symnode *ptr;
int tsc;
int type;
#endif
{
    /*dimnode *dimp;*/              /* Also char * */
    union {
        char *c;
        dimnode *d;
    } p;
    int count;                  /* Remainder of str after init data */

    /* print appropriate vsec */

    switch (tsc) {
        case TYPEDEF:          /* L3cc7 */
        case EXTERN:
        case EXTERND:
            return initerr();   /* can't initialize these dudes! */
        case DIRECT:
            /* L3ccd */
        case STATICD:
            vsect(DDP);     /* dp variable section */
            break;
        default:
            vsect(NDP);     /* non-dp variable section */
            break;
    }

    if (!blklev) {              /* L3cfe */
        /* else L3d30 */
        p.c = ptr->sname;

        if ((tsc != STATIC) && (tsc != STATICD)) {
            nlabel(p.c, 1); /* with colon *//* Global label */
        } else {
            ptr->storage = EXTDEF;      /* _16 */
            nlabel(p.c, 0);
        }
    } else {
        olbl(ptr->offset);  /* function local */
        nl();
    }

    if (type == (CHAR | ARRAY)) {
        /* L3d3c */
        /* else L3d90 */
        datstring = 1;
        getsym();

        if (sym == STRING) {
            /* else L3d89 */
            p.d = ptr->dimptr;

            if (p.d->dim == 0) {       /* else L3d64 */
                p.d->dim = stringlen;
            } else {
                if ((count = p.d->dim - stringlen) >= 0) {       /* L3d64 */
                    oz(count);
                } else {
                    error("too long");
                }
            }

            getsym();
            /* L3d81 */
            goto do_endsct;
        }

        datstring = 2;
        /* L3d89 */
    } else {
        datstring = 2;
        getsym();
    }

    if (type == STRUCT) {       /* else L3db5 */
        ilist(type, ptr, (dimnode *)(ptr->x.elems), 0);
        /* go to L3de7 */
    } else {
        if (isary(type)) {      /* L3db5 */
            ilist(type, ptr, ptr->dimptr, 0);
        } else {
            if (!(ilist(type, ptr, ptr->dimptr, 1))) {
                iskip();     /* Look for comma or ";" */
            }
        }
    }

  do_endsct:
    endsect();
    datstring = 0;
}


static int
#ifdef __STDC__
ilist(int type, register symnode * ptr, dimnode * list,     /* or memberdef for structs */
          int level)
#else
ilist(type, ptr, list, level)
int type;
register symnode *ptr;
dimnode *list;
int level;
#endif
{
    int flag;
    int t;
    unsigned int i;
    dimnode *newlist;
    unsigned int n;
    elem *e;

    if(level == 0)
        need(LBRACE);
    else if(sym == LBRACE) {
        flag = 1;
        getsym();
    } else {
        flag = 0;
    }

    if (isary(type)) {
        /* L3e25 */
        /* else L3f13 */
        /* Array initialization */
        if (!(n = list->dim)) {
            n = -1;
        }

        if ((t = decref(type)) == STRUCT)
        {     /* L3e3f */
            newlist = (dimnode *) ptr->x.elems;
        } else {
            newlist = list->dptr;       /* L3e55 */
        }

        i = 0;

        while (sym != RBRACE) { /* L3e98 */
            /*L3e62 */
            if (ilist(t, ptr, newlist, (level + 1))) {
                if ((++i >= n) || (sym != COMMA)) {
                    break;
                }

                getsym();
                /* The following is needless, and can be removed once
                 * the code is acertained to be identical */
#ifdef DEVEL
#asm
                fdb $2000
#endasm
#endif
            } else {
                return 0;
            }
        }

        if (n == -1) {
            list->dim = i;
        } else {
            /* If less than full array has been initialized, fill
             * remainder with nulls
             */

            if (i < n) {        /* L3eaf */
                rsrvnulls(getsize(t, ptr->size, list->dptr) *
                          (n - i));
            }
        }

      L3edc:
        if ((level) && (!flag)) {
            return 1;
        }

        if (sym == COMMA) {
            getsym();
        }

        if (sym == RBRACE) {
            getsym();
            return 1;
        }

        error("too many elements");
        iskip();
        return 1;
    }

    /* Here, we need to change defs for list, and some vars,
     * since we are now dealing with structs rather than braces */

    /* L3f13 */
    if (type == STRUCT) {       /* else L3f9e */
        if (!(e = (elem *) list)) {     /* else L3f6c */
            error("unions not allowed");
            iskip();
            return 0;
        }

        while (sym != RBRACE) {
            ptr = e->element;   /* L3f2c */

            if (!ilist(ptr->type, ptr,
                           (ptr->type == STRUCT ?
                            (dimnode *) (ptr->x.elems) :
                            (ptr->dimptr)), (level + 1))) {
                return 0;
            }

            if ((e = e->strnext) && (sym == COMMA)) {   /* else L3f95 */
                getsym();
                continue;       /* not necessary, really */
            } else {
                break;
            }
        }

        while (e) {             /* L3f95 */
            ptr = e->element;
            rsrvnulls(getsize(ptr->type, ptr->size, ptr->dimptr));
            e = e->strnext;
        }

        goto L3edc;
    }

    if (prtconst(type)) {       /* L3f9e */
        /* else L3fbc */
        if (flag) {             /* else L3fb7 */
            need(RBRACE);
        }

        return 1;
    } else {
        error("constant expression required");
        iskip();
    }

    return 0;
}

static void
#ifdef __STDC__
rsrvnulls(int nulcount)
#else
rsrvnulls(nulcount)
int nulcount;
#endif
{
    ot("rzb ");
    od(nulcount);
    nl();
}

/* ************************************************************************ *
 * prtconst () - Output constant value                                      *
 * Passed : FT_TY                                                           *
 * Returns: Non-Zero if value is a constant, NULL if not a constant         *
 * ************************************************************************ */

static int
#ifdef __STDC__
prtconst(int type)
#else
prtconst(type)
int type;
#endif
{
    register expnode *p;
    expnode *p2;
    int nval;
    int done;
    int t;

    if (!(p = optim(parsexp(CHAR)))) {  /* must be char or int */
        return 0;
    }

    nval = 0;
    done = 1;
    t = p->type;

    if (isptr(type)) {          /* else L4054 */
        switch (t) {
            case LONG:         /* L4031 */
                cvt(p, INT);    /* do this at L407d */
                break;          /* jump to L4086 */
            default:           /* L4036 */
                if (!isptr(t)) {
                    goto not_const;
                }
            case INT:          /* L4086 */
            case UNSIGN:
                break;
        }
    } else {                    /* L4054 */
    if (isptr(t)) {             /* else L406e */
        if (!isintegral(type)) {        /* else L4086 */
            goto not_const;
        }
    } else {                    /* Not a pointer */
        /* L406e */
        cvt(p, ((type == FLOAT) ? DOUBLE : type));
    }
    }

    /* L4086 here */

    if ((p->op == PLUS) || (p->op == MINUS)) {  /* else L40df */

        if ((p2 = p->right)->op != CONST) {     /* else L40b1 */
          not_const:
            done = 0;
            goto const_ret;
        }

        if (p->op == MINUS) {
            nval = -(p2->val.num);
        } else {
            nval = p2->val.num; /* L40c7 */
        }

        reltree(p2);
        p2 = p;
        p = p->left;
        release(p2);
    }

    /* L40df */

    if (p->op == AMPER) {       /* else L4143 */
        switch (((p2 = p->left)->val.sp)->storage) {
            default:           /* L40f5 */
                done = 0;
                break;          /* go to L4192 */
            case STATIC:
            case EXTERN:       /* L40fc */
            case EXTDEF:
            case DIRECT:
            case EXTERND:
                defword();
                datflag = 1;
                deref(NODE, (int) p2, nval);
                datflag = 0;
                nl();
                break;          /* go to L4192 */
        }
    } else {                    /* L4143 */
        switch (p->op) {
                /*struct dbltree *tree;
                   int dbl[2];
                   int *valptr; */

            default:           /* L40f5 */
                done = 0;
                break;
            case FCONST:       /* L4147 */
                /* NOTE: This is strictly for the cross-compilers.
                 *       It will not work on the COCO
                 * Note2: The above is not in effect
                 */

                /*tree = p->cmdval; */

                if (type == FLOAT) {    /* else L415b */
                    /*valptr = dbl; */
                    /*                   _coc_dtof (tree->cocoarr, valptr); */
                    *(p->val.fp) = *(p->val.dp);
                }
                /*else {
                   valptr = tree->cocoarr;
                   } */

                /*datdef (p->val.num, type); Just let it fall through
                   break; */

            case CONST:        /* L415b */
            case LCONST:
                datdef((void *)(p->val.num), type);

                break;
            case STRING:       /* L4169 *//* '7' */
                defword();
                label(p->val.num);
                break;
        }
    }

  const_ret:
    reltree(p);
    return done;
}

static void
#ifdef __STDC__
datdef(void *p, int type)
#else
datdef(p,type)
#ifdef COCO
register
#endif
void *p;
int type;
#endif
{
    int size;

    switch (type) {
        case CHAR:             /* L41af */
            defbyte();
            od((int)p);
            nl();
            return;
        case INT:              /* L41be */
        case UNSIGN:
        default:
            size = INTSIZE / 2;
            break;
        case LONG:             /* L41c3 */
            size = LONGSIZE / 2;
            break;
/*#ifndef COCO
            p = split_long(numarray, (int)p);
            break;
#endif*/
        case FLOAT:
            size = FLOATSIZE / 2;
            break;
        case DOUBLE:           /* L41c8 */
            size = DOUBLESIZE / 2;
            break;
    }

    defcon(p, size);
}

void
#ifdef __STDC__
initerr(void)
#else
initerr()
#endif
{
    error("cannot initialize");
    iskip();
}


static void
#ifdef __STDC__
iskip(void)
#else
iskip()
#endif
{
    for (;;) {
        switch (sym) {
            case COMMA:
            case SEMICOL:
            case EOF:
                return;
            default:
                getsym();
        }
    }
}
