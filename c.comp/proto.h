/* ************************************************************** *
 * proto_h - Prototypes for functions in dis68                    $
 *                                                                $
 * This file handles both STDC and non-STDC forms                 $
 * $Id::                                                          $
 * ************************************************************** */

#ifndef _HAVEPROTO_
#define _HAVEPROTO_
#ifdef __STDC__
/* bool.c */
void tranbool(expnode *node, labstruc *tlab, labstruc *flab, int nj);
int isaleaf(expnode *node);
/* build.c */
expnode *parsexp(int priority);
int getconst(int p);
int constop(int op, int r, int l);
expnode *newnode(int op, expnode *left, expnode *right, int value, int lno, char *errpnt);
void experr(void);
/* chcodes.c */
/* cmain.c */
int main(int argc, char **argv);
void errexit(void);
/* cocodbl.c */
int *cocodbl(char *str, int *cocdbl);
void _coc_dtof(int *dbl, int *flt);
double scale(char *dblarr, int decplaces, int pos_expon);
/* codgen.c */
void gen(int op, int rtype, int arg, expnode *val);
void defcon(short *p, int n);
void deref(int arg, int val, int offset);
void ot(char *s);
void ol(char *s);
void nl(void);
void os(char *s);
void od(int n);
void label(int n);
void olbl(int n);
void on(char *s);
int modstk(int nsp);
void nlabel(char *ptr, int scope);
/* declare.c */
void funcmain(void);
void block(void);
int settype(int *size, dimnode **dimptr, elem **ellist);
int declarator(symnode **ptr, dimnode **dptr, int bastype);
int sizeup(register symnode *ptr, dimnode *dimptr, int size);
int getsize(int t, int size, dimnode *dptr);
void clear(symnode **list);
void identerr(void);
/* fadjust.c */
double fadjust(double n);
/* floats.c */
void dload(expnode *cref);
void trandexp(expnode *node);
/* get.c */
void initbuf0(void);
void blanks(void);
void getch(void);
/* inits.c */
int signextend(int val, int varsiz);
void do_initvar(register symnode *ptr, int tsc, int type);
void initerr(void);
/* lex.c */
void getsym(void);
void lexinit(void);
symnode *lookup(char *name);
char *grab(int size);
void oz(int n);
/* local.c */
void epilogue(void);
void locstat(int l, int size, int area);
void defglob(symnode *ptr, int size, int area);
void extstat(symnode *ptr, int size, int area);
void profname(char *name, int lab);
void startfunc(char *name, int flag, int lab);
void prt_profend(void);
void endfunc(void);
void defbyte(void);
void defword(void);
void comment(void);
void vsect(int area);
void endsect(void);
void tidy(void);
/* longs.c */
void lload(expnode *ptr);
void tranlexp(expnode *node);
void getadd(expnode *ptr);
void pushslong(register expnode *p);
/* misc.c */
void pushdown(register symnode *sptr);
void pullup(register symnode *dstdef);
void move(register char *_src, register char *_dest, int siz);
void fatal(char *errstr);
void multidef(void);
void error(char *_str);
void comperr(expnode *node, char *errstr);
void terror(register expnode *node, char *errstr);
void reltree(register expnode *myref);
void release(register expnode *node);
void nodecopy(expnode *n1, expnode *n2);
int istype(void);
int issclass(void);
int decref(int t);
int incref(int p1);
int numeric_op(int op);
dimnode *prevbrkt(register dimnode *dptr);
int need(int key);
void junk(void);
/* optim.c */
expnode *optim(register expnode *tree);
void diverr(void);
int cvt(register expnode *node, int t);
void chkdecl(register expnode *node);
int ckstrct(register expnode *node);
void makedummy(register expnode *node);
int isintegral(int tstval);
void notintegral(expnode *node);
/* stats.c */
void statement(void);
/* tranexp.c */
void lddexp(expnode *tree);
void ldxexp(expnode *tree);
void loadexp(expnode *tree);
expnode *tranexp(expnode *tree);
int isashift(int x);
void doquery(expnode *cref, void (*fnc)(expnode *));
void docall(expnode *node);
int L76bf(expnode *node);
int crf_isint(expnode *cref);
void getinx(expnode *cref);
void dostar(expnode *node);
int isreg(int r);

#else

/* bool.c */
void tranbool();
int isaleaf();
/* build.c */
expnode *parsexp();
int getconst();
int constop();
expnode *newnode();
void experr();
/* chcodes.c */
/* cmain.c */
int main();
void errexit();
/* cocodbl.c */
int *cocodbl();
void _coc_dtof();
double scale();
/* codgen.c */
void gen();
void defcon();
void deref();
void ot();
void ol();
void nl();
void os();
void od();
void label();
void olbl();
void on();
int modstk();
void nlabel();
/* declare.c */
void funcmain();
void block();
int settype();
int declarator();
int sizeup();
int getsize();
void clear();
void identerr();
/* fadjust.c */
double fadjust();
/* floats.c */
void dload();
void trandexp();
/* get.c */
void initbuf0();
void blanks();
void getch();
/* inits.c */
int signextend();
void do_initvar();
void initerr();
/* lex.c */
void getsym();
void lexinit();
symnode *lookup();
char *grab();
void oz();
/* local.c */
void epilogue();
void locstat();
void defglob();
void extstat();
void profname();
void startfunc();
void prt_profend();
void endfunc();
void defbyte();
void defword();
void comment();
void vsect();
void endsect();
void tidy();
/* longs.c */
void lload();
void tranlexp();
void getadd();
void pushslong();
/* misc.c */
void pushdown();
void pullup();
void move();
void fatal();
void multidef();
void error();
void comperr();
void terror();
void reltree();
void release();
void nodecopy();
int istype();
int issclass();
int decref();
int incref();
int numeric_op();
dimnode *prevbrkt();
int need();
void junk();
/* optim.c */
expnode *optim();
void diverr();
int cvt();
void chkdecl();
int ckstrct();
void makedummy();
int isintegral();
void notintegral();
/* printf.c */
int printf();
int fprintf();
int sprintf();
/* stats.c */
void statement();
/* tranexp.c */
void lddexp();
void ldxexp();
void loadexp();
expnode *tranexp();
int isashift();
void doquery();
void docall();
int L76bf();
int crf_isint();
void getinx();
void dostar();
int isreg();
#endif   /* __STDC__*/

#endif   /* #ifndef _HAVEPROTO_*/

