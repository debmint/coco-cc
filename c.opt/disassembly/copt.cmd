+ z pd0 S=copt.lbl S=miscos9lbl
* The following can be deleted after being defined
" L 0031 "The following ref is to etext"
" L 003d "The following ref is to dpsiz"
" L 0057 "The following ref is to btext"
" L 006b "The following ref is to memend"
" L 006f "The following ref is to argv"
" L 0077 "The following ref is to argc"
" L 00ef "The following ref is to main()"
" L 00f6 "The following ref is to exit"
" L 00f9 "The following ref is to end"
" L 00fd "The following ref is to _mtop"
" L 0101 "The following ref is to _sttop"
" L 0106 "The following ref is to _stbot"
" L 014c "The following ref is to _exit"

" D 23 'Begin of Init Non-DP data'
" D 0515 ' End of Init Non-DP data'

A 0121-0139

* Redefine Y and U references for "C"

> Y D; U &; #D &

* Program name - Adjust the end address
* The version byte follows, it needs no adjustment
A 0d-11; S &

> R & 57
> U & 62; U D 003d-0061; U D 0065-0077; U D (+&2) 007b; U D (+&1) 0083
> U D (+&1) 00a5-00c7; U D 00df-00e5

> #1 ^ 008b-00d0
> #1 E 013d

" L 0167 '
End of cstart
Begin of part 1
'

> #1 ^ 01A1-01CB; #X ^ 021c; #1 ^ 03e1-0400; #1 ^ 0443
> D D (+ & 2) 600-609
> #1 ^ 050E-0516; #X ^ 0542-0565

A 069f-06B2; B; A -06C1; B; A; B; A -06D1; B
A; B; A -06E1; B; A -06ee; B
* The following is a tab... we'll do it as ASCII
A -0708; B
* Another tab
A -072b; B; A -0750; B; A -0773; B; A -077B; B
A -0780; B; A /3; B; A /3; B; A -078E; B
A -0792; B; A -0798; B; A /3; B; A /3; B
* Null string?
B; A -07ab; B

" L 07ad '
Begin of part 2
'

> #1 ^ 07C0-07C9; #1 ^ 0810; #1 ^ 083D-0844
> #1 ^ 08A1-08A8

A 08FF-091D; B

" L 091F '
Begin of part 3
'

> #U & 093D;

A 0A53 /4; B

" L 0A58 '
Begin of part 4
'

> D D (+ & 2) 0a82
> Y D (+&24) 0b26
> #X ^ 0C53-0C66; #D ^ 0DA1-0DB5

A 1052 /2; B
17 A /2; B
A -108b; B;
2 A /3; B
A -10ab; B; A -10ca; B; A /2; B; A -10D6; B
A ; B; B; A /2; B; A /3; B

" L 10e2 '
Begin of part 5
'

> #1 $ 1306; #1 ^ 1371

A 17A9 /3; B;
5 A /3; B
A -17c4; B
9 A /4; B

" L 17f3 '
Begin of part 6
'

> #D ^ 1848

A 185e-186c; B; A - 1880; B

" L 1882 '
Begin of Library routines
'

A 1fab-1fb0; B
S & 23eb; S & 23f6
> Y D (+ &2) 271D

" L 27DC '
Begin of Initialized Data
'

' L 27DC .    Init DP data size
' L 27de .   To D0001
' L 27e0 .   To D0003
' L 27e2 .   To D0005
' L 27e4 .   To D0007


L & 27DC; S &; L &; L &; L &

' L 27E5 .  Init Non-DP data size
L &

' L 27e7 .   To dpsiz
' L 27e9 .   To D0025
' L 27eb .   T0 D0027
' L 27ed .   To D0029
' L 27ef .   To D002b
' L 27f1 .   To D002d
' L 27f3 .   To D002f
' L 27f5 .   To D0031
' L 27f7 .   To D0033
' L 27f9 .   To D0035
' L 27fb .   To D0037
' L 27fd .   To D0039
' L 27ff .   To D003b
' L 2801 .   To D003d
' L 2803 .   To D003f
' L 2805 .   To D0041
' L 2807 .   To D0043
' L 2809 .   To D0045
' L 280b .   To D0047
' L 280d .   To D0049

2 W
18 L L
*W 27ed-280e

' L 280f . To D004b
' L 2811 . To D004d
' L 2813 . To D004f
' L 2815 . To D0051
* Register/cmd names
4 A; B

' L 2817 .    To D0053
' L 281b .    To D0057
' L 281f .    To D005b

3 A /3; B

' L 2823 .    To D005f
' L 2829 .    To D0065
' L 2831 .    To D006d

A /5; B; A /7; B; A /6; B
* following @ 2838
' L 2838 .    To D0074
' L 283c .    To D0078
' L 2840 .    To D007c
3 A /3; B


' L 2844 .    To D0080
A /4; B
* opcodes

' L 2849 .    To D0085
' L 284e .    To D008a
' L 2853 .    To D008f
' L 2858 .    To D0094
' L 285d .    To D0099
' L 2862 .    To D009e
6 A /4; B

' L 2867 .    To D00a3
' L 286b .    To D00a7
' L 286f .    To D00ab
' L 2873 .    To D00af
' L 2877 .    To D00b3
' L 287b .    To D00b7
' L 287f .    To D00bb
' L 2883 .    To D00bf
' L 2887 .    To D00c3
' L 288b .    To D00c7
10 A /3; B
' L 288f .    To D00cb
' L 2894 .    To D00d0
' L 2899 .    To D00d5
' L 289e .    To D00da
' L 28a3 .    To D00df
' L 28a8 .    To D00e4
6 A /4; B
' L 28ad .    To D00e9
' L 28b1 .    To D00ed
2 A /3; B
* "#0" follows
' L 28b5 .    To D00f1
A /2; B
* following is @ $28B8
' L 28b8 .    To D00f4
' L 28bc .    To D00f8
' L 28c0 .    To D00fc
3 A /3; B
' L 28c4 .    To D0100
' L 28c9 .    To D0105
2 A /4; B
' L 28ce .    To D010a
' L 28d2 .    To D010e
' L 28d6 .    To D0112
3 A /3; B
' L 28da .    To D0116
' L 28df .    To D011b
2 A /4; B
' L 28e4 .    To D0120
' L 28e8 .    To D0124
' L 28ec .    To D0128
3 A /3; B
' L 28f0 .    To D012c
' L 28f5 .    To D0131
2 A /4; B
' L 28fa .    To D0136
' L 28fe .    To D013a
2 A /3; B
' L 2902 .    To D013e
' L 2907 .    To D0143
A /4; B; A /2; B

" L 290a '
To D0146'
5 L &;L D; W; L D; W /8
" L 295a 'To D0196'
6 L &; L D /8; W; L D; W /2

" L 29ba '
To D01f6 (struct)'
6 L &; L D; W; L D /4; W /6

" L 2a1a '
To D0256'
2 L &; L D; W; L D; W /8

" L 2a3a 'To D0276'
12 L &; L D /4; W; L D; W; L L; W /2

" L 2afa '
To D0336'
2 L &; L D; W; L D /6; W /4

" L 2b1a '
To D0356'
L &; L D /6; L D; W; L L; W /2
" L 2b2a '
To D0366'
L &; L D /6; L D; L D; L D; W /2
" L 2b3a '
To D0376'
L &; L D /6; L D; L &; L D; W /2
" L 2b4a '
To D0386'
" L 2b5a '
To D0396'
2 L &; L D /6; L L; L &; L D; W /2

"L 2b6a 'Marks the end of the above table
Chould the code after this also be part of the structs???'
L &
" L 2b6c '
To D03a8 (????)'

B /16
" L 2b7c 'To D03b8 (_chcodes)'
B /128

" L 2bfc 'To D0438'
L & /8
" L 2c04 'To D0440'
L D
' L 2C06 .  To D0442;
A /2; B;

" L 2c09 '_iob's for stdin, stdout, stderr, etc'
16 W /10; B; W


" L 2cd9 'DataText references'
L & 2cd9; L D /66

" L 2d1d 'Data-Data references'
L &; L D  /260

* c.opt name
A 2e23/5; B
