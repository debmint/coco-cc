/* ******************************************************** *
 * Part3.c                                                  *
 *                                                          *
 * $Id::                                                $   *
 * ******************************************************** */

#include "rma.h"

        /* The next section comes from part3.c */

#ifdef COCO
int (*Drectives[]) ()
#else
JMPTBL Drectives[]
#endif
         = {
         asm_dirct, do_ifs, do_endc,
         equset, setmac, endmac
     };

#ifdef COCO
int (*j_secs[]) ()
#else
JMPTBL j_secs[]
#endif
= {_psect, csectsetup, vsectsetup, asm_dirct};

#ifdef COCO
int (*codtabl[]) ()
#else
    JMPTBL codtabl[]
#endif
    = {
        l_brnch,   cc_stuff, int_stuf,
        chr_stuf,  bit_stuf, no_opcod,
        lea_s,     tfr_exg,  stk_stuf,
        lb_,       do_brnch, equset,
        asm_dirct, csectsetup
#ifndef DEVEL
        ,q_immed,  do_md,    do_tfm,
        do_band,   do_aim,   do_divd,
        do_addr,   chr_ef
#endif
};

/* CsecTbl is  a csect */

#ifdef COCO
int (*CsecTbl[]) ()
#else
    JMPTBL CsecTbl[]
#endif
    = {equset, end_csect};

    /* The following is for vsect within a psect.  VsecTbl is for
     * use outside of a psect
     */

#ifdef COCO
int (*VsecInCode[]) ()
#else
    JMPTBL VsecInCode[]
#endif
    = {equset, codsetup};

/* VsecTbl is for code within a vsect when it's outside of a psect */

#ifdef COCO
int (*VsecTbl[]) ()
#else
    JMPTBL VsecTbl[]
#endif
    = {equset, precodeset};

extern
#ifdef COCO
int (*CnstTbl[]) (),  (*D03b6[])(), (*_if_tbl[])();
#else
    JMPTBL CnstTbl[], D03b6[], _if_tbl[];
#endif
extern CODFORM S_realcmds[], S_Rmb[], S_Rmb3[], S_Nam[], S_branch[];
extern /*direct*/ CODFORM *SBsr_End, *SLbrEnd, *S_Rmbend, *Rmb3End;

int is_pass1(ZILCH);

#ifndef DEVEL
static int poprnd (
#ifdef __STDC__
    int
#endif
);
#endif

#ifdef COCO
#   define strchr(a, b) index(a, b)
#endif

int
l_brnch (ZILCH)

    /* lbra & lbsr */
{
    NumBytes = 3;
    return (brnch_size ());
}

#ifndef DEVEL
int
do_aim (ZILCH)
    /* aim, oim, tim, eim  */
{

    AIMFlg = 1;

    if (cc_stuff ())
    {                           /* first, get memory byte */
        if (*SrcChar == ',')
        {
            ++SrcChar;
            /*++opr_ptr;  Bump union ptr by one byte */
            /* to allow for extra byte    */
            return bit_stuf ();
        }
    }
    return 0;
}

int
do_band (ZILCH)
{
    char bitpchr,
      namhold[SYMLEN],
     *sr_tmp;
    unsigned bitpos;
    int count;
    char *boff = "01234567CVZNIHFE";

    NumBytes = 4;

    switch (getreg (0))
    {
        case 0x08:             /* accA */
            INDX_BYT |= 0x40;
            break;
        case 0x09:             /* accB */
            INDX_BYT |= 0x80;
            break;
        case 0x0a:             /* CC   */
            break;              /* default */
        default:
            return bd_rgnam ();
    }

    if (*SrcChar == '.')
    {
        bitpchr = *(++SrcChar);

        if (islower ( (int)bitpchr) )
        {
            bitpchr = _toupper (bitpchr);
        }

        if ( (bitpos = (int) strchr (boff, bitpchr)) )
        {
            ++SrcChar;
            bitpos -= (int) boff;   /* make bitpos offset into string */

            if ( ((INDX_BYT) && (bitpos < 8)) || ! (INDX_BYT) )
            {
                if (bitpos > 7)
                {
                    bitpos -= 8;
                }

                INDX_BYT |= bitpos;

                if (*SrcChar == ',')
                {
                    ++SrcChar;

                    if (*SrcChar == '<')
                    {
                        ++SrcChar;
                    }

                    count = 0;

                    while ( (count < SYMLEN - 1) && (namhold[count] = *(SrcChar++)) &&
                            (namhold[count] != '.')                          )
                    {
                        ++count;
                    }

                    namhold[count] = '\0';
                    sr_tmp = --SrcChar; /* Tmp save SrcChar */
                    SrcChar = namhold;

                    if (getNmbr () == 0)
                    {
                        SrcChar = sr_tmp;
                        return (e_report ("bad number for band operation"));
                    }

                    SrcChar = sr_tmp;

                    if ((NmbrInt > -256) && (IndxVal < 256))
                    {
                        IndxVal = NmbrInt;
                        rmcoderefs ();
                        genreflist ();
#ifdef COCO
                        MLCod.opers.shrtlng.f_int = IndxVal;
#else
                        storInt(&opr_ptr[1], IndxVal);
#endif

                        if (*SrcChar == '.')
                        {
                            ++SrcChar;

                            if ( (bitpos = (int) strchr (boff, *SrcChar)) )
                            {
                                ++SrcChar;
                                bitpos -= (int) boff;

                                if (bitpos < 8)
                                {
                                    INDX_BYT |= (bitpos << 3);
                                    return 1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return ilAdrMod ();
}

int
q_immed (ZILCH)
    /* does q register (needed for long usage on immediate mode */
{
    if (immediat ())
    {
        NumBytes += 4;
        do_long ();
#ifdef COCO
        opr_ptr->q_long = NmbrInt;
#else
        storInt (opr_ptr, (NmbrInt >> 16) & 0xffff);   /* save 16 MSB */
        storInt (&(opr_ptr[2]), NmbrInt & 0xffff);     /* and 16 LSB */
#endif
        return (not_immed ());
    }
    else
    {
        MLCod.prebyte = 0x10;   /* all other modes require MLCod.prebytee */
        CmdCod = &MLCod.prebyte;
        ++NumBytes;
        MLCod.opcode = 0xcc;    /* Opcode is also different */
        return (regofst ());
    }
}

int
do_md (ZILCH)
{
    if ( ! immediat ())
    {
        return 0;
    }

    ++NumBytes;
    proc_byte ();
    INDX_BYT = NmbrInt;
    return 1;
}

         /* add, sub, cmp, ld */
         /* for special cases that include "E" & "F" regs
            if "E" or "F" not found, falls through to chr_stuf() */

int
chr_ef (ZILCH)
    /* 8-bit processes */
{
    int lngreg = 0;

    switch (*CmdNamEnd)
    {
        case 'e':
            *(--CmdCod) = 0x11;
            ++NumBytes;
            break;
        case 'f':
            *(--CmdCod) = 0x11;
            ++NumBytes;
            MLCod.opcode |= 0x40;
            break;
        default:
            return (chr_stuf ());       /* then check for standard regs */
    }

    return poprnd (lngreg);
}

int
do_divd (ZILCH)
{
    return poprnd (0);
}

int
do_tfm (ZILCH)
{
    int rg0,
      rg1;
    char nxtc;
    int sgn0,
      sgn1;

    sgn0 = sgn1 = 0;
    NumBytes += 1;

    if (((rg0 = getreg (0)) >= 0x100) && (rg0 <= 0x104) &&
        (((nxtc = *SrcChar) == ',') || (nxtc == '+') || (nxtc == '-')))
    {
        if (nxtc != ',')
        {
            ++SrcChar;
            if (nxtc == '+')
            {
                ++sgn0;
            }
            else
            {
                --sgn0;
            }
            if (*SrcChar != ',')
            {
                return bdrglist ();
            }
        }
        
        ++SrcChar;
        
        if (((rg1 = getreg (0)) >= 0x100) && (rg1 <= 0x104))
        {
            if (((nxtc = *SrcChar) == '+') || (nxtc == '-'))
            {
                ++SrcChar;
                if (nxtc == '+')
                {
                    ++sgn1;
                }
                else
                {
                    --sgn1;
                }
            }
            
            if (sgn0 || sgn1)
            {
                if ((sgn0 > 0) && (sgn1 > 0))
                {
                    MLCod.opcode = 0x38;
                }
                else
                {
                    if ((sgn0 < 0) && (sgn1 < 0))
                    {
                        MLCod.opcode = 0x39;
                    }
                    else
                    {
                        if ((sgn0 > 0) && (sgn1 == 0))
                        {
                            MLCod.opcode = 0x3a;
                        }
                        else
                        {
                            if ((sgn0 == 0) && (sgn1 > 0))
                            {
                                MLCod.opcode = 0x3b;
                            }
                            else
                            {
                                return bdrglist ();
                            }
                        }
                    }
                }
            }

            INDX_BYT = (rg0 << 4 | rg1);
            return 1;
        }
    }

    return bdrglist ();
}

/* ******************************************************************** *
 * poprand() - Process the operand ref_ent                              *
 * ******************************************************************** */

static int
#ifdef __STDC__
poprnd (int notbyte)
#else
poprnd (notbyte)
     int notbyte;
#endif
{
    if (immediat ())
    {
        ++NumBytes;

        if (notbyte)
        {
            ++NumBytes;
            proc_int ();
#ifdef COCO
            MLCod.opers.C_Int[0] = NmbrInt;
#else
            storInt(&opr_ptr[1], NmbrInt);
#endif
        }
        else    /* Single byte */
        {
            LOC1Msk |= LOC1BYT;
            proc_byte ();
            INDX_BYT = NmbrInt;
        }

        return (not_immed ());
    }
    else
    {
        return (regofst ());
    }
}

int
do_addr (ZILCH)
{
    int rg0,
      rg1;

    NumBytes = 3;  /* Orig code ws 2 - did we do this for 6309 ? */
    
    if ((rg0 = getreg (0)) && (*SrcChar == ','))
    {
        ++SrcChar;
    
        if ( (rg1 = getreg (0)) )
        {
            if (rg1 != 0x0a)
            {
                INDX_BYT = (rg0 << 4 | rg1);
                return 1;
            }
            else
            {
                return (e_report ("storing to CCR"));
            }
        }
    }

    return bdrglist ();
}
#endif

int
swapint(val)
    int val;
{
    return ((val & 0xff00) >> 8) | ((val & 0xff) << 8);
}

int
cc_stuff (ZILCH)
    /* orcc, andcc, & cwai */
{
    NumBytes = 2;

    if (immediat ())
    {
        LOC1Msk |= LOC1BYT;
        proc_byte ();
#ifdef COCO
        MLCod.opers.shrtlng.f_byt = (char)NmbrInt;
#else
        storInt(&opr_ptr[0], NmbrInt);
#endif
        return 1;
    }

    return (ilAdrMod ());
}

int
int_stuf (ZILCH)
    /* add's etc for 16-bit stuff */
{
    if (immediat ())
    {
        NumBytes += 2;
        proc_int ();
#ifdef COCO
        MLCod.opers.shrtlng.C_Int[0] = NmbrInt;
#else
        storInt(opr_ptr, NmbrInt);
#endif
        return (not_immed ());
    }

    return (regofst ());
}

         /* sbc,and, bit, st, eor, adc, etc */
int
chr_stuf (ZILCH)
    /* 8-bit processes */
{
#ifndef DEVEL
    int lngreg = 0;
#endif

    switch (*CmdNamEnd)
    {
#ifndef DEVEL
        case 'd':
            *(--CmdCod) = 0x10;
            ++NumBytes;
            lngreg = 1;
            break;
#endif
        case 'b':
            MLCod.opcode |= 0x40;
        case 'a':
            if (immediat())
            {
                ++NumBytes;
                LOC1Msk |= 8;
                proc_byte();
                INDX_BYT = NmbrInt;
                return not_immed();
            }
            else
            {
                return regofst();
            }
        default:
            return (bd_rgnam ());
    }
#ifndef DEVEL
    return poprnd (lngreg);
#endif
}

int
bit_stuf (ZILCH)
{
    if ((MLCod.opcode != 0x0e)
            && *CmdNamEnd)
    {
        switch (*CmdNamEnd)
        {
            case 'a':
                MLCod.opcode |= 0X40;
                return 1;
            case 'b':
                MLCod.opcode |= 0X50;
                return 1;
            default:
                return (bd_rgnam ());
        }
    }
    else
    {
        if (regofst() == 0)
        {
            return 0;
        }

        if ((MLCod.opcode & 0xf0) == 0)
        {
            return 1;
        }

        MLCod.opcode |= 0X40;
    }
    
    return 1;
}

int
no_opcod (ZILCH)
    /* No opcode, just a null subroutine */
{
    return 1;
}

int
lea_s (ZILCH)
    /* leax, leas, etc. */
{
    return regofst() ? 1 : ilAdrMod();
}

int
tfr_exg (ZILCH)
{
    int rg0,
        rg1;

    NumBytes = 2;
    
    if ( (rg0 = getreg (0)) && (*SrcChar == ',') )
    {
        ++SrcChar;

        if ( (rg1 = getreg (0)) )
        {
            if (((rg0 ^ rg1) & 8) == 0)
            {
                INDX_BYT = (char)(rg0 << 4 | rg1);
                return 1;
            }
            else
            {
                return (e_report ("register size mismatch"));
            }
        }
    }

    return bdrglist ();
}

int
stk_stuf (ZILCH)
    /* Psh.. pul.. on/off stacks */
{
    int var2;

    NumBytes = 2;
    SkipSpac ();
    
    while ((var2 = getreg (1)))
    {
        INDX_BYT |= (char)var2;
        if (*SrcChar != ',')
        {
            return 1;
        }

        ++SrcChar;
    }

    return bdrglist ();
}

/* ******************************************************************** *
 * lb_() - All lb's except "lbra" and "lbsr"                            *
 * ******************************************************************** */

int
lb_ (ZILCH)
    /* Long branches */
{
    NumBytes = 4;

    if ( (Cod_Ent = FindCmd (&(LabelName[1]), S_branch, SBsr_End)) )
    {
        MLCod.opcode = Cod_Ent->op_cod;
        return (brnch_size ());               /* in part4.c */
    }

    return (e_report ("bad mnemonic"));
}

/* ******************************************************** *
 * do_brnch() - Handles short branches                      *
 * ******************************************************** */

int
do_brnch (ZILCH)
{
    int _jmpsize;

    NumBytes = 2;

    if (getNmbr ())
    {
        if (Pass2)
        {
            _jmpsize = (int)NmbrInt - (unsigned) CodeSize - 2;
            LOC1Msk |= RELATIVE | LOC1BYT;
            rmcoderefs ();

            if (D0791 == OptBPtr)
            {
                if ((_jmpsize > 127) || (_jmpsize < -128))
                {
                    _jmpsize = -2;
                    e_report ("branch out of range");
                }
            }
            else
            {
                genreflist ();
            }

/*#ifdef COCO
            MLCod.opers.f_byt = _jmpsize;
#else*/
            INDX_BYT = _jmpsize;
/*#endif*/
        }

        return 1;
    }

    return 0;
}

/* equset(): handles fcc's, fcb's , etc */

int
equset (ZILCH)
{
    NumBytes = 0;
    return (*CnstTbl[(int)(MLCod.opcode)]) ();
}

int
asm_dirct (ZILCH)
    /* setdp, org, etc */
{
    NumBytes = 0;
    return ( (*D03b6[(int)(MLCod.opcode)]) () );
}

/* ************************************************************ *
 * do_ifs() - Handles if statememts.                            *
 * ************************************************************ */

int
do_ifs (ZILCH)
{
    NumBytes = 0;
    ++ifDpth;

    if (_if_tbl[(int)(MLCod.opcode)] != is_pass1)
    {
        if (getNmbr () == 0)
        {
            return 0;
        }

        if (D0791 != OptBPtr)
        {
            return (ilExtRef ());
        }
    }

    if ((*_if_tbl[(int)(MLCod.opcode)]) () == 0)
    {
        ++FalseIf;     /* Inc FalseIf on FALSE */
    }

    doList &= (C_Flg > 0);
    return 1;
}

int
ilExtRef (ZILCH)
{
    return (e_report ("illegal external reference"));
}

/* ******************************************** *
 * do_endc() - Handle "endc" or "else"          *
 *         "endc": reduce ifDpth and FalseIf  *
 *         "else": invert case of FalseIf       *
 *                                              *
 *         Sets doList to proper state          *
 * ******************************************** */

int
do_endc (ZILCH)
{
    NumBytes = 0;

    if (ifDpth)
    {
        if (MLCod.opcode == 0)  /* "endc" */
        {
            --ifDpth;

            if (FalseIf)
            {
                --FalseIf;
            }
        }
        else        /* then it's "else" */
        {
            (FalseIf) ? --FalseIf : ++FalseIf;  /* Invert FalseIf */
        }

        doList &= (C_Flg > 0);
        return 1;
    }

    return (e_report ("conditional nesting error"));
}

int
_psect (ZILCH)
{
    codsetup ();    /* Set up now to allow reading of real commands */
    do_psect ();
    /*CodeSize = 0;*/ /* Not in original*/
    I_dpd = U_dpd = I_data = U_data = 0;
    CodeSize = 0;
    return 1;
}

/* ************************************************************ *
 * codsetup() - Sets up for code data within a psect            *
 * ************************************************************ */

int
codsetup (ZILCH)
{
    NumBytes = 0;
    CodTbBgn = S_realcmds;
    CodTbEnd = SLbrEnd;
    jmp_ptr = codtabl;
    CurLclRef = &LRefs_Code;
    TypeBasic = CODENT;
    DLocField = RELATIVE | CODLOC | DIRENT; /* DIRENT is CONENT?? */
    TmpLocMsk = CODLOC;
    S_Addrs = &CodeSize;
    CurntOrg = 0;
    return 1;
}

/* Sets up to do a csect section */

int
csectsetup (ZILCH)
{
    NumBytes = 0;
    CodTbBgn = S_Rmb;
    CodTbEnd = S_Rmbend;
    jmp_ptr = CsecTbl;
    CurLclRef = 0;
    TypeBasic = CODENT | DIRENT;
    DLocField = (DLocField & DIRENT) | RELATIVE;
    TmpLocMsk = 0;
    S_Addrs = (int *) 0;
    Adrs_Ptr = CurntOrg = &CsectOrg;
    CsectOrg = 0;

    if (SkipSpac ())
    {
        if (getNmbr () == 0)
        {
            return 0;
        }

        if (OptBPtr != D0791)
        {
            return ilExtRef ();
        }

        CsectOrg = NmbrInt & 0xffff;
    }

    return 1;
}

/* Sets up to do a vsect section if OUTSIDE a psect */

int
vsectsetup (ZILCH)
{
    NumBytes = 0;
    CodTbBgn = S_Rmb3;
    CodTbEnd = Rmb3End;
    jmp_ptr = VsecTbl;
    TypeBasic = 0;

    if (is_dp ())
    {
        TypeBasic = DIRENT;
    }

    return 1;
}

int
end_csect (ZILCH)
{
    Adrs_Ptr = CurntOrg;

    if (DLocField & DIRENT)
    {
        return (codsetup ());
    }

    return (precodeset ());
}

               /* sets up a macro definition */

int
setmac (ZILCH)
{
    if (MacDefining)
    {
        return (nstmacdf ());
    }
    else
    {
        if (Label == 0)
        {
            return (e_report ("label missing"));
        }
        else
        {
            l2069 (8);      /* LOC1BYT ?? */

            if (PASS1)
            {
                createMac ();
            }

        }
    }

    return MacDefining = 1;
}

int
endmac (ZILCH)
{
    return (e_report ("ENDM without MACRO"));
}
