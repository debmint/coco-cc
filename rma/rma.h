/* ******************************************************************** *
 * rma.h - Data definitions for rma                                     *
 *                                                                      *
 * $Id::                                                            $   *
 * ******************************************************************** */

#ifdef COCO
#   define const
#   define void int
#   define u_char char
#   define LBLLEN 9
#   define ZILCH    /* To allow void parameter in prototypes */
#define INDX_BYT MLCod.opers.shrtlng.f_byt
#else
#  define direct
#    ifndef u_char
#      define u_char unsigned char
#      define __u_char_defined
#     endif
#   define LBLLEN 9
#   define ZILCH void
#define INDX_BYT opr_ptr[0]
#endif

#ifndef MAIN
#  define GLOBAL extern
#else
#  define GLOBAL
#endif


/* symbol table types */
/* symbol definition/reference type/location */

/* data type flags */

#define CODENT      0x04        /* data/code flag */

/* location flags */

#define DIRENT      0x02        /* global/direct flag */
#define INIENT      0x01        /* clear/init. data flag */
#define CODLOC      0x20        /* data/code flag */
#define DIRLOC      0x10        /* global/direct flag */
#define LOC1BYT     0x08        /* two/one byte size flag */
#define LOCMASK     (CODLOC | DIRLOC)
#define NEGMASK     0x40        /* negate on resolution */
#define RELATIVE    0x80        /* relative reference */

/* Masks in LblTyp */

#define LT_INPSECT 2
#define LT_GLOBAL 4
#define LT_SET 5
#define LT_EQU 6
#define LT_HASRIGHT 8       /* <=LOC1BYT=>*/
#define LT_HASREFS 0x20     /* <=CODLOC=> */
#define LT_CONST 0x40
#define LT_INIT 0x80        /* Flags that the symblstr has been initialized */

#include "cocotype.h"
#include <stdio.h>
#include <string.h>

/* We could use #include <ctype.h> for non-coco systems, but it's too
 * complicated for just getting one macro definition.
 */

#ifdef NEED_CTYPE
#   ifdef DEVEL
#   define isblank(c) ((c == ' '))
#else
#   define isblank(c) ((c == ' ') || (c == '\t'))
#endif
#   define notblank(c) ! isblank(c)
#endif

#include "rstruct.h"
#   include "proto.h"

/*GLOBAL direct char *incdirs[32];
GLOBAL direct int  inccount
#ifdef MAIN
=0
#endif
;*/

GLOBAL direct FILE *SrcFile,    /* D0015 */
 *OutFile,                      /* D0017 */
 *PrtPth;
GLOBAL direct unsigned int NumBytes,
  D001d,
  jsrOfst,
  D0021,
  HadError;
GLOBAL direct unsigned int D0025,
  D0027,
  ifDpth,
  FalseIf;
GLOBAL direct unsigned ListPag,
  ListLin;                      /* D002d, D002f */
    /* NmbrInt is a general catch-all storage for integral #
     * is used to store operand values, count, etc
     */
#ifdef COCO
#ifdef DEVEL
GLOBAL direct int NmbrInt;
#else
GLOBAL direct long NmbrInt;
#endif
#else
GLOBAL int NmbrInt;
#endif
GLOBAL direct short IndxVal;
GLOBAL direct int HadBrkt,
                  HadArrow;     /* -1 if extended, 1 if direct, else 0 */
GLOBAL direct int LongJmp[2];     /*  ???   */
GLOBAL direct int doList,
                  IsExtrn,
                  U_data,
                  U_dpd,
                  I_data,
                  I_dpd;
GLOBAL direct int  CsectOrg,
                  *S_Addrs;
GLOBAL direct int  CodeSize;
GLOBAL direct int *Adrs_Ptr;
GLOBAL direct int  CodeAddrs,
                  *CurntOrg;
GLOBAL direct short GlobCnt,
                    ref_cnt,        /* D0055, D0057 */
                    comn_cnt,
                    locl_cnt;
GLOBAL direct char Pass2,           /* D005d    */
                   TypeBasic,       /* Raw type */
                   WhereLoc,        /* Used as a copy of TypeBasic */
                   DLocField,
                   LblTyp,
                   TmpLocMsk,
                   LOC1Msk,
                  *SrcChar,         /* D0064 originally */
                  *Label,
                  *Operatr,
                  *CmdNamEnd,       /* The final character in the asm cmd */
                  *Oprand,
                  *coment;
GLOBAL
#ifdef COCO
    direct JMPTBL (*jmp_ptr);
#else
    JMPTBL *jmp_ptr;
#endif
GLOBAL direct CODFORM *Cod_Ent;
GLOBAL direct CODFORM *CodTbBgn,
                      *CodTbEnd;
GLOBAL direct struct symblstr **SblTrePtr,
                               *CurntLabel,                      /* D007a */
                               *CurLclRef;
GLOBAL direct struct symblstr   LRefs_Code,
                                LRefs_DPD,
                                LRefs_NonDPdat;
/* delete this.. try to make whole thing a single structure..
 * seems that these aren't inline in linux, at least */
/*GLOBAL direct char prebyt, OpCod;
GLOBAL direct union storer1 C_Oprnd, *opr_ptr;*/
GLOBAL direct struct cmdbytes MLCod;
GLOBAL direct char *opr_ptr;
GLOBAL direct char AIMFlg;
GLOBAL direct int RefExtern;

            /* Command-line option flags */

GLOBAL direct char C_Flg,
                   ff_flg,
                   g_flg,
                   D00c4,
                   ListFlg,
                   listON,
                   NarrowList,
                   OFlg,
#ifdef DEVEL
                   S_Flg;
#else
                   S_Flg,
                   R_Keep;                       /* D00c8, D00c9 */
#endif

GLOBAL direct int list_lin;     /* D00ca */
GLOBAL direct char *CmdCod;
GLOBAL direct int MacDefining,
  DoingMacro,
  ReptCount,
  InRept;

GLOBAL char AltLbl[SYMLEN],
            LabelName[SYMLEN];
GLOBAL char D0587[SYMLEN],
            _nam[120],
            _ttl[120];
GLOBAL char CmdMnemonic[SYMLEN];
GLOBAL long reptFpos;
/* Note: I believe that only 64 elements of SmblTreArr are used !!! */
GLOBAL struct symblstr *SmblTreArr[128],
                       *D078f;
GLOBAL struct ref_ent  D0791[SYMLEN],
                       *OptBPtr;


extern char _chcodes[];
