
/* ******************************************************************** *
 * part 8 - for RMA                                                     $
 * This deals with input/output.. both printing/listing and the rof,    $
 *                          symbol table etc..                          $
 *                                                                      $
 * $Id::                                                                $
 * ******************************************************************** */

#include <time.h>
#include "rma.h"

#ifndef DEVEL
int rversion = 1;
#endif

#define BEGIN 0                 /* fseek "place" */
#define DO_FF putc( 0x0c, PrtPth );
#define DO_CR putc ( '\n', PrtPth );

#ifdef COCO
#	define PUTINT(i,c) fwrite(&i,2,1,c)
#else
#	define PUTINT(i,c) putc( i>>8&0xff,c);putc(i&0xff,c);
#endif

static long CurFPos = 0;

static struct binhead
{                               /* header structure */
#ifdef COCO
    long h_sync;                /* D03e0 */
    _ushort h_tylan;            /* D03e4 */
#else
    char h_sync[4];
    char h_tylan[2];
#endif
    char h_valid,                                      /* D03e6 */
         h_date[5],                                    /* D03e7 */
         h_edit,                                       /* D03ec */
         h_spare;                                      /* D03ed */
#ifdef COCO
    _ushort h_udata,    /* Uninit NDP data size */
            h_udpd,     /* Uninit DP data size  */
            h_idata,    /* Init NDP data size   */
            h_idpd,     /* Init DP data size    */
            h_ocode,    /* Code size            */
            h_stack,    /* Stack size           */
      h_entry;          /* Program entry point  */
} RofHdr = {
    0x62cd2387, 0, 0,
   {0, 0, 0, 0, 0},
    0, 1, 0, 0, 0, 0, 0, 0, 0
};
#else
    char h_udata[2],
         h_udpd[2],
         h_idata[2],
         h_idpd[2],
         h_ocode[2],
         h_stack[2],
         h_entry[2];
} RofHdr =
{
    {'\x62', '\xcd', '\x23', '\x87'},
    { 0, 0},
    0,
    {0, 0, 0, 0, 0},
    0, 1,
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0}
};

#endif

long RofHdLoc, OCodeBegin, O_idpdBegin, O_idatBegin;
#ifdef COCO
struct sgtbuf d_tim;
#else
struct tm d_tim;
#define t_year tm_year
#define t_month tm_mon
#define t_day tm_mday
#define t_hour tm_hour
#define t_minute tm_min
#define t_second tm_sec
#endif

/* FrstFil is a "stack", so to speak, of Files being read.  When
 * a "use" statement is encountered, the file is opened, and the
 * FILE * is appended onto the end of the stack.  The variable
 * "ThisFile" (below) is a pointer to the last file on the stack,
 * actually always points to the next empty slot.  When the use-file
 * is ended, "ThisFile" is decremented back to the previous file pointer
 */

FILE *FrstFil[12];
const char *srcnam[32];
char InpBuf[256];

FILE **ThisFile = FrstFil;  /* Ptr to first empty slot in the use-file stack */
const char **LastSrc = srcnam;   /* Ptr to first empty slot in srcfiles array */
char *O_Nam = 0;
int SColumn = 0;

extern direct int PgDepth, PgWidth;

static void do_reflist (
#ifdef __STDC__
    struct ref_xtrnal *parm
#endif
);

static void PrnTree (
#ifdef __STDC__
    void (function) (struct symblstr * xx)
#endif
);

static int
CountRfs(
#ifdef __STDC__
    struct symblstr *smbls
#endif
);

static int DecSFil ();

static void PrtSmbl (
#ifdef __STDC__
    struct symblstr *
#endif
);

static void wrtxtrn (
#ifdef __STDC__
    struct symblstr *parm1
#endif
);

static void wrt_global (
#ifdef __STDC__
    struct symblstr *parm
#endif
);

static void ck_xtrnl();

/* ******************************************************** *
 * GetOpts - Parse command line for options.                *
 *          This uses the same routine that interprets      *
 *          inline options (read from source file)          *
 * ******************************************************** */

void
#ifdef __STDC__
GetOpts (unsigned _argc, char **_argv)  /* parse parameter line */
#else
GetOpts (_argc, _argv)
     unsigned _argc;
     char **_argv;
#endif
{
    char var;

    SrcFile = OutFile = 0;
    PrtPth = stdout;
    ++_argv;

    while (--_argc)
    {
        if (**_argv == '-')
        {
            SrcChar = *_argv + 1;
            set_opts ();        /* Check to see if SrcChar must be passed */

            if ((OFlg) && ((var = _toupper (SrcChar[-1])) != '\0') &&
                (var == 'O') && (*SrcChar == '='))
            {
                if (O_Nam)
                {
                    errexit ("too many object files");
                }
                else
                {
                    O_Nam = ++SrcChar;
                }
            }

            /*if (_toupper(*SrcChar) == 'I')
            {
                char *p = ++SrcChar;

                if (*p == '=')
                {
                    p++;
                }

                incdirs[inccount++] = p;
            }*/
        }
        else
        {
            if (LastSrc > &srcnam[31])
            {
                errexit ("too many input files");
            }

            *(LastSrc++) = *_argv;
        }

        ++_argv;
    }

    if (LastSrc == srcnam)
    {
        errexit ("no input file");
    }

    if ( ! O_Nam)
    {
        O_Nam = "output.r";
    }
}

FILE *
#ifdef __STDC__
f_opn (const char *fna, char *fmo)
#else
f_opn (fna, fmo)
     char *fna,
      *fmo;
#endif
{
    FILE *fptr;

    if ( ! (fptr = fopen (fna, fmo)) )
    {
        fprintf (stderr, "\"%s\" - ", fna);
        errexit ("can't open file");
    }

    return fptr;
}

#ifndef DEVEL
/* Open a use-file
 * Returns File * to file on success, NULL on failure
 */

FILE *
#ifdef __STDC__
u_opn(char *fna, char *fmo)
#else
u_opn(fna, fmo)
    char *fna,
         *fmo;
#endif
{
     FILE           *fptr;
     char           f[128];
     int            i;

    /* First try the raw entry */

    if ((fptr = fopen(fna, fmo)) != 0)
    {
        return fptr;
    }

    /* Try each include-path to open the use-file
     * If file not found, return NULL
     */

    /*for (i = 0; i < inccount; i++)
    {
        sprintf(f, "%s/%s", incdirs[i], fna);

        if ((fptr = fopen(f, fmo)) != 0)
        {
            return fptr;
        }
    }*/

    fprintf(stderr, "\"%s\" - ", fna);
    errexit("can't open file");
#ifndef COCO
    return 0;  /* Doesn't happen but it does keep gcc quiet */
#endif
}
#endif

/* ******************************************************************** *
 * pagehdr() - If we're not on Pass 2 and not listing, do nothing.      *
 *             If anything has been printed, first eject the page.      *
 *             Print header with current time, set list_line to 2       *
 * ******************************************************************** */

void
pagehdr (ZILCH)
    /*  Prints header for listing */
{
#ifndef COCO
    time_t now;
#endif

    if (Pass2 && doList)
    {
        if (list_lin)
        {
            if (ff_flg)
            {
                DO_FF;
            }
            else
            {
                while (list_lin < PgDepth)
                {
                    DO_CR;
                    ++list_lin;
                }
            }
        }

#ifdef COCO
        getime (&d_tim);
#else
        time (&now);
        localtime_r (&now, &d_tim);
#endif
        printf ("Microware OS-9 %s  %02d/%02d/%02d  %02d:%02d", "RMA - V2.0",
                d_tim.t_month
#ifdef __STDC__
                + 1
#endif
                , d_tim.t_day, d_tim.t_year + 1900,
                 d_tim.t_hour, d_tim.t_minute);

        printf ("   %-22s Page %03d\n", srcnam[0], ++ListPag);
        printf ("%s - %s\n\n", _nam, _ttl);
        list_lin = 3;
    }
}

/* **************************************************************** *
 * newline() - Does a newline to listing.                           *
 *             Ejects page and prints header if needed.             *
 * **************************************************************** */

void
newline (ZILCH)
{
    if (Pass2 && doList)
    {
        if ((PgDepth - 3) < list_lin)
        {
            pagehdr ();
        }

        DO_CR;
        ++list_lin;
    }
}

/* ******************************************************************** *
 * getsrcline() - Retrieve a line from a source.  The source line will  *
 *                come from either the current source file, or the      *
 *                macro file if we're reading a macro.                  *
 * Returns: 1 on successful read of either sourcefile or macro          *
 *          0 on end of sourcefile(s)                                   *
 * ******************************************************************** */

int
getsrcline (ZILCH)
{
    int _curchr;
    register char *_dest;

    /* If we are in a macro, get it from macro file,
     * else get it from sourcefile
     */

    if (getmacline ())
        return 1;

    do
    {
        if (D00c4 && (ThisFile == FrstFil))
        {
            printf ("Asm:");
        }

        _dest = SrcChar = InpBuf;

        /* Read up to newline, EOF, or read error */

        while ((_curchr = getc (SrcFile)) != -1)
        {
            if (_curchr == '\n')
            {
                *_dest = '\0';
                ++ListLin;
                return 1;
            }

            *(_dest++) = _curchr;
        }
    } while (DecSFil ());

    return 0;
}

/* ************************************************************ *
 * DecSFil() - Close current input file, decrements file stack  *
 *             and points SrcFile to previous file in stack     *
 * Returns: 0 when all sourcefiles are exhausted                *
 *          1 otherwise                                         *
 * ************************************************************ */

static int
DecSFil (ZILCH)
{
    if (ThisFile == FrstFil)
    {
        return 0;
    }

    if (fclose (SrcFile))
    {
        e_report ("file close error");
    }

    SrcFile = *(--ThisFile);
    return 1;
}

/* ******************************************** *
 * DoSymTbl() - Print Symbol table to stdout    *
 * ******************************************** */

void
DoSymTbl (ZILCH)
{
    printf ("\nSymbol Table\n");
    PgWidth -= 24;
    PrnTree (PrtSmbl);

    if (SColumn > 0)
    {
        printf ("\n");
    }

    printf ("\n");
}

/* ************************************************************ *
 * PrtSmbl() - Called by PrnTree() to format and print symbol   *
 *             description to stdout for symbol defined in      *
 *             'struct symblstr *' passed in the parameter      *
 * ************************************************************ */

static void
#ifdef __STDC__
PrtSmbl (register struct symblstr *sblptr)
#else
PrtSmbl (sblptr)
     register struct symblstr *sblptr;
#endif
{
    if (SColumn > 0)
    {
        printf ("  |");
    }

    printf ("  %-9s %02x %02x %04x", sblptr->symblnam,
                                     sblptr->smbltyp & 0xff,
                                     (sblptr->locmsk) & 0xff,
                                     sblptr->s_ofst);

    if (((SColumn += 25) > PgWidth) || NarrowList)
    {
        printf ("\n");
        SColumn = 0;
    }
}

/* ******************************************************************** *
 * do_psect() - Interprets the psect line and sets up the ROF header    *
 *              Also, on Pass 2, establishes some of the file positions *
 *              in the output file if there is an output file           *
 *              and writes Global data                                  *
 * ******************************************************************** */

void
do_psect (ZILCH)
{
    char rof_nam[16];
    register char *destptr;
#ifndef COCO
    time_t now;
#endif

    if (*SrcChar != '\0')
    {
        int _namlen;
        destptr = rof_nam;
        _namlen = 16;

        /* move psect name to local rof_nam */

        while ((_namlen--) && (*SrcChar != '\0') && (*SrcChar != ','))
        {
            *(destptr++) = *(SrcChar++);
        }

        *destptr = '\0';

        /* Type */

        if (iscomma ())
        {
            getNmbr ();
            ck_xtrnl ();
#ifdef COCO
            RofHdr.h_tylan = NmbrInt << 8;
#else
            RofHdr.h_tylan[0] = NmbrInt & 0xff;
#endif
            /* Language */

            if (iscomma ())
            {
                getNmbr ();
                ck_xtrnl ();
#ifdef COCO
                RofHdr.h_tylan |= NmbrInt & 0xff;
#else
                RofHdr.h_tylan[1] = NmbrInt & 0xff;
#endif
                /* Edition */

                if (iscomma ())
                {
                    getNmbr ();
                    ck_xtrnl ();
                    RofHdr.h_edit = NmbrInt & 0xff;

                    /* Stack size */

                    if (iscomma ())
                    {
                        getNmbr ();
                        ck_xtrnl ();
#ifdef COCO
                        RofHdr.h_stack = NmbrInt;
#else
                        storInt (RofHdr.h_stack, NmbrInt);
#endif
                        /* Code entry address */

                        if (iscomma ())
                        {
                            getNmbr ();
                            ck_xtrnl ();
#ifdef COCO
                            RofHdr.h_entry = NmbrInt;
#else
                            storInt (RofHdr.h_entry, NmbrInt);
#endif
                        }
                    }
                }
            }
        }
    }
    else
    {
        strcpy (rof_nam, "program");
    }

#ifdef COCO
    getime (&d_tim);
    _strass (RofHdr.h_date, &d_tim, 5);
#else
    time (&now);
    localtime_r (&now, &d_tim);
    RofHdr.h_date[0] = (d_tim.tm_year) & 0xff;
    RofHdr.h_date[1] = (d_tim.tm_mon) & 0xff;
    RofHdr.h_date[2] = (d_tim.tm_mday) & 0xff;
    RofHdr.h_date[3] = (d_tim.tm_hour) & 0xff;
    RofHdr.h_date[4] = (d_tim.tm_min) & 0xff;
#endif

    RofHdr.h_valid = HadError;
#ifndef DEVEL
    RofHdr.h_spare = rversion;
#endif

#ifdef COCO
    RofHdr.h_udata = U_data;
    RofHdr.h_udpd = U_dpd;
    RofHdr.h_idata = I_data;
    RofHdr.h_idpd = I_dpd;
    RofHdr.h_ocode = CodeSize;
#else
    storInt (RofHdr.h_udata, U_data);
    storInt (RofHdr.h_udpd, U_dpd);
    storInt (RofHdr.h_idata, I_data);
    storInt (RofHdr.h_idpd, I_dpd);
    storInt (RofHdr.h_ocode, CodeSize);
#endif

    /* If applicable:
     * Write ROF header and write Global data info
     * Initialize file positions for begin of code, idpd, and idat
     */

    if (Pass2 && OutFile)
    {
        RofHdLoc = ftell (OutFile);     /*  ?? OutFile assumed */
        fwrite (&RofHdr, sizeof (RofHdr), 1, OutFile);
        fputs (rof_nam, OutFile);
        putc ('\0', OutFile);
        /*fwrite( &GlobCnt, 2, 1, OutFile ); */
        PUTINT (GlobCnt, OutFile);

        if (GlobCnt)
        {
            PrnTree (wrt_global);
        }

        O_idatBegin = (O_idpdBegin = (OCodeBegin = CurFPos = ftell (OutFile)) +
                 (long) (unsigned)CodeSize) + (long) (unsigned)I_dpd;
    }
}

static void
ck_xtrnl (ZILCH)
{
    if (Pass2 && RefExtern)
    {
        e_report ("no external allowed");
    }
}


void
wrtvalid (ZILCH)                 /* Write Valid into ROF header */
{
    unsigned hd_sz = (unsigned) (&(RofHdr.h_valid)) - (unsigned) (&RofHdr);

    if ((OutFile) && (OFlg > 0))
    {
        fseek (OutFile, (RofHdLoc + (long) hd_sz), BEGIN);
        putc (HadError, OutFile);
        CurFPos = RofHdLoc + (long) hd_sz + 1;
    }
}


void
#ifdef __STDC__
w_ocod (char *_codaddr, int _codlen)
#else
w_ocod (_codaddr, _codlen)
     char *_codaddr;
     int _codlen;
#endif
{
    register long *_filpos;
#ifndef COCO
    char *p;
#endif

    if (S_Addrs && _codlen)
    {
        *S_Addrs += _codlen;

        if ( (Pass2) && (DLocField & DIRENT) && (OutFile) && (OFlg > 0) )
        {
            if (WhereLoc & CODENT)
            {
                _filpos = &OCodeBegin;
            }
            else
            {
                if (WhereLoc & DIRENT) 
                {
                    _filpos = &O_idpdBegin;
                }
                else
                {               /* INIENT (clear/init data) */
                    _filpos = &O_idatBegin;
                }
            }

            if (CurFPos != *_filpos)
            {
                fseek (OutFile, *_filpos, BEGIN);
            }
/*#ifdef COCO*/
            fwrite (_codaddr, _codlen, 1, OutFile);
/*#else
            p = &(_codaddr[_codlen - 1]);
            while ( p >= _codaddr)
            {
                fputc(*(p--), OutFile);
            }
#endif*/
            CurFPos = *_filpos = *_filpos + (long) _codlen;
        }
    }
}

static void
#ifdef __STDC__
PrnTree (void (function) (struct symblstr * xx))
#else
PrnTree (function)
     int (*function) ();
#endif
{
    register struct symblstr *refrnc;
    int count;

    count = 0;

    while (count < 64)
    {
        if ( (refrnc = SmblTreArr[count]) )
        {
            while (refrnc->left)    /* Go to leftmost entry in list */
            {
                refrnc = refrnc->left;
            }

            do
            {
                (*function) (refrnc);
            } while ( (refrnc = WlkTreLft (refrnc)) );
        }

        ++count;
    }
}

/* **************************************************************** *
 * wrt_refs() - Write all external, local, dp, non-dp refs          *
 *              and common symbols if applicable                    *
 * **************************************************************** */

void
wrt_refs (ZILCH)
{
    if (OutFile)
    {
        fseek (OutFile, O_idatBegin, BEGIN);

        /* Write external references */

        PUTINT (ref_cnt, OutFile);

        if (ref_cnt)
        {
            PrnTree (wrtxtrn);
        }

        /* Write local count */

        PUTINT (locl_cnt, OutFile);

        if (locl_cnt)                   /* Write locals, if any */
        {
            if (LRefs_Code.reftbl)              /* refs to code */
            {
                do_reflist (LRefs_Code.reftbl);
            }

            if (LRefs_DPD.reftbl)              /* refs to dp data */
            {
                do_reflist (LRefs_DPD.reftbl);
            }

            if (LRefs_NonDPdat.reftbl)              /* refs to non-dp data */
            {
                do_reflist (LRefs_NonDPdat.reftbl);
            }
        }

        /* Write commons if rof version > 0 */

#ifdef DEVEL
        PUTINT (comn_cnt, OutFile);

        if (comn_cnt)
        {
            PrnTree (wrtCmmns);
        }
#else
        if (rversion)
        {
            /*fwrite( &comn_cnt, 2, 1, OutFile ); */
            PUTINT (comn_cnt, OutFile);

            if (comn_cnt)
            {
                PrnTree (wrtCmmns);
            }
        }
#endif
    }
}

/* ************************************************************ *
 * wrt_global() - PrnTree function.                             *
 *                Writes an entry contained in the symblstr     *
 *                passed as a parameter, into the ROF file      *
 * ************************************************************ */

static void
#ifdef __STDC__
wrt_global (register struct symblstr *parm)
#else
wrt_global (parm)
     register struct symblstr *parm;
#endif
{
    /*struct symblstr *reg = parm;*/

    short sbloffst;
    char styp;

    /* if bit for 0x40 (NEGMASK) NOT SET */

    if ( (parm->locmsk & 0xc6) == (RELATIVE | CODENT | DIRENT) &&
         (parm->smbltyp != (CODENT | DIRENT | INIENT)) )
    {
        fputs (parm->symblnam, OutFile);
        putc ('\0', OutFile);           /* Null-termiat name */

        /* write symbol type (8-bit), offset (16-bit) */

        styp = parm->smbltyp;
        sbloffst = parm->s_ofst;
#ifdef COCO
        fwrite( &styp, 3, 1, OutFile );
#else
        putc (styp, OutFile);
        PUTINT (sbloffst, OutFile);
#endif
    }
}

static void
wrtxtrn 
#ifdef __STDC__
    (register struct symblstr *parm1)
#else
    (parm1)
     register struct symblstr *parm1;
#endif
{
    short numrefs;

    if (!(parm1->locmsk & (NEGMASK | DIRENT))
         &&   (parm1->reftbl)
         &&   ((numrefs = parm1->smbltyp & 0x0f) != (CODENT | DIRENT))
         &&   (numrefs != (CODENT | INIENT)))
    {
        /* Write null-terminated symbol name */
        fputs (parm1->symblnam, OutFile);
        putc ('\0', OutFile);

        numrefs = CountRfs (parm1);   /* Ref count */
        PUTINT (numrefs, OutFile);

        /* List all references to this name */

        do_reflist (parm1->reftbl);
    }
}

/* ******************************************************** *
 * CountRfs() : counts references and apparently reverses   *
 *              the direction ->NxtRef points in the line   *
 * Used for externals and commons                           *
 * ******************************************************** */


static int
CountRfs
#ifdef __STDC__
    (struct symblstr *smbls)
#else
    (smbls)
    struct symblstr *smbls;
#endif
{
    register struct ref_xtrnal *_curnt;
    struct ref_xtrnal *next;
    int _refcount;

    _refcount = 0;
    next = smbls->reftbl;          /* init next */
    smbls->reftbl = 0;

    while ( (_curnt = next) )
    {
        next = _curnt->NxtRef;
        _curnt->NxtRef = smbls->reftbl;     /* Point NxtRef to prev ref    */
        smbls->reftbl = _curnt;             /* point smbls->reftbl to last */
        ++_refcount;
    }

    return _refcount;
}

/* ******************************************************************** *
 * do_reflist() - Writes the whole list of references passed in the     *
 *                parameter                                             *
 * ******************************************************************** */

static void
#ifdef __STDC__
do_reflist (register struct ref_xtrnal *parm)
#else
do_reflist (parm)
     register struct ref_xtrnal *parm;
#endif
{
    while (parm)
    {
#   ifdef COCO
        fwrite (parm, 3, 1, OutFile);
#   else
        putc (parm->RfTyp, OutFile);
        PUTINT ((int) parm->r_offset, OutFile);
#   endif
        parm = parm->NxtRef;
    }
}

void
#ifdef __STDC__
wrtCmmns (register struct symblstr *parm)
#else
wrtCmmns (parm)
     register struct symblstr *parm;
#endif
{
    int var;

    /* DIRENT && CODENT && RELATIVE && ! NEGMASK */

    if (((parm->locmsk & 0xc6) == (RELATIVE | CODENT | DIRENT)) &&
         ((parm->smbltyp & 0x0f) == 7))    /* Code for Common */
    {
        fputs (parm->symblnam, OutFile);
        putc ('\0', OutFile);

        PUTINT (parm->s_ofst, OutFile); /* Address of common  */

        var = CountRfs (parm);          /* Count refs to this */
        PUTINT (var, OutFile);

        do_reflist (parm->reftbl);      /* Write all refs     */
    }
}
