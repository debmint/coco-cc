/* ************************************************************ *
 * Part 4 of "rma" assember                                     *
 *                                                              *
 * $Id::                                                    $   *
 * ************************************************************ */

#include "rma.h"

#ifdef COCO
char skparrow ();
#endif

#ifndef DEVEL
#define AIM_INDXD if (AIMFlg) {opr_ptr[0] |= 0x60;}
#else
#define AIM_INDXD
#endif

static int ckpcr (ZILCH);
static int l11be (ZILCH);
static int indx_setup ();
static int not_bracket ();
static int not_pcr ();
static int l11e8 ();
static int l1215 ();
static int l149b ();
static int l1584 ();
static int cod_codref();

struct regstrct
{
    char regnam[2],
      RType;
};

struct regstrct RegCode[] = {
    {{'A', '\0'}, 0x02}, {{'B', '\0'}, 0x04}, {{'C', 'C'}, 0x01},
    {{'D', 'P'}, 0x08},
#ifndef DEVEL
    {{'\xfe', '\0'}, 0}, {{'\xfe', '\0'}, 0},
    {{'E', '\0'}, 0x00}, {{'F', '\0'}, 0x00},
#endif
    {{'D', '\0'}, 0x06}, {{'X', '\0'}, 0x10}, {{'Y', '\0'}, 0x20},
    {{'U', '\0'}, 0x40}, {{'S', '\0'}, 0x40}, {{'P', 'C'}, 0x80}
#ifndef DEVEL
    ,  {{'W', '\0'}, 0x00}, {{'V', '\0'}, 0x00}
#endif
};
#ifndef DEVEL
direct char Is_W;        /* Flag that we are indexing off W  */
#endif

         /* Count of entries in "regstrct" array (6309) */
#define REGCOUNT sizeof(RegCode)/sizeof(RegCode[0])

int
immediat (ZILCH)
{
    if (SkipSpac () == '#')
    {
        ++SrcChar;
        return 1;
    }

    return 0;
}

int
not_immed (ZILCH)
{
    if (jsrOfst & NO_IMMED)
    {
        NumBytes = 3;
        return (ilAdrMod ());
    }

    return 1;
}

int
brnch_size (ZILCH)
{
    LOC1Msk |= RELATIVE;

    if (proc_int () == 0)
        return 0;

#ifdef COCO
    if (((MLCod.opers.C_Int[0] = (int)NmbrInt - CodeSize - NumBytes) > 127)
        || MLCod.opers.C_Int[0] < -128)
    {
        LongJmp[0] = 1;
    }
    else
    {
        LongJmp[0] = 0;
    }
#else
    storInt(opr_ptr, NmbrInt - CodeSize - NumBytes);
    LongJmp[0] = ((NmbrInt > 127) || (NmbrInt < -128));
#endif

    return 1;

}

/* ************************************************************************ *
 * getreg(PhsPul) = l102b() : Parses the Register name structure to match   *
 *              up  register bit patterns                                   *
 * Returns: if not PshPul                                                   *
 *                case not register (X,Y,U,S,PC), offset into table | 8     *
 *                case register, offset-4 | $100                            *
 *          if PshPul : push/pull code for register (0 if invalid)          *
 * ************************************************************************ */
#ifdef DEVEL
#   define RGADJ 4
#else
#   define RGADJ 8
#endif

int
getreg (PshPul)
     int PshPul;
{
    int regptr;
    char var2;

    regptr = 0;

    while (regptr < REGCOUNT)
    {
        if (RegCode[regptr].regnam[0] == _toupper (*SrcChar))
        {
            /* First char matched */
            if ((var2 = RegCode[regptr].regnam[1]))
            {
                /* second char not null, check if it matches */
                if (_toupper (SrcChar[1]) == var2)
                {
                    ++SrcChar;  /* bump ptr for extra char */
                }
                else
                {
                    ++regptr;   /* Bump regptr and get next */
                    continue;
                }
            }

            ++SrcChar;  /* L0185 */

            if (PshPul == 0)
            {
                if (regptr < RGADJ)
                {               /* Not register (X,Y,U,S,PC) */
                    return (regptr | RGADJ);
                }
                else
                {
                    return (regptr - RGADJ) | 0x0100;
                }
            }

#ifndef DEVEL

            if ((regptr == 4) || (regptr == 5))
            {
                return 0;       /* Blank areas */
            }
#endif

            /* No psh/pul for E,F,W or V
               not needed?? RType returns correct value??  */
/*         if ( (regptr!=6) && (regptr!=7) && (regptr < 14) ) {*/
            return (RegCode[regptr].RType);     /*L10a5 */
/*         }
         else {
            return 0;
         }*/
        }

        ++regptr; /*L10ba */              /* bump to next positin */
    }

    return 0;                   /* No match found */
}

/* ******************************************************************** *
 * regofst() - Most code-parse functions, if they are not immediate     *
 *             fall thorough to this function here.                     *
 * ******************************************************************** */

int
regofst (ZILCH)      /*              l10d1()                 */
{                    /* Parses indexed mode for A/B/D offset */
    char c;

    if ( (HadBrkt = ((c = SkipSpac ()) == '[')) )
    {
        c = *(++SrcChar);
    }

    if ((c = skparrow (c)) == ',')      /* ",R" */
    {
        AIM_INDXD;
        return l1215 ();
    }

    if (SrcChar[1] == ',')
    {
        switch (_toupper (c))
        {
#ifdef DEVEL
            case 'A':
                INDX_BYT = 0x86;
                return ckpcr();
            case 'B':
                INDX_BYT = 0x85;
                return ckpcr();
            case 'D':
                INDX_BYT = 0x8b;
                return ckpcr();
#else
            case 'A':
                INDX_BYT = 0x86;
                AIM_INDXD;
                return ((_toupper (SrcChar[2]) == 'W') ? ilAdrMod() : ckpcr());
            case 'B':
                INDX_BYT = 0x85;
                AIM_INDXD;
                return ((_toupper (SrcChar[2]) == 'W') ? ilAdrMod() : ckpcr());
            case 'D':
                INDX_BYT = 0x8b;
                AIM_INDXD;
                return ((_toupper (SrcChar[2]) == 'W') ? ilAdrMod() : ckpcr());
            case 'E':
                INDX_BYT = 0x87;
                AIM_INDXD;
                return ((_toupper (SrcChar[2]) == 'W') ? ilAdrMod() : ckpcr());
            case 'F':
                INDX_BYT = 0x8a;
                AIM_INDXD;
                return ((_toupper (SrcChar[2]) == 'W') ? ilAdrMod() : ckpcr());
            case 'W':
                INDX_BYT = 0x8e;
                AIM_INDXD;
                return ((_toupper (SrcChar[2]) == 'W') ? ilAdrMod() : ckpcr());
#endif
        }
    }

    getNmbr ();
    IndxVal = NmbrInt;

    if (*SrcChar == ',')
    {
        AIM_INDXD;
        return l156b ();
    }

    /* if  no '[' && dp referencing */

    if ((HadBrkt == 0) && (HadArrow >= 0))
    {
        if ((HadArrow > 0) || ((IndxVal >> 8) == D0027))
        {
            /* if dp referencing || == DP_blk */
            return l11e8 ();
        }
    }
#ifndef DEVEL
      if ( AIMFlg ) {
     MLCod.opers[0] |= 0x70;
  }
#endif

    return l11be ();
}

/* **************************************************************** *
 * skparrow() - If we are at an arrow ">" or "<", set HadArrow and  *
 *              bump SrcChar to next character, and return that     *
 *              char.  If no arrow, return char passed as a param.  *
 * HadArrow set to :  -1 = extended, 1 = direct, 0 otherwise        *
 * **************************************************************** */

char
skparrow (ch)
     char ch;
{
    if (ch == '>')
    {
        HadArrow = -1;
    }
    else
    {
        if (ch == '<')
        {
            HadArrow = 1;
        }
        else
        {
            HadArrow = 0;
        }
    }

    return HadArrow ? *(++SrcChar) : ch;
}

static int
ckpcr (ZILCH)
{
    SrcChar += 2;

    return ( not_pcr () ? indx_setup () : ilIdxReg () );
}

static int
l11be (ZILCH)
{
    NumBytes += 2;

    if (HadBrkt)
    {
#ifdef COCO
        MLCod.opers.shrtlng.f_byt = 0x9f;
        MLCod.opers.shrtlng.f_int = IndxVal;
#else
        INDX_BYT = 0x9f;        /* Let "C_Oprnd" be struct ref_ent */
        storInt (&(opr_ptr[1]), IndxVal);
#endif
        return (indx_setup ());
    }
    else
    {
        rmcoderefs ();
        genreflist ();
#ifdef COCO
        MLCod.opers.C_Int[0] = IndxVal;
#else
        /*opr_ptr->C_Int[0] = IndxVal;*/
        storInt (opr_ptr, IndxVal);
#endif
        MLCod.opcode |= 0x30;
    }

    return 1;
}

static int
l11e8 ()
{
    ++NumBytes;
    LOC1Msk |= LOC1BYT;
    rmcoderefs ();
    genreflist ();
    INDX_BYT = IndxVal;

    if (MLCod.opcode & 0xf0)
    {
        MLCod.opcode |= 0x10;
    }

    return 1;
}

/* **************************************************************** *
 * l1215() - Handles ,R indexing                                    *
 * **************************************************************** */

static int
l1215 ()
{
    ++SrcChar;
    IndxVal = 0;

    /* Predecrement */

    if (*SrcChar == '-')
    {
        ++SrcChar;

        if (*SrcChar == '-')
        {
            ++SrcChar;
            INDX_BYT = 0x83;
        }
        else
        {
            not_bracket ();
            INDX_BYT = 0x82;
        }

        /* We're at register name now, get mode if legal */

        return not_pcr() ? indx_setup () : ilIdxReg();
    }

    /* L125e */
    if (not_pcr () == 0)    /* If it's PC, handle it and return */
    {
        return pc_rel ();
    }

    if (*SrcChar == '+')
    {
        ++SrcChar;

        if (*SrcChar == '+')
        {
            ++SrcChar;

#ifdef DEVEL
            MLCod.opers.shrtlng.f_byt |= 0x81;
#else
            if ( ! Is_W)
            {
                INDX_BYT |= 0x81;   /*   ,R++ */
            }
            else
                INDX_BYT |= 0x40;
#endif
        }
        else
        {
#ifndef DEVEL
            if (Is_W)       /* 6309 specific */
            {
                return 0;
            }
#endif
            not_bracket ();
            INDX_BYT |= 0x80;       /*   ,R+  */
        }

        return (indx_setup ());
    }
            /* It's ,R without inc/dec */
    return (l1584 ());

    /* Was: return 0, I suspect this was causing clr ,-s and others to be incorrect - BGP 
    return 1;*/
}

/* ************************************************************ *
 * not_bracket() - Returns 1 if not in brackets.                *
 *                 Returns 0 if in brackets after reporting err *
 * ************************************************************ */

static int
not_bracket ()
{
    return ( HadBrkt ? ilAdrMod () : 1 );
}

int
ilAdrMod ()
{
    return (e_report ("illegal addressing mode"));
}

/* ******************************************************** *
 * not_pcr() - Handle setup for all registers except PC     *
 * ******************************************************** */

static int
not_pcr ()                      /* process 0,[reg] */
{
    char _indx_msk = -1;

    switch (_toupper (*SrcChar))
    {
        case 'X':
            _indx_msk = '\0';
            break;
        case 'Y':
            _indx_msk = '\x20';
            break;
        case 'U':
            _indx_msk = '\x40';
            break;
        case 'S':
            _indx_msk = '\x60';
            break;
#ifndef DEVEL
        case 'W':              /* do most of processing here */
            Is_W = TRUE;

            /* ,-W  not allowed */
            if ((INDX_BYT == '\x82'))
            {
                return 0;       /* let fall through to error condition */
            }

            if (INDX_BYT == '\x83')
            {                   /* if ,--W       */
                INDX_BYT = '\xef';
            }
            else
            {
                INDX_BYT = '\x8f';

                if (HadArrow)
                {
                    HadArrow = -1;  /* Force extended addressing if spec'd. */
                }
/*               if ( SrcChar[1] == '+' ) {
                  if ( SrcChar[2] == '+' ) {
                     SrcChar += 2;
                     INDX_BYT |= 0x40;
                  }
                  else {                   / * If ,W+, let caller fall 
                     return 0;                  / *    to error   
                  }
               }
               else {
                  if ( IndxVal || HadArrow<0 ) {
                     INDX_BYT |= 0x01;
                     NumBytes += 2;
#ifdef COCO
                     opr_ptr->shrtlng.f_int = IndxVal;
#else
                     nptr = &IndxVal;
                     nptr += sizeof(IndxVal)-2;   / * int part
                     opr_ptr[1] = *(nptr++);
                     opr_ptr[2] = *nptr;
#endif
              / * force extended addressing, no other offset modes allowed 
                     HadArrow = -1;
                  }
               }*/
            }

            ++SrcChar;
/*         if (HadBrkt) {
            if ( *(SrcChar) != ']' ) {
               return( e_report( "bracket missing" ) );
            }
            INDX_BYT ^= 0x1f;
            ++SrcChar;
            HadBrkt = 0;
         }*/
            return 1;
#endif
        /*default:
            return 0;*/
    }

    /* Cleanup for an X-Y-U-S match */
    if (_indx_msk != -1)
    {
        ++SrcChar;
        INDX_BYT |= _indx_msk;
        return 1;
    }

    return 0;
}

/* This was the way to get the original compilation for the above
   function (to be used after the switch, but it would not work
   without the "default" above.  For some extremely strange reason,
   the below method would always return 1, even though the assembly
   code matched the original WORD FOR WORD!!!  The above method produces
   the correct result.

   if ( var != '\xff' ) {
      ++SrcChar;
      INDX_BYT |= var;
      return 1;
   }
   else {
      return 0;
   }
*/

int
ilIdxReg ()
{
    return (e_report ("illegal index register"));
}

static int
indx_setup ()
{
    MLCod.opcode |= 0x20;
    ++NumBytes;
    ++D0021;

    if (HadBrkt)
    {
#ifdef DEVEL
        MLCod.opers.shrtlng.f_byt |= 0x10;
#else
        if ( ! Is_W)
            INDX_BYT |= 0x10;
        else
            INDX_BYT ^= 0x1f;
#endif
        if (*SrcChar != ']')
        {
            e_report ("bracket missing");
        }
        else
        {
            ++SrcChar;
        }
    }

    switch (*SrcChar)
    {
        case ' ':
        case '\t':
        case '\0':
            rmcoderefs ();
            genreflist ();
            return 1;
        default:
            return (ilAdrMod ());
    }
}

/* ******************************************************************** *
 * genreflist() - Generates reference structs for refs contained in     *
 *                D0791 and cleans out the list in D0791                *
 * ******************************************************************** */

void
genreflist ()
{
    int _refdest;
    int rtyp;

    if (Pass2 && (OptBPtr > D0791))
    {
        if (S_Addrs == 0)
        {
            ilExtRef ();        /* report "illegal external reference" */
            /* return; */
        }
        else
        {
            _refdest = *S_Addrs + NumBytes - ((LOC1Msk & LOC1BYT) ? 1 : 2);

            /* Generate list of all refs from OptBPtr downward to D0791 */

            while (OptBPtr > D0791)
            {
                register struct symblstr *__lbl;

                __lbl = (--OptBPtr)->RAddr;
                rtyp = __lbl->smbltyp | LOC1Msk | OptBPtr->ETyp;

                if ((__lbl->locmsk & (NEGMASK | DIRENT)) == 0)
                {
                    RefExtern = 1;

                    if (__lbl->reftbl == 0)
                    {
                        ++ref_cnt;
                    }

                    RefCreat (__lbl, rtyp, _refdest);
                }
                else
                {
                    if ((__lbl->smbltyp & 0x0f) == 7)       /* common ? */
                    {
                        RefCreat (__lbl, rtyp, _refdest);
                    }
                    else        /* It's a local ref */
                    {
                        ++locl_cnt;
                        RefCreat (CurLclRef, rtyp, _refdest);
                    }
                }
            }   /* end while (OptBPtr > D0791) */
        }
    }
}

/* **************************************************************** *
 * rmcoderefs() - Deletes code-code references, and those that are  *
 *           refs to symbols with opposite NEGMASK.  (On Pass 2)    *
 * **************************************************************** */

int
rmcoderefs ()
{
    struct ref_ent *_delref;

    if (Pass2 && (OptBPtr > D0791))
    {
        register struct ref_ent *_curntref;

        _curntref = D0791;

        while (_curntref < OptBPtr)
        {
            /* If _curntref and an opposite NEGMASK not deleted */

            if (l149b (_curntref) == 0)
            {
                /* If it's a code-code ref, delete the ref */

                if (cod_codref (_curntref->RAddr))
                {
                    _delref = _curntref;

                    /* Move current ref_ent back one slot */

                    while (++_delref < OptBPtr)
                    {
#ifdef COCO
                        (_delref - 1)->ETyp = _delref->ETyp;
                        (_delref - 1)->RAddr = _delref->RAddr;
#else
                        memcpy (_delref - 1, _delref, sizeof (struct ref_ent));
#endif
                    }

                    --OptBPtr;

                    /* If this happens, an unchanged _curntref will now be
                     * pointing to the next ref, so no bump
                     */
                }
                else
                {
                    ++_curntref;
                }
            }   /* end "if ( ! l149b() )" */
        }   /* end "while (_curntref < OptBPtr)" */
    }

    return 0;
}

/* ************************************************************************ *
 * l149b() - Only happens if reference of "baseref" is to CODE.             *
 *           Beginning at entry point in D0971 passed as "baseref", parses  *
 *           through remaining segment of D0971 for a ref_ent, in CODE      *
 *           section with ETyp having NEGMASK opposite that of "baseref"s   *
 *           "symbltyp".  If found, "baseref" and that entry in D0971 is    *
 *           deleted, and OptBPtr reduced by one, and function exits.       *
 * ************************************************************************ */

static int
#ifdef __STDC__
l149b (struct ref_ent *baseref)
#else
l149b (baseref)
     register struct ref_ent *baseref;
#endif
{
    struct ref_ent *_curref;
    struct symblstr *_smblstr;
    int _basetyp;

    if ((_smblstr = baseref->RAddr)->locmsk & DIRENT)
    {
        _basetyp = _smblstr->smbltyp | baseref->ETyp;
        _curref = baseref;

        /* Begin parse at baseref +  1 */

        while (++_curref < OptBPtr)
        {
            /*_smblstr = _curref->RAddr;*/

            /* Must be inside "psect" */

            /*if (_smblstr->locmsk & DIRENT)*/
            if ((_smblstr = _curref->RAddr)->locmsk & DIRENT)
            {
                if (((_smblstr->smbltyp | _curref->ETyp) ^ _basetyp) == NEGMASK)
                {
                    /* Delete "baseref" and Move all ref_ent's from
                     * "baseref" + 1 to _ref down one slot, up to
                     * the matching ref/
                     */

                    while (baseref + 1 < _curref)
                    {
#ifdef COCO
                        baseref->ETyp = (baseref + 1)->ETyp;
                        baseref->RAddr = (baseref + 1)->RAddr;
#else
                        memcpy (baseref, baseref + 1, sizeof (struct ref_ent));
#endif
                        ++baseref;
                    }

                    /* Now move remainder of D0971 down 2,
                     * overwriting the match found
                     */

                    while (++_curref < OptBPtr)
                    {
                        baseref->ETyp = _curref->ETyp;
                        baseref->RAddr = _curref->RAddr;
                        ++baseref;
                    }

                    OptBPtr = baseref;      /* OptBPtr -= 1 */
                    return 1;
                }
            }
        }
    }

    return 0;
}

/* ******************************************************************** *
 * cod_codref() - An is-<something>                                     *
 *           Is in code and ref to code ?                               *
 * ******************************************************************** */

static int
#ifdef __STDC__
cod_codref (struct symblstr *parm)
#else
cod_codref (parm)
     register struct symblstr *parm;
#endif
{   /* not DIRLOC ? */
    /*if ( ((parm->smbltyp & 0x0f) == CODENT) &&
         (parm->locmsk & DIRENT)            &&
         ((LOC1Msk & (RELATIVE | LOCMASK)) == (RELATIVE | CODLOC)) )
    {
        return 1;
    }

    return 0;*/
    return ((parm->smbltyp & 0x0f) == CODENT) &&
         (parm->locmsk & DIRENT)            &&
         ((LOC1Msk & (RELATIVE | LOCMASK)) == (RELATIVE | CODLOC));
}

int
l156b ()                        /* THIS FUNCTION IS OK */
{
    ++SrcChar;
    return !not_pcr() ? pc_rel() : l1584();
}

/* **************************************************************** *
 * l1584() - Handle mmnn,R modes                                    *
 * **************************************************************** */

static int
l1584 ()
{
    if (  (HadArrow < 0)    ||
          ((HadArrow == 0) && ((IndxVal > 127) || (IndxVal < -128)))
#ifndef COMPARE
           /* All assemblers I've seen use 16-bit mode only for extended  */
        /*|| (Is_W && IndxVal)*/
#endif
       )
    {       /* extended (16-bit) mode */
        /*if (Is_W && (IndxVal > 0))
        {
        }*/

#ifdef COCO
        MLCod.opers.shrtlng.f_int = IndxVal;
#else
        storInt (&(opr_ptr[1]), IndxVal);
#endif
        NumBytes += 2;
#ifdef DEVEL
        MLCod.opers.shrtlng.f_byt |= 0x89;
#else
        if ( ! Is_W)
            INDX_BYT |= 0x89;   /*      mmnn,R     */
        else
            INDX_BYT |= 0x20;
#endif
        return (indx_setup ());
    }

    if ( ! HadArrow && (IndxVal == 0))
    {
#ifdef DEVEL
        MLCod.opers.shrtlng.f_byt |= 0x84;
#else
        if ( ! Is_W)
            INDX_BYT |= 0x84;   /*  set  0,R  to  ,R   */
        else
            INDX_BYT &= 0x9f;
#endif
        return (indx_setup ());
    }
    else
    {       /* 8-bit offset */

        if ( (HadArrow > 0)     ||
              HadBrkt           ||
              (IndxVal > 0x0f)  ||
              (IndxVal < -(0x10))   )
        {
#ifdef COCO
            MLCod.opers.chrs[1] = (char)IndxVal;
#else
            opr_ptr[1] = IndxVal;
#endif
            ++NumBytes;
            LOC1Msk |= LOC1BYT;
            INDX_BYT |= 0x88;
            return (indx_setup ());
        }
        else        /* 5-bit mode */
        {
            INDX_BYT |= (IndxVal & 0x1f);
            return (indx_setup ());
        }
    }
}

/* *********************************
 * Sets up pc-relative addressing
 */

int
pc_rel ()                       /* originally  l1602() */
{
    if ((_toupper (*SrcChar) != 'P') || (_toupper (SrcChar[1]) != 'C'))
    {
        return (ilIdxReg ());   /* No match, quit */
    }

    SrcChar += 2;               /* bump past "PC" string */

    if (_toupper (*SrcChar) == 'R')      /* if "pcr" instead of "pc" */
    {
        ++SrcChar;
    }

    ++NumBytes;
    LOC1Msk |= RELATIVE;
    IndxVal -= (CodeSize + NumBytes + 1);

    if (HadArrow > 0)
    {
#ifdef COCO
        MLCod.opers.chrs[1] = (char)IndxVal;
#else
        opr_ptr[1] = IndxVal;
#endif
        LOC1Msk |= LOC1BYT;
        INDX_BYT |= 0x8c;
    }
    else
    {
        ++NumBytes;
#ifdef COCO
        MLCod.opers.shrtlng.f_int = --IndxVal;
#else
            storInt(&opr_ptr[1], --IndxVal);
#endif
        INDX_BYT |= 0x8d;
    }

    return (indx_setup ());
}

/* New addition:
 * storInt() - saves an integer value in LITTLE-ENDIAN format
 */

#ifndef COCO
void
storInt (char *dst, int vlu)
{
    *dst = (vlu >> 8) & 0xff;
    dst[1] = vlu & 0xff;
}
#endif
