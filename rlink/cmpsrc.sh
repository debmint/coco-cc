#! /bin/bash

# This script will perform the make using the suite from toolshed code.
# It then enters the "disasm" directory and disassembles the original
# rlink module for the case where the os9disasm control files might have been
# changed.  It then enters the "test" directory and disassembles the rlink
# that is being built.
# Both disassemblies produce source files and the two can be dff'ed to see
# how well they match.  The best way to do this is to issue the command
# "diff -y disasm/fullrlink.asm test/myrlink.asm | less"
# To use this utility, the package found at
# https://gitlab.com/debmint/coco-disasm must be downloaded, built,
# and installed

make
# Generate a fresh asm src file to update any new variable/function names
cd disasm
os9disasm -c=rl.cmd rlink -o=fullrlink.asm >/dev/null
cd ../test
# Generate a new rl.lbl file
grep '  [iu]d\(at\|pd\) ' symbols | sort -k3 | sed 's/$/ D/' >rl.lbl
grep '  code ' symbols | sort -k3 | sed 's/$/ L/' >>rl.lbl
sed -i 's/^ \+//' rl.lbl
sed -i 's/  *\(\([iu]d\(at\|pd\)\|code\)\) / equ $/' rl.lbl

# Now output a new asm src file based on new make module
os9disasm -c=rl.cmd t_rlink -o=myrlink.asm >/dev/null
cd ..
