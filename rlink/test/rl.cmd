+ -S=/home/dlb/coco/defs/miscos9lbl pd0 -S=rl.lbl -S=/dd/defs/rof.lbl s=static.lbl

>Y D; U @; #D &

" D 09 'End of init DP
'
" D 0067 'End of DP data
'
" D 01e4 'End of Non-DP Init data
'

A 0d/5; S &
*W 2c2a-2c2d; W 2c6a-2c6d
*A 30d6-30da
W 2b9d-2ba0

" L 016A '
End of cstart
Begin of part 1
'

> X D 2b; R & 57; #1 ^ 81-89; #1 ^ 9f
> Y & d1
A 10f-126; S &
> #1 I 0124; #Y & 0126; #1 E 012b
> #1 ^ 0193 - 01b9
> #X ^ 027b-02c1
" L 03c8 /
End main()
/
> #D ^ 0533

A 549-0558; S &; A -571; B /2; A -058a; S &
A -05a0; S &; A -05b5; S &; A -05c8; S &
A -05d7; S &; A -05fa; S &; A -0605; S &
A -0614; S &; A -0628; S &; A -0643; S &
A; S &; A -065f; S &; A -0670; S &
A -0684; S &
A -0694; S &; A -06b5; S &; A -06d3; S &
A -0701; S &; A -0722; S &; A -73e; S &

" L 0740 '
Begin of part 2
lnk_02.a
'
W 077e/4
> X R (+&2) 0775; X R 0779; S R (+&1) 0798
>S R 07a7
> S R 07db-07d5; S R 07db; S R 07e5
> S R 0829; S R 0864; X R 08a9; X R 08ad
W 08b4 /4

A 1022 /5; S &; A /5; S &; A /3; S &
2 A /5; S &
* Begin of "r"
A; S &; A -1058; S &; A; S &
A -107d; B /2; A -1093; S &
A -10a8; S &; A -10c4; S &
>#D $ 10dd

" L 10c6 '
Begin of part 3
lnk_03.a
'

L & 1250 /4; L & 1273 /4
> Y & (+ D 0081) 13e9-13fe
> #1 S 1a39

A 1a37 /3; S &; A -1a52; S &; A -1a66; S &
A -1a8b; S & /2; A; S &; A -1aa9; S &
A -1ac7; S &; A -1ae1; S &; A -1af0; S & /2
A -1b28; S &; a -1b3e; S &; A -1b54; S &

" L 1b56 '
Begin of part 4
lnk_04.a
'
> #D ^ 1cbc-1cc2
" L 1fc4 "
_fgets() Similar to fgets() except it replaces newline w/NULL
Passed: (1) Storage buffer (2) File ptr
Returns: NULL on error, ptr to buffer end on success
"
> #D ^ 1ff2
A 2026-2033; S & /3; A -204f; B /2; A -205e; S &
7 A /4; S &
A /3; S &; A -2097; B /2; A -20b6; S & /3
A -20e7; S & /2; A -210c; S & /2; A -211c; B /2; A -2140; S &
A -216b; S & /2; A -2183; S & /2; A -219d; B /2

" L 21a0 '
Begin of part 5
lnk_05.a
'
> #D E 23cd; #1 ^ 23e1-23e6; #1 ^ 23ef-23f4
A 238d-23a1; B; S &; A -23b4; S &


" L 23d4 '
End split
Begin of Library routines
'
* ****************
* Library routines
* ****************

> #D E 2482
S & 2c59
> Y D (+&2) 2d36-2d46; Y D (+&2) 3151
W 28e0 /8; S & 2c77
>Y & 2cb3-2cbd; Y & 2dc6
* get/set stat
> #1 S 2f4f-2f68; #1 E 2f6b
> #1 S 2f91-2fce; #1 E 2fd1
> # 1 E 303d; #1 E 307b; #1 E 30c6; #1 E 312a

" L 3284 'Init data area

Init DP values
'
L & 3284; B; W; B /2; L L; L D

" L 328F '
Non-Dp Init Data  @ D0067
'
L & 328F
L D -3294; B -32a0; L L -32aa; B -32ad; L D; L & -32b7; A /2; S &
"L 32b0 'Begin Library functions'
'L 32b0 TenMults
*B -37a1; A -37c2
" L 32bb 'STDIO path descriptors
stdin'
" L 32d5 'stdout'
" L 32e2 'stderr'
16 W /10; B; L &
B -340a

"L 340b '
Data-text references
'
L & 340b; L D /12

"L 3419 '
Data-data references
'
*L &; L & (+D0067); L D /8
L & 3419; L D (+&2); L D /6

A /5; S &
