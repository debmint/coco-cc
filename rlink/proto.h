/* ************************************************************** *
 * proto_h - Prototypes for functions in make                     $
 *                                                                $
 * This file handles both STDC and non-STDC forms                 $
 * $Id::                                                          $
 * ************************************************************** */

#ifndef _HAVEPROTO_
#define _HAVEPROTO_
#ifdef __STDC__
/* rl_02.c */
void readrofs(void);
void L0937(binhead *, int);
void adj_ofst(void);
void adjsmofst(void);
void setbgns(void);
void goextref(OBFILES *);
void skiprefs(int);
int ncodlocs(register int);
void skipcomn(unsigned short);
SYMDAT *lastobfile(register SYMDAT *);
/* rl_03.c */
void wrtmodule(void);
void L1457(int);
void L14d3(void);
void procref(def_ref *, SYMDAT *);
int pty_gen(register char *, int);
void read_err(char *);
void writ_err(void);
int crc(char *, int, char *);
short getwrd(FILE *);
int putwrd(short, FILE *);
/* rl_04.c */
int picklist(register char *);
SYMDAT *matchsym(char *);
int L1bcf(SYMDAT *);
int fatal(char *, char *, char *, char *);
int reqMem(char *);
void ref2base(register struct ext_ref *);
struct ext_ref *findext(char *, int);
struct ext_ref *L1d61(char *);
void *get_mem(int);
char *get_loc(int);
int writemap(void);
int unrsolved(void);
void getname(register char *, int);
char *_fgets(register char *, register FILE *);
int etrap(void);
/* rl_05.c */
void creatdnodes(int);
void setrefvoid(register unsigned short *, int);
void *emptyref(register short *);
void notused(char *);
void getrefs(register struct dref_node **, int);
void writrefs(register struct dref_node *);
void putref(int);
void flushbuf(void);
/* rlink_main.c */
int main(int, char **);
char *nxtfnam(void);
char *nxtlib(void);
void illmainline(void);
void cantopen(void);
void notROF(void);
void asmerrs(void);
void toonew(void);
void twomains(void);
void in_rderr(void);
unsigned int o9_int(unsigned short);

#else

/* rl_02.c */
int readrofs();
int L0937();
int adj_ofst();
int adjsmofst();
int setbgns();
int goextref();
int skiprefs();
int ncodlocs();
int skipcomn();
SYMDAT *lastobfile();
/* rl_03.c */
int wrtmodule();
int L1457();
int L14d3();
int procref();
int pty_gen();
int read_err();
int writ_err();
/* rl_04.c */
int picklist();
SYMDAT *matchsym();
int L1bcf();
int fatal();
int reqMem();
int ref2base();
struct ext_ref *findext();
struct ext_ref *L1d61();
int *get_mem();
char *get_loc();
int writemap();
int unrsolved();
int getname();
char *_fgets();
int etrap();
/* rl_05.c */
int creatdnodes();
int setrefvoid();
int *emptyref();
int notused();
int getrefs();
int writrefs();
int putref();
int flushbuf();
/* rlink_main.c */
int main();
char *nxtfnam();
char *nxtlib();
int illmainline();
int cantopen();
int notROF();
int asmerrs();
int toonew();
int twomains();
int in_rderr();
#endif   /* __STDC__*/

#endif   /* #ifndef _HAVEPROTO_*/

